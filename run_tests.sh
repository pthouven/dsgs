
session_name=test

tmux new -d -s ${session_name}
# tmux send-keys -t ${session_name}.0 "echo a=${a}; echo b=${b}" ENTER
tmux send-keys -t ${session_name}.0 "conda activate dsgs; \
export NUMBA_DISABLE_JIT=1; \
coverage run -m pytest; \
coverage html; \
coverage xml -o reports/coverage/coverage.xml; \
genbadge coverage -o docs/coverage.svg; \
docstr-coverage ." \
ENTER
