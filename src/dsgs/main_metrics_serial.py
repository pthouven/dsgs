"""Main script to compute the reconstruction metrics and extract the estimators
(MSSE and MAP) from the available samples.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import logging

import h5py
import numpy as np
from mpi4py import MPI
from skimage.metrics import structural_similarity as ssim

from dsgs.utils.checkpoint import DistributedCheckpoint
from dsgs.utils.communications import local_split_range_nd


def main_metrics(
    comm,
    rank: int,
    filename: str,
    checkpointname: str,
    logger: logging.Logger,
    Nbi: int,
    Nmc: int,
    checkpoint_frequency: int,
    outputname: str,
    results_text_filename: str,
):
    r"""Main file to compute the performance metrics for the serial samplers
    (estimator quality and runtime).

    Parameters
    ----------
    comm : mpi4py.MPI.Intrcommunicator
        MPI communicator to use multiple cores.
    rank : int
        MPI rank of the current process.
    filename : str
        Name of the file containing the ground truth image.
    checkpointname : str
        Root pattern for the name of the checkpoint files.
    logger : logging.Logger
        Logger object to keep track of the results.
    Nbi : int
        Number of burn-in iterations to compute the estimators.
    Nmc : int
        Total number of samples in the MCMC chain (burn-in included).
    checkpoint_frequency : int
        Number of samples saved in the checkpoint files.
    outputname : str
        Name of the `.h5` file where estimators and performance metrics will be
        saved.
    results_text_filename : str
        Convenience text file displaying the performance metrics (SSIM and SNR
        for the MMSE and MAP estimators, average runtime per iteration with
        standard deviation).
    """
    size = comm.Get_size()
    ndims = 2

    # * setup communicator
    grid_size = MPI.Compute_dims(size, ndims)
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # parallel data loading
    if rank == 0:
        logger.info("Begin: defining auxiliary variables to load ground truth")

    f = h5py.File(filename + ".h5", "r+", driver="mpio", comm=MPI.COMM_WORLD)
    N = f["N"][()]
    f.close()

    # slice to extract image tile from full image file
    tile_pixels = local_split_range_nd(grid_size, N, ranknd)
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )

    # * create distributed checkpoint object to load the files of interest
    if rank == 0:
        logger.info("End: defining auxiliary variables to load ground truth")
        logger.info("Begin: setup checkpointer objects")

    # ground truth
    checkpointer_gt = DistributedCheckpoint(comm, filename)
    dic = checkpointer_gt.load("", [global_slice_tile], None, "x")
    local_x_gt = dic["x"]
    # checkpoint files from sampler
    checkpointer = DistributedCheckpoint(comm, checkpointname)
    # object to save final results to disk
    saver = DistributedCheckpoint(comm, outputname)

    # * form MMSE estimator across all files and processes
    if rank == 0:
        logger.info("End: setup checkpointer objects")
        logger.info("Begin: compute MMSE estimator")

    c_ = 0
    local_x_mmse = np.zeros(tile_size, dtype="d")

    for counter, value in enumerate(
        range(Nbi + checkpoint_frequency, Nmc + 1, checkpoint_frequency)
    ):
        dic = checkpointer.load(
            value, [global_slice_tile, np.s_[:]], None, "x_m", "counter"
        )
        local_x_mmse += dic["counter"] * dic["x_m"]
        c_ += dic["counter"]
    local_x_mmse /= c_

    # * aggregate all necessary elements for MAP, atime + std, potential, reg.
    # parameter in a single file
    if rank == 0:
        logger.info("End: compute MMSE estimator")
        logger.info("Begin: retrieve MAP estimator")

    file_id_map = np.empty(1, dtype="i")
    atime = 0.0
    stdtime = 0.0
    runtime = 0.0
    beta_ = None
    potential_ = None

    if rank == 0:
        c_ = 0
        potential_map = np.inf
        file_id_map[0] = 0
        id_map = 0
        atime = 0.0
        asqtime = 0.0
        beta_ = np.empty(Nmc - Nbi)
        potential_ = np.empty(Nmc - Nbi)

        for counter, value in enumerate(
            range(Nbi + checkpoint_frequency, Nmc + 1, checkpoint_frequency)
        ):
            with h5py.File("{}{}.h5".format(checkpointname, value), "r") as f:
                # potential function
                dset = f["potential"]
                dset.read_direct(
                    potential_,
                    np.s_[:],
                    np.s_[
                        counter
                        * checkpoint_frequency : (counter + 1)
                        * checkpoint_frequency
                    ],
                )

                # beta (regularization parameter)
                dset = f["beta"]
                dset.read_direct(
                    beta_,
                    np.s_[:],
                    np.s_[
                        counter
                        * checkpoint_frequency : (counter + 1)
                        * checkpoint_frequency
                    ],
                )

                # time (average and standard deviation)
                c__ = f["counter"][0]
                atime += c__ * f["atime"][0]
                asqtime += c__ * f["asqtime"][0]
                c_ += c__

                # MAP estimator
                file_id_m = value
                id_m = np.argmin(f["potential"])
                potential_m = f["potential"][id_map]
                if potential_m < potential_map:
                    potential_map = potential_m
                    file_id_map[0] = file_id_m
                    id_map = id_m
        runtime = atime
        atime /= c_
        stdtime = np.sqrt((asqtime - c_ * atime**2) / (c_ - 1))

    comm.Bcast([file_id_map, 1, MPI.INT], root=0)

    dic = checkpointer.load(file_id_map[0], [global_slice_tile], None, "x_map")
    local_x_map = dic["x_map"]

    # * compute reconstruction metrics
    if rank == 0:
        logger.info("End: retrieve MAP estimator")
        logger.info("Begin: compute metrics")

    local_norm_gt = np.array(np.sum(local_x_gt**2), dtype="d")
    local_err_mmse = np.array(np.sum((local_x_gt - local_x_mmse) ** 2), dtype="d")
    local_err_map = np.array(np.sum((local_x_gt - local_x_map) ** 2), dtype="d")

    norm_gt = np.full(1, np.inf, dtype="d")
    err_mmse = np.full(1, np.inf, dtype="d")
    err_map = np.full(1, np.inf, dtype="d")

    comm.Reduce(
        [local_norm_gt, MPI.DOUBLE],
        [norm_gt, MPI.DOUBLE],
        op=MPI.SUM,
        root=0,
    )

    comm.Reduce(
        [local_err_mmse, MPI.DOUBLE],
        [err_mmse, MPI.DOUBLE],
        op=MPI.SUM,
        root=0,
    )

    comm.Reduce(
        [local_err_map, MPI.DOUBLE],
        [err_map, MPI.DOUBLE],
        op=MPI.SUM,
        root=0,
    )

    # * save results to disk

    # MMSE and MAP (need all processes)
    saver.save("", [N], [global_slice_tile], [None], mode="w", x_mmse=local_x_mmse)
    saver.save("", [N], [global_slice_tile], [None], mode="a", x_map=local_x_map)

    snr_mmse = 0.0
    snr_map = 0.0
    if rank == 0:
        snr_mmse = 10 * np.log10(norm_gt[0] / err_mmse[0])
        snr_map = 10 * np.log10(norm_gt[0] / err_map[0])
        metrics_str = r"snr (MMSE) = {:1.3e}, snr (MAP) = {:1.3e}, atime = {:1.3e}, std_time = {:1.3e}, runtime = {:1.3e}".format(
            snr_mmse,
            snr_map,
            atime,
            stdtime,
            runtime,
        )
        logger.info(metrics_str)

        with open(results_text_filename, "a+") as text_file:
            print(metrics_str, file=text_file)

    # compute SSIM (only on rank 0, cannot be easily parallelized (unless
    # rewriting the function manually))
    dic_x = saver.load_from_process(0, "", 2 * [np.s_[:]], None, "x_mmse", "x_map")

    checkpointer_gt = DistributedCheckpoint(comm, filename)
    dic = checkpointer_gt.load_from_process(0, "", [np.s_[:]], None, "x")

    ssim_mmse = -1.0
    ssim_map = -1.0

    if rank == 0:
        ssim_mmse = ssim(
            dic["x"], dic_x["x_mmse"], data_range=dic["x"].max() - dic["x"].min()
        )
        ssim_map = ssim(
            dic["x"], dic_x["x_map"], data_range=dic["x"].max() - dic["x"].min()
        )

        metrics_str = r"SSIM (MMSE) = {:1.3e}, SSIM (MAP) = {:1.3e}".format(
            ssim_mmse,
            ssim_map,
        )
        logger.info(metrics_str)
        with open(results_text_filename, "a+") as text_file:
            print(metrics_str + "\n", file=text_file)
            print(
                "------------------------------------------------------------\n",
                file=text_file,
            )
        logger.info("End: compute metrics")
        logger.info("Begin: save results to disk")

    # snr, timing, reg. parameter, potential
    select = 10 * [np.s_[:]]
    chunk_sizes = 10 * [None]

    saver.save_from_process(
        0,
        "",
        select,
        chunk_sizes,
        mode="a",
        snr_mmse=snr_mmse,
        snr_map=snr_map,
        ssim_mmse=ssim_mmse,
        ssim_map=ssim_map,
        atime=atime,
        stdtime=stdtime,
        runtime=runtime,
        beta=beta_,
        potential=potential_,
        file_id_map=file_id_map,
    )

    if rank == 0:
        logger.info("End: save results to disk")


if __name__ == "__main__":
    import argparse
    import json
    from os.path import basename, join, realpath, splitext
    from pathlib import Path

    import utils.args

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    cli_parser = utils.args.my_parser()
    args, unknown = cli_parser.parse_known_args()
    parser = argparse.ArgumentParser(parents=[cli_parser], add_help=False)

    if args.config_file is not None:
        if ".json" in args.config_file:
            # The escaping of "\t" in the config file is necesarry as
            # otherwise Python will try to treat is as the string escape
            # sequence for ASCII Horizontal Tab when it encounters it
            # during json.load
            config = json.load(open(args.config_file))
            parser.set_defaults(**config)

            [
                parser.add_argument(arg)
                for arg in [arg for arg in unknown if arg.startswith("--")]
                if arg.split("--")[-1] in config
            ]

    args = parser.parse_args()

    # * debugging values
    # args.datafilename = "inpainting_data_image_micro_8_ds1_isnr40" # "data_image_micro_8_ds1_M30"
    # args.rpath = "results_inpainting_image_micro_8_ds1_isnr40_s1234_h5" # "results_image_micro_8_ds1_M30_s1234_h5_fbeta"
    # args.checkpointname = "myula/checkpoint_serial_t="  # "myula/checkpoint_t="
    # args.Nbi = 50000
    # args.checkpoint_frequency = 10000
    # args.Nmc = 100000
    # args.rfile = "final_results"
    # args.sampler = "myula"
    # Path(join(args.rpath, args.sampler)).mkdir(parents=True, exist_ok=True)

    image_name = splitext(basename(realpath(args.imfile)))[0]
    identifier = (
        "{6}_{0}_M{1}_ks{2}_s{3}_Nmc{4}_Nbi{5}".format(
            image_name,
            args.M,
            args.kernel_size,
            args.seed,
            args.Nmc,
            args.Nbi,
            args.sampler,
        ),
    )

    args.rpath = join(
        args.rpath,
        "deconv_{0}_M{1}_ks{2}_seed{3}".format(
            image_name, args.M, args.kernel_size, args.seed
        ),
        args.sampler,
    )

    if rank == 0:
        Path(args.rpath).mkdir(parents=True, exist_ok=True)
        Path(args.dpath).mkdir(parents=True, exist_ok=True)

        logpath = join(args.rpath, "logs")
        Path(logpath).mkdir(parents=True, exist_ok=True)
        logfilename = join(logpath, "metrics_" + identifier[0] + ".log")

        logger = logging.getLogger(__name__)
        logging.basicConfig(
            filename=logfilename,
            level=logging.INFO,
            filemode="w",
            format="%(asctime)s %(levelname)s %(message)s",
        )
        logger.info("Host: {}".format(MPI.Get_processor_name()))

        args.results_text_filename = args.results_text_filename + ".txt"
        with open(args.results_text_filename, "a+") as text_file:
            print("Dataset: {}".format(identifier), file=text_file)
    else:
        logger = None

    datafilename = join(args.dpath, args.datafilename)
    checkpointname = join(args.rpath, args.checkpointname)
    outputname = join(args.rpath, args.rfile)

    main_metrics(
        comm,
        rank,
        datafilename,
        checkpointname,
        logger,
        args.Nbi,
        args.Nmc,
        args.checkpoint_frequency,
        outputname,
        args.results_text_filename,
    )

    pass
