"""Helper functions to implement the FFT-based convolution operator.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import scipy

from dsgs.operators.padding import adjoint_padding, pad_array_nd


def fft2_conv(x, h, shape=None):
    r"""FFT-based 2d convolution.

    Convolve the array ``x`` with the 2d kernel ``h`` using the FFT algorithm.
    Performs linear or circular convolution depending on the padding needed to
    reach the desired size ``shape``.

    Parameters
    ----------
    x : numpy.ndarray
        Input array (of size :math:`N`).
    h : numpy.ndarray
        Input convolution kernel (of size :math:`M`).
    shape : tuple, int, optional
        Desired convolution size (:math:`K \geq \max \{ N, M \}`), by default
        None.

    Returns
    -------
    y : numpy.ndarray
        Output convolution.
    fft_h : numpy.ndarray
        Fourier transform of the convolution kernel ``h`` (of size :math:`K`).

    Raises
    ------
    ValueError
        ``x.shape`` and ``shape`` must have the same length.
    ValueError
        ``x.shape`` and ``h.shape`` must have the same length.

    Note
    ----
    This function does not allow the adjoint convolution operator to be easily
    encoded. See :func:`dsgs.operators.convolutions.fft_conv` instead.
    """
    if shape is None:
        shape = x.shape

    if not len(x.shape) == len(shape):
        raise ValueError("x.shape and shape must have the same length")

    if not len(h.shape) == len(shape):
        raise ValueError("x.shape and h.shape must have the same length")

    if (x.dtype.kind == "c") or (h.dtype.kind == "c"):
        fft_h = np.fft.fft2(h, shape)
        y = np.fft.ifft2(fft_h * np.fft.fft2(x, shape))  # cropping handled separately
    else:
        fft_h = np.fft.rfft2(h, shape)
        y = np.fft.irfft2(fft_h * np.fft.rfft2(x, shape), shape)

    return y, fft_h


def fft_conv(x, fft_h, shape):
    r"""FFT-based nd convolution.

    Convolve the array ``x`` with the kernel of Fourier transform ``fft_h``
    using the FFT. Performs linear or circular convolution depending on
    the 0-padding initially adopted for ``fft_h``.

    Parameters
    ----------
    x : numpy.ndarray
        Input array (of size :math:`N`).
    fft_h : numpy.ndarray
        Input kernel (of size
        :math:`\lfloor K/2 \rfloor + 1` if real, :math:`K` otherwise).
    shape : tuple[int]
        Full shape of the convolution (referred to as :math:`K` above).

    Returns
    -------
    y : numpy.ndarray
        Convolution results.
    """
    # turn shape into a list if only given as a scalar
    if np.isscalar(shape):
        shape_ = [shape]
    else:
        shape_ = shape
    if x.dtype.kind == "c":
        y = np.fft.ifftn(fft_h * np.fft.fftn(x, shape_))
    else:  # assuming h is a real kernel as well
        y = np.fft.irfftn(fft_h * np.fft.rfftn(x, shape_), shape_)

    return y


def linear_convolution(x, h, mode="constant"):
    """Multi-dimensional linear convolution (i.e., with zero-padding
    boundary condition).

    Parameters
    ----------
    x : numpy.ndarray
        Input array (of size :math:`N`).
    h : numpy.ndarray
        Input kernel (of size :math:`M`).

    Returns
    -------
    y : numpy.ndarray
        Convolution result (of size :math:`M + N - 1`).

    Note
    ----
    The function `scipy.ndimage.convolve` produces an output of the same
    size as the input (truncation implictly operated).
    """
    lsize = np.zeros(len(h.shape), dtype="i")
    rsize = np.array(h.shape, dtype="i") - 1
    y = pad_array_nd(x, lsize, rsize, mode="constant")
    return scipy.ndimage.convolve(y, h, mode="constant", cval=0.0)


def adjoint_linear_convolution(y, h, mode="constant"):
    """Adjoint multi-dimensional linear convolution (i.e., with zero-padding
    boundary condition).

    Parameters
    ----------
    y : numpy.ndarray
        Input array (of size :math:`M + N - 1`).
    h : numpy.ndarray
        Input kernel (of size :math:`M`).

    Returns
    -------
    x : numpy.ndarray
        Adjoint convolution result (of size :math:`N`).
    """
    lsize = np.zeros(len(h.shape), dtype="i")
    rsize = 2 * (np.array(h.shape, dtype="i") - 1)
    yp = pad_array_nd(y, lsize, np.array(h.shape, dtype="i") - 1, mode="constant")
    x = scipy.ndimage.convolve(
        yp, np.conj(np.flip(h, axis=None)), mode="constant", cval=0.0
    )
    # ! np.flip(h, axis=None) flips all axes
    return adjoint_padding(x, lsize, rsize, mode="constant")
