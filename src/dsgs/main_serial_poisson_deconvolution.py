"""Main script to launch the serial SGS sampler and MYULA :cite:p:`Durmus2018`
instantiated for a linear deconvolution problem under Poisson noise.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import logging

import h5py
import numpy as np

from dsgs.operators.data import generate_2d_gaussian_kernel, generate_data, get_image
from dsgs.operators.linear_convolution import SerialConvolution
from dsgs.samplers.serial.serial_pmyula_poisson_deconvolution import MyulaSGS
from dsgs.samplers.serial.serial_psgla_poisson_deconvolution import PsglaSGS
from dsgs.utils.checkpoint import SerialCheckpoint


def main(
    bool_data: bool,
    results_path: str,
    imagefilename: str,
    filename: str,
    checkpointname: str,
    logger: logging.Logger,
    sampler: str = "psgla",
    warmstart_iter: int = 0,
    downsampling: int = 1,
    checkpointflag: bool = False,
    profiling: bool = False,
    M=30,
    Nmc: int = 1000,
    rho: float = 1.0,
    alpha: float = 1.0,
    beta: float = 1.0,
    checkpoint_frequency: int = 500,
    monitor_frequency: int = 5,
    seed=1234,
    kernel_size: int = 8,
):
    r"""Main file to launch the serial samplers on synthetic data (Poisson
    deconvolution under TV-based prior).

    Parameters
    ----------
    bool_data : bool
        Flag indicating whether synthetic data should be generated or not.
    results_path : str
        Path to which the results (i.e., checkpoint files) will be saved.
    imagefilename : str
        Name of the ground truth image from which synthetic data have been
        generated.
    filename : str
        Name of the data file produced (data generation phase).
    checkpointname : str
        Root name for the ``.h5`` checkpoint files saved to disk.
    logger : logging.Logger
        Logger object to save information about the current run.
    sampler : str, optional
        Name of the sampler considered, by default "psgla".
    warmstart_iter : int, optional
        Iteration index identifying the checkpoing file from which the
        sampler should be restarted, by default 0.
    downsampling : int, optional
        Downsampling factor of the original grountruth image to generate a
        smaller dataset, by default 1. The same value is adopted along each
        axis (i.e., a factor 2 means each axis is downsampled by a factor 2).
    checkpointflag : bool, optional
        Flag to indicate the sampler needs to be restarted from a checkpoint
        file, by default False.
    profiling : bool, optional
        Flag to activate code profiling, by default False.
    M : int, optional
        Maximum image intensity (data generation), by default 30. Controls the
        intensity level of the Poisson noise.
    Nmc : int, optional
        Number of samples to be generated, by default 1000.
    rho : float, optional
        Value of the AXDA splitting parameters, by default 1.0.
    alpha : float, optional
        Value of the AXDA augmentation parameters, by default 1.0.
    beta : float, optional
        Value of the TV regularization parameter, by default 1.0.
    checkpoint_frequency : int, optional
        Number of iterations after which a checkpoint file is saved to disk, by
        default 500.
    monitor_frequency : int, optional
        Iteration frequency at which information is displayed in the logger, by
        default 5.
    seed : int, optional
        Number to seed the random number generator, by default 1234.
    kernel_size : int, optional
        Horizontal / vertical size of the square convolution kernel, by default
        8.

    Raises
    ------
    ValueError
        Checks whether the sampler is among the known options. ``Unknown sampler``.
    """
    # * loading / generating synthetic data
    if bool_data:
        logger.info("Data generation")

        # data  parameters
        kernel_std = 1

        # load image and generate kernel
        x = get_image(imagefilename, M)
        x = x[::downsampling, ::downsampling]
        N = np.array(x.shape, dtype="i")
        h = generate_2d_gaussian_kernel(kernel_size, kernel_std)

        logger.info("Begin: write image to disk")
        logger.info("Image size {}x{}".format(*N))
        f = h5py.File(filename, "w")
        dset_N = f.create_dataset("N", [2], dtype="i")
        dset_h = f.create_dataset("h", 2 * [kernel_size], dtype="d")
        dset_M = f.create_dataset("M", [1], dtype="i")
        dset_x = f.create_dataset("x", N, dtype="d")
        dset_N[()] = N
        dset_h[()] = h
        dset_M[()] = M
        dset_x[()] = x

        del dset_N, dset_M, dset_h, dset_x

        logger.info("End: write image to disk")
        logger.info("Begin: load image")

        # load image tile
        overlap_size = np.array(h.shape, dtype="i")
        overlap_size -= 1

        logger.info("End: load image")
        logger.info("Begin: generating local data")

        # data generation
        data, clean_data = generate_data(x, h)

        logger.info("End: generating local data")
        logger.info("Begin: writing data to disk")

        # data save
        data_size = N + overlap_size
        dset_data = f.create_dataset("data", data_size, dtype="d")
        dset_clean_data = f.create_dataset("clean_data", data_size, dtype="d")
        dset_data[()] = data
        dset_clean_data[()] = clean_data
        f.close()

        logger.info("End: writing data to disk")
    else:
        logger.info("Begin: loading data and images")

        f = h5py.File(filename, "r")
        N = f["N"][()]
        M = f["M"][()]
        h = f["h"][()]
        data = f["data"][()]
        f.close()
        logger.info("End: loading data and images")

        logger.info("Parameters defined, setup sampler")

        if args.prof:
            pr = cProfile.Profile()
            pr.enable()

        # ! to be revised
        data_size = np.array(data.shape, dtype="i")
        serial_chkpt = SerialCheckpoint(checkpointname)

        if sampler == "myula":
            # sampler described in :cite:p:`Vono2019icassp`
            # ! use circular convolution model (leads to sampling a 0-padded
            # ! version of the image of interest)
            conv_operator = SerialConvolution(data_size, h, data_size)

            hyperparameters = {
                "rho1": rho,
                "rho2": rho,
                "rho3": rho,
                "alpha1": alpha,
                "alpha2": alpha,
                "alpha3": alpha,
                "beta": beta,
            }

            myula_sampler = MyulaSGS(
                M,
                data,
                conv_operator,
                hyperparameters,
                Nmc,
                seed,
                serial_chkpt,
                checkpoint_frequency,
                logger,
                warmstart_it=warmstart_iter,
                warmstart=checkpointflag,
                save_batch=True,
            )
            myula_sampler.sample()

        elif sampler == "psgla":
            # proposed sampler

            # ! linear convolution model
            conv_operator = SerialConvolution(N, h, data_size)

            hyperparameters = {
                "rho1": rho,
                "rho2": rho,
                "alpha1": alpha,
                "alpha2": alpha,
                "beta": beta,
            }

            psgla_sampler = PsglaSGS(
                M,
                data,
                conv_operator,
                hyperparameters,
                Nmc,
                seed,
                serial_chkpt,
                checkpoint_frequency,
                logger,
                warmstart_it=warmstart_iter,
                warmstart=checkpointflag,
                save_batch=True,
            )

            psgla_sampler.sample()

        else:
            raise ValueError("Unknown sampler: {}".format(sampler))


if __name__ == "__main__":
    import argparse
    import cProfile
    import json
    import sys
    from os.path import basename, join, realpath, splitext
    from pathlib import Path
    from socket import gethostname

    import utils.args

    cli_parser = utils.args.my_parser()
    args, unknown = cli_parser.parse_known_args()
    parser = argparse.ArgumentParser(parents=[cli_parser], add_help=False)

    # debugging option
    # args.config_file = "config_file.json"

    if args.config_file is not None:
        if ".json" in args.config_file:
            # The escaping of "\t" in the config file is necesarry as
            # otherwise Python will try to treat is as the string escape
            # sequence for ASCII Horizontal Tab when it encounters it
            # during json.load
            config = json.load(open(args.config_file))
            parser.set_defaults(**config)

            [
                parser.add_argument(arg)
                for arg in [arg for arg in unknown if arg.startswith("--")]
                if arg.split("--")[-1] in config
            ]

    args = parser.parse_args()

    # # * debugging values
    # print(args)
    # args.imfile = "img/image_micro_8.h5"
    # args.datafilename = "data_image_micro_8_ds1_M30_k8"
    # args.rpath = "debug"
    # args.checkpointname = "checkpoint_serial_t="
    # args.restart = 20
    # args.downsampling = 1
    # args.data = False
    # args.load = False
    # args.sampler = "skrock"  # psgla_aug, sk-rock, myula-aug (to be debugged)
    # args.checkpoint_frequency = 10
    # args.Nmc = 30
    # args.kernel_size = 8
    # args.M = 30
    # args.rho = 1.0
    # args.alpha = 1.0
    # args.beta = 0.2

    image_name = splitext(basename(realpath(args.imfile)))[0]
    args.rpath = join(
        args.rpath,
        "deconv_{0}_M{1}_ks{2}_seed{3}".format(
            image_name, args.M, args.kernel_size, args.seed
        ),
        args.sampler,
    )
    Path(args.rpath).mkdir(parents=True, exist_ok=True)
    Path(args.dpath).mkdir(parents=True, exist_ok=True)

    # TODO: to be simplified
    # https://stackoverflow.com/questions/3609852/which-is-the-best-way-to-allow-configuration-options-be-overridden-at-the-comman
    logpath = join(args.rpath, "logs")
    Path(logpath).mkdir(parents=True, exist_ok=True)
    if args.data:
        logfilename = join(
            logpath,
            "data_generation_{0}_M{1}_ks{2}_s{3}_Nmc{4}_r{5}.log".format(
                image_name, args.M, args.kernel_size, args.seed, args.Nmc, args.restart
            ),
        )
    else:
        logfilename = join(
            logpath,
            "{6}_{0}_M{1}_ks{2}_s{3}_Nmc{4}_r{5}.log".format(
                image_name,
                args.M,
                args.kernel_size,
                args.seed,
                args.Nmc,
                args.restart,
                args.sampler,
            ),
        )

    logger = logging.getLogger(__name__)
    logging.basicConfig(
        filename=logfilename,
        level=logging.INFO,
        filemode="w",
        format="%(asctime)s %(levelname)s %(message)s",
    )
    logger.info("Host: {}".format(gethostname()))
    logger.info("Poisson deconvolution")
    logger.info(
        "Dataset: {0}, max intensity={1}, kernel size={2}".format(
            image_name, args.M, args.kernel_size
        )
    )

    if args.verbose:
        logger.info("Verbosity: on")
    if args.prof:
        logger.info("Code profiling: on")
    if args.data:
        logger.info("Generate data")
    else:
        logger.info("AXDA sampling from observations in {}".format(args.datafilename))
    if args.load:
        logger.info("Loading checkpoint: {}".format(args.checkpointfile))
    logger.info("Results saved to {}".format(args.rpath))

    datafilename = join(args.dpath, args.datafilename + ".h5")
    checkpointname = join(args.rpath, args.checkpointname)

    main(
        args.data,
        args.rpath,
        args.imfile,
        datafilename,
        checkpointname,
        logger,
        sampler=args.sampler,
        warmstart_iter=args.restart,
        downsampling=args.downsampling,
        checkpointflag=args.load,
        profiling=args.prof,
        M=args.M,
        Nmc=args.Nmc,
        rho=args.rho,
        alpha=args.alpha,
        beta=args.beta,
        checkpoint_frequency=args.checkpoint_frequency,
        monitor_frequency=1,
        seed=args.seed,
        kernel_size=args.kernel_size,
    )

    pass
