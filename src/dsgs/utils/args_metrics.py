"""List of parameters (with default values) to be passed to the main script
aimed at evaluating the reconstruction metrics.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import argparse


def parse_args():
    """Parse main scripts input arguments.

    Returns
    -------
    argparse.ArgumentParser
        List of parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-cfreq",
        "--checkpoint_frequency",
        help="frequency at which checkpoint .h5 files are saved",
        default=20,
        type=int,
    )
    parser.add_argument(
        "-cn",
        "--checkpointname",
        help="template name for any checkpoint file",
        default="checkpoint",
        type=str,
    )
    parser.add_argument(
        "-df",
        "--datafilename",
        help="name of the data file from which to load the ground truth",
        default="checkpoint.h5",
        type=str,
    )
    parser.add_argument(
        "--dpath",
        help="path to data folder (created if it does not exist)",
        default="data",
        type=str,
    )
    parser.add_argument(
        "--downsampling",
        help="Image downsampling factor (data generation)",
        default=1,
        type=int,
    )
    parser.add_argument(
        "--logfile",
        help="full path and name of the log file, with extension",
        default="std.log",
        type=str,
    )
    parser.add_argument(
        "--Nmc",
        help="number of Monte-Carlo iterations",
        default=50,
        type=int,
    )
    parser.add_argument(
        "--Nbi",
        help="number of burn-in iterations",
        default=0,
        type=int,
    )
    parser.add_argument(
        "--rfile",
        help="name of results file (w/o file extension)",
        default="final_results",
        type=str,
    )
    parser.add_argument(
        "--rpath",
        help="path to results folder (created if it does not exist)",
        default="results",
        type=str,
    )
    args = parser.parse_args()

    return args
