"""Generic checkpointing objects (serial and distributed version), relying on
the ``h5py`` library. Handles any number of variables to be saved, but only
expects ``numpy.ndarray``, ``int`` or variables describing the state of a
``numpy.random.Generator`` object.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import sys
from abc import ABC, abstractmethod
from os.path import join

import h5py
import numpy as np


def extract_rng_state(rng):
    r"""Extract the state of a random number generator in the form of two
    ``numpy.ndarray`` objects.

    Parameters
    ----------
    rng : numpy.random.Generator
        Random number generator.

    Returns
    -------
    state_array : numpy.ndarray
        State parameter of the input random generator.
    inc_array : numpy.ndarray
        Increment parameter of the input random generator.

    Note
    ----
    The ``state`` and ``inc`` fields of a ``numpy.random.Generator`` object
    are very large integers, and thus need to be converted to the
    hexadecimal format (and later to an array of ``int``) to be saved into
    a ``.h5`` file.
    """
    # * state and inc are very large integers, and thus need to be
    # converted to hex format (later to an array of ints) to be saved in an
    # .h5 file
    # https://docs.python.org/3/library/stdtypes.html#int.to_bytes
    # ! need 32 bytes in length: otherwise, the inverse operation
    # ! int.from_bytes(state_array,sys.byteorder) does not coincide with the
    # ! original int value
    state_array = np.array(
        bytearray(rng.__getstate__()["state"]["state"].to_bytes(32, sys.byteorder))
    )
    inc_array = np.array(
        bytearray(rng.__getstate__()["state"]["inc"].to_bytes(32, sys.byteorder))
    )
    return state_array, inc_array


def restore_rng_state(rng, loaded_inc_array, loaded_state_array):
    """Set the state of a random number generator using the 32 bytes
    increment and state arrays stored in ``loaded_inc_array`` and
    ``loaded_state_array``, respectively.

    Parameters
    ----------
    rng : numpy.random.Generator
        Random number generator object.
    loaded_inc_array : np.ndarray of numpy.uint8, of size 32.
        Increment variable to restore the state of the generator.
    loaded_state_array : np.ndarray of numpy.uint8, of size 32.
        State variable to restore the state of the generator.
    Note
    ----
    Input generator updated in-place.
    """
    loaded_state = int.from_bytes(loaded_state_array, sys.byteorder)
    loaded_inc = int.from_bytes(loaded_inc_array, sys.byteorder)
    current_state = rng.__getstate__()
    current_state["state"]["state"] = loaded_state
    current_state["state"]["inc"] = loaded_inc
    rng.__setstate__(current_state)

    pass


class BaseCheckpoint(ABC):
    r"""Base checkpoint object gathering the parameters common to the checkpoint
    schemes used in this library.

    .. _hdf5plugin: http://www.silx.org/doc/hdf5plugin/latest/usage.html#hdf5plugin.Blosc

    Attributes
    ----------
    root_filename : str
        Root of the filename (containing path to the appropriate directory)
        where the checkpoint file is / will be stored.
    cname : str
        Name of the hdf5 compression filter (aka compressor). Default to
        "gzip".
    clevel : int
        Compression level. Default to 5 (default for Blosc).
    shuffle : int
        Byte shuffle option (see `hdf5plugin`_ documentation). Default to 1.
        Not used for the moment.

    Note
    ----
        The following virtual methods need to be implemented in any daughter class:

        - :meth:`dsgs.utils.checkpoint.BaseCheckpoint.save`,
        - :meth:`dsgs.utils.checkpoint.BaseCheckpoint.load`.
    """

    def __init__(
        self,
        root_filename,
        cname="gzip",
        clevel=5,
        shuffle=1,
    ):
        """
        Parameters
        ----------
        root_filename : str
            Root of the filename (containing path to the appropriate directory)
            where the checkpoint file is / will be stored.
        cname : str
            Name of the hdf5 compression filter (aka compressor). Default to
            "gzip".
        clevel : int
            Compression level. Default to 5 (default for Blosc).
        shuffle : int
            Byte shuffle option (see hdf5plugin_ documentation). Default to 1.
            Not used for the moment.
        """
        self.root_filename = root_filename
        self.cname = cname
        self.clevel = clevel
        self.shuffle = shuffle

    def filename(self, file_id):
        """Get name of target file.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.

        Returns
        -------
        str
            Target filename.
        """
        return "{}{}.h5".format(self.root_filename, file_id)

    @abstractmethod
    def save(
        self, file_id, chunk_sizes, rng=None, mode="w", rdcc_nbytes=None, **kwargs
    ):  # pragma: no cover
        r"""Saving content of the input variables within the dictionary
        kwargs to disk.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        chunk_sizes : list of tuples, of length ``n``
            List of tuples representing the chunk size for each input variable.
            For scalar input, the corresponding chunk size needs to be
            ``None``.
        rng : numpy.random.Generator or None, optional
            Random number generator to be restored using specific state stored
            on disk, by default None.
        mode : str, optional
            Mode to open the h5 file ("a", or "w"). By default "w".
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).
        kwargs : list of numpy.ndarray, of length ``n``
            List of keyword arguments reprensenting Python variables to be
            saved. Only allows numpy.ndarray or integers.
        """
        pass

    @abstractmethod
    def load(self, file_id, *args):  # pragma: no cover
        r"""Loading some variables from a checkpoint file.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        args : list of str
            Name of variables to be loaded from disk.

        Returns
        -------
        NotImplemented
        """
        return NotImplemented


class SerialCheckpoint(BaseCheckpoint):
    r"""Checkpoint in serial environments, using ``h5py``.

    Attributes
    ----------
    root_filename : str
        Root of the filename (containing path to the appropriate directory)
        where the checkpoint file is / will be stored.
    cname : str
        Name of the hdf5 compression filter (aka compressor). Default to
        "gzip".
    clevel : int
        Compression level. Default to 5 (default for Blosc).
    shuffle : int
        Byte shuffle option (see hdf5plugin_ documentation). Default to 1.
        Not used for the moment.

    .. _hdf5plugin: http://www.silx.org/doc/hdf5plugin/latest/usage.html#hdf5plugin.Blosc
    """

    def __init__(
        self,
        root_filename,
        cname="gzip",
        clevel=5,
        shuffle=1,
    ):
        """
        Parameters
        ----------
        root_filename : str
            Root of the filename (containing path to the appropriate directory)
            where the checkpoint file is / will be stored.
        cname : str
            Name of the hdf5 compression filter (aka compressor). Default to
            "gzip".
        clevel : int
            Compression level. Default to 5 (default for Blosc).
        shuffle : int
            Byte shuffle option (see hdf5plugin_ documentation). Default to 1.
            Not used for the moment.

        .. _hdf5plugin: http://www.silx.org/doc/hdf5plugin/latest/usage.html#hdf5plugin.Blosc
        """
        super(SerialCheckpoint, self).__init__(root_filename, cname, clevel, shuffle)

    def save(
        self, file_id, chunk_sizes, rng=None, mode="w", rdcc_nbytes=None, **kwargs
    ):
        r"""Saving content of the input variables within the dictionary
        kwargs to disk.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        chunk_sizes : list of tuples, of length ``n``
            List of tuples representing the chunk size for each input variable.
            For scalar input, the corresponding chunk size needs to be
            ``None``.
        rng : numpy.random.Generator or None, optional
            Save to disk the current state of a random number generator, if
            any. By default None.
        mode : str, optional
            Mode to open the h5 file ("a", or "w"). By default "w".
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).
        kwargs : list of numpy.ndarray, of length ``n`` or int
            List of keyword arguments reprensenting Python variables to be
            saved. Only allows numpy.ndarray or integers.
        """
        filename_ = self.filename(file_id)

        # ! 200 MB cache size
        with h5py.File(filename_, mode, rdcc_nbytes=rdcc_nbytes) as f:
            # * backup state random number generator
            if rng is not None:
                # convert rng to numpy array
                state_array, inc_array = extract_rng_state(rng)
                # backup rng state
                dset = f.create_dataset("state", (32,), dtype=np.uint8)
                dset[:] = state_array
                dset = f.create_dataset("inc", (32,), dtype=np.uint8)
                dset[:] = inc_array

            # * backup other variables
            for count, (var_name, var) in enumerate(kwargs.items()):
                # ! only allow numpy.ndarray or scalar integers
                if isinstance(var, np.ndarray):
                    if var.size > 1:
                        dset = f.create_dataset(
                            var_name,
                            var.shape,
                            dtype=var.dtype,
                            compression=self.cname,
                            compression_opts=self.clevel,
                            chunks=chunk_sizes[count],
                        )
                    else:
                        dset = f.create_dataset(var_name, (1,), dtype=var.dtype)
                else:
                    # ! when input is a scalar, only allow integer type
                    dset = f.create_dataset(var_name, (1,), dtype="i")
                dset[()] = var

    def load(self, file_id, select, rng, *args, rdcc_nbytes=None):
        r"""Loading some variables from a checkpoint file.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        select : list of slices, of size ``n``
            List of slices to save each variable to parts of larger arrays to
            on disk.
        rng : numpy.random.Generator or None
            Random number generator to be restored using specific state stored
            on disk.
        args : list of str, of size ``n``
            Variable list of strings corresponding to the name of the variables
            to be loaded.
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).

        Returns
        -------
        dic_var : dict
            Dictionary containing all the variables loaded from the ``.h5``
            file.
        """
        # https://stackoverflow.com/questions/10223427/python-function-that-returns-a-variable-number-of-outputs
        filename_ = self.filename(file_id)
        dic_var = {}

        with h5py.File(filename_, "r", rdcc_nbytes=rdcc_nbytes) as f:
            if rng is not None:
                restore_rng_state(rng, f["inc"][:], f["state"][:])

            for count, var in enumerate(args):
                dic_var[var] = f[var][select[count]]

        return dic_var


class DistributedCheckpoint(BaseCheckpoint):
    r"""Checkpoint in distributed environments, using ``h5py``.

    Attributes
    ----------
    comm : mpi4py.MPI.Comm
        MPI communicator.
    root_filename : str
        Root of the filename (containing path to the appropriate directory)
        where the checkpoint file is / will be stored.
    cname : str
        Name of the hdf5 compression filter (aka compressor). Default to
        "gzip".
    clevel : int
        Compression level. Default to 5 (default for Blosc).
    shuffle : int
        Byte shuffle option (see hdf5plugin_ documentation). Default to 1.
        Not used for the moment.
    rank : int
        Rank of the current process.
    ncores : int
        Total nunmber of processes involved in ``comm``.

    .. _hdf5plugin: http://www.silx.org/doc/hdf5plugin/latest/usage.html#hdf5plugin.Blosc
    """

    def __init__(
        self,
        comm,
        root_filename,
        cname="gzip",
        clevel=5,
        shuffle=1,
    ):
        """
        Parameters
        ----------
        comm : mpi4py.MPI.Comm
            MPI communicator.
        root_filename : str
            Root of the filename (containing path to the appropriate directory)
            where the checkpoint file is / will be stored.
        cname : str, optional
            Name of the hdf5 compression filter (aka compressor). Default to
            "gzip".
        clevel : int, optional
            Compression level. Default to 5 (default for Blosc).
        shuffle : int
            Byte shuffle option (see hdf5plugin_ documentation). Default to
            1. Not used for the moment.

        .. _hdf5plugin: http://www.silx.org/doc/hdf5plugin/latest/usage.html#hdf5plugin.Blosc
        """
        super(DistributedCheckpoint, self).__init__(
            root_filename, cname, clevel, shuffle
        )
        self.comm = comm
        self.rank = comm.Get_rank()
        self.ncores = comm.Get_size()

    def save(
        self,
        file_id,
        shape,
        select,
        chunk_sizes,
        rng=None,
        mode="w",
        rdcc_nbytes=None,
        **kwargs,
    ):
        r"""Saving content of the input variables within the dictionary
        kwargs to disk.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        shape : list of tuples, of size ``n``
            List of tuples corresponding to the shape of each larger arrays
            in which the variables will be saved to disk.
        select : list of slices, of size ``n``
            List of slices to save each variable to parts of larger arrays to
            on disk.
        chunk_sizes : list of tuples, of length ``n``
            List of tuples representing the chunk size for each input variable.
            For scalar input, the corresponding chunk size needs to be
            ``None``.
        rng : numpy.random.Generator or None, optional
            Save to disk the current state of a random number generator, if
            any. By default None.
        mode : str, optional
            Mode to open the h5 file ("a", or "w"). By default "w".
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).
        kwargs : list of numpy.ndarray, of length ``n`` or int
            List of keyword arguments reprensenting Python variables to be
            saved. Only allows numpy.ndarray or integers.
        """
        filename_ = self.filename(file_id)

        with h5py.File(
            filename_, mode, driver="mpio", comm=self.comm, rdcc_nbytes=rdcc_nbytes
        ) as f:
            # * backup state random number generator
            if rng is not None:
                # convert local rng to numpy array
                state_array, inc_array = extract_rng_state(rng)
                # backup rng state: dataset creation must be done collectively
                dset_state = []
                dset_inc = []
                for r in range(self.ncores):
                    dset_state.append(
                        f.create_dataset(join(str(r), "state"), (32,), dtype=np.uint8)
                    )
                    dset_inc.append(
                        f.create_dataset(join(str(r), "inc"), (32,), dtype=np.uint8)
                    )
                dset_state[self.rank][:] = state_array
                dset_inc[self.rank][:] = inc_array

            # * backup other variables
            for count, (var_name, var) in enumerate(kwargs.items()):
                # ! only allow numpy.ndarray or scalar integers
                if isinstance(var, np.ndarray):
                    dset = f.create_dataset(
                        var_name,
                        shape[count],
                        dtype=var.dtype,
                        compression=self.cname,
                        compression_opts=self.clevel,
                        chunks=chunk_sizes[count],
                    )
                else:
                    # ! when input is a scalar, only allow integer type
                    dset = f.create_dataset(var_name, (1,), dtype="i")

                with dset.collective:
                    dset[select[count]] = var

    def save_from_process(
        self,
        rank,
        file_id,
        select,
        chunk_sizes,
        rng=None,
        mode="a",
        rdcc_nbytes=None,
        **kwargs,
    ):
        r"""Saving content of some input variables from a single process.

        Parameters
        ----------
        rank : int
            Rank of the process in the communicator.
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        select : list of slices
            List of slices to save each variable to parts of larger arrays to
            on disk.
        chunk_sizes : list of tuples, of length ``n``
            List of tuples representing the chunk size for each input variable.
            For scalar input, the corresponding chunk size needs to be
            ``None``.
        rng : numpy.random.Generator or None, optional
            Save to disk the current state of a random number generator, if
            any. By default None.
        mode : str, optional
            Mode to open the h5 file ("a", or "w"). By default "a".
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).
        kwargs : list of numpy.ndarray, of length ``n`` or int
            List of keyword arguments reprensenting Python variables to be
            saved. Only allows numpy.ndarray or integers.
        """
        if self.rank == rank:
            filename_ = self.filename(file_id)

            with h5py.File(filename_, mode, rdcc_nbytes=rdcc_nbytes) as f:
                # * backup state random number generator
                if rng is not None:
                    # convert local rng to numpy array
                    state_array, inc_array = extract_rng_state(rng)
                    # backup some rng state
                    dset = f.create_dataset(
                        join(str(rank), "state"), (32,), dtype=np.uint8
                    )
                    dset[:] = state_array
                    dset = f.create_dataset(
                        join(str(rank), "inc"), (32,), dtype=np.uint8
                    )
                    dset[:] = inc_array

                # * backup other variables
                for count, (var_name, var) in enumerate(kwargs.items()):
                    # ! only allow numpy.ndarray or scalar integers
                    # ! compression only allowed for non-scalar datasets
                    if isinstance(var, np.ndarray):
                        if var.size > 1:
                            dset = f.create_dataset(
                                var_name,
                                var.shape,
                                dtype=var.dtype,
                                compression=self.cname,
                                compression_opts=self.clevel,
                                chunks=chunk_sizes[count],
                            )
                        else:
                            dset = f.create_dataset(var_name, (1,), dtype=var.dtype)
                    else:
                        # ! when input is a scalar, only allow integer type
                        dset = f.create_dataset(var_name, (1,), dtype="i")

                    dset[select[count]] = var

    def load(self, file_id, select, rng, *args, rdcc_nbytes=None):
        r"""Loading some variables from a checkpoint file.

        Parameters
        ----------
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        select : list of slices
            List of slices to retrieve parts of each variable to be loaded from
            disk.
        rng : numpy.random.Generator or None
            Random number generator to be restored using specific state stored
            on disk.
        args : list of str
            Variable list of strings corresponding to the name of the variables
            to be loaded.
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).

        Returns
        -------
        dic_var : dict
            Dictionary containing all the variables loaded from the ``.h5``
            file.
        """
        # https://stackoverflow.com/questions/10223427/python-function-that-returns-a-variable-number-of-outputs
        filename_ = self.filename(file_id)
        dic_var = {}

        # rdcc_nbytes = 1024 ** 2 * 200
        with h5py.File(
            filename_, "r", driver="mpio", comm=self.comm, rdcc_nbytes=rdcc_nbytes
        ) as f:
            if rng is not None:
                restore_rng_state(
                    rng,
                    f[join(str(self.rank), "inc")][:],
                    f[join(str(self.rank), "state")][:],
                )

            for count, var in enumerate(args):
                dic_var[var] = f[var][select[count]]

        return dic_var

    def load_from_process(self, rank, file_id, select, rng, *args, rdcc_nbytes=None):
        r"""Loading some variables from a single process using a checkpoint
        file.

        Parameters
        ----------
        rank : int
            Rank of the process in the communicator.
        file_id : str or int
            String or integer describing the id of the target ``.h5`` file.
        select : list of slices
            List of slices to retrieve parts of each variable to be loaded from
            disk.
        rng : numpy.random.Generator or None
            Random number generator to be restored using specific state stored
            on disk.
        args : list of str
            Variable list of strings corresponding to the name of the variables
            to be loaded.
        rdcc_nbytes : float, optional
            Sets the total size (measured in bytes) of the raw data chunk cache
            for each dataset. The default size is 1 MB. This should be set to
            the size of each chunk times the number of chunks that are likely
            to be needed in cache. By default None (see
            `h5py documentation <https://docs.h5py.org/en/stable/high/file.html>`_).

        Returns
        -------
        dic_var : dict
            Dictionary containing all the variables loaded from the ``.h5``
            file.
        """
        filename_ = self.filename(file_id)
        dic_var = {}

        if self.rank == rank:
            with h5py.File(filename_, "r", rdcc_nbytes=rdcc_nbytes) as f:
                if rng is not None:
                    restore_rng_state(
                        rng,
                        f[join(str(self.rank), "inc")][:],
                        f[join(str(self.rank), "state")][:],
                    )

                for count, var in enumerate(args):
                    dic_var[var] = f[var][select[count]]
                    # ! fixing error with h5py: load data in the native byteorder
                    # https://numpy.org/doc/stable/reference/generated/numpy.dtype.newbyteorder.html
                    # dic_var[var].dtype = dic_var[var].dtype.newbyteorder("=")

        return dic_var
