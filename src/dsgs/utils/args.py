"""List of parameters (with default values) to be passed to the main synthetic
data scripts (serial and distributed). Some parameters will only be taken into
account for one configuration or the other (i.e., serial or distributed).
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import argparse


def parse_args():
    """Parse main scripts input arguments.

    Returns
    -------
    argparse.ArgumentParser
        List of parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rho",
        help="AAXDA splitting parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "--alpha",
        help="AAXDA augmentation parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "--beta",
        help="TV regularization parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "-cfreq",
        "--checkpoint_frequency",
        help="frequency at which checkpoint .h5 files are saved",
        default=20,
        type=int,
    )
    parser.add_argument(
        "-cf",
        "--checkpointfile",
        help="name of the checkpoint .h5 file to be loaded",
        default="checkpoint.h5",
        type=str,
    )
    parser.add_argument(
        "-cn",
        "--checkpointname",
        help="template name for any checkpoint file",
        default="checkpoint",
        type=str,
    )
    parser.add_argument(
        "-d", "--data", help="activate data generation", action="store_true"
    )
    parser.add_argument(
        "-df",
        "--datafilename",
        help="name for the data file (without file extension)",
        default="data",
        type=str,
    )
    parser.add_argument(
        "--dpath",
        help="path to data folder (created if it does not exist)",
        default="data",
        type=str,
    )
    parser.add_argument(
        "--downsampling",
        help="Image downsampling factor (data generation)",
        default=1,
        type=int,
    )
    parser.add_argument(
        "-ext",
        "--extension",
        help="file extension for the checkpoint file; only used for the serial version of the algorithm (not active anymore)",
        default="h5",
        type=str,
        # choices={"h5", "zarr"},
    )
    parser.add_argument(
        "--imfile",
        help="full path to the ground truth image",
        type=str,
        default="img/boat.png",
    )
    parser.add_argument(
        "-ksize",
        "--kernel_size",
        help="size of the convolution kernel",
        type=int,
        default=8,
    )
    parser.add_argument(
        "--load", help="option to load checkpoint file", action="store_true"
    )
    parser.add_argument(
        "--logfile",
        help="full path and name of the log file, with extension",
        default="std.log",
        type=str,
    )
    parser.add_argument(
        "--M",
        help="max value of the image used to generate the synthetic data",
        default=30,
        type=int,
    )
    parser.add_argument(
        "--Nmc",
        help="number of Monte-Carlo iterations",
        default=50,
        type=int,
    )
    parser.add_argument(
        "--restart",
        help="restart iteration",
        default=0,
        type=int,
    )
    parser.add_argument("--prof", help="activate code profiling", action="store_true")
    parser.add_argument(
        "--rpath",
        help="path to results folder (created if it does not exist)",
        default="results",
        type=str,
    )
    parser.add_argument(
        "--sampler",
        help="name of the selected sampler (choice only active for the serial versions)",
        type=str,
        default="psgla",
        choices={"psgla", "myula", "psgla_aug", "myula_aug"},
    )
    parser.add_argument(
        "--save",
        help="mode to save file: write samples to a single file or one file per process",
        default="process",
        choices={"process", "single"},
    )
    parser.add_argument(
        "--seed",
        help="random seed used initialize the sampler; overriden when loading a checkpoint file",
        type=int,
        default=1234,
    )
    parser.add_argument(
        "--dataseed",
        help="random seed used initialize the sampler; overriden when loading a checkpoint file",
        type=int,
        default=1234,
    )
    parser.add_argument(
        "-v", "--verbose", help="increase output verbosity", action="store_true"
    )

    args = parser.parse_args()

    return args


def my_parser():
    """Parser for the main scripts input arguments.

    Returns
    -------
    argparse.ArgumentParser
        List of parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rho",
        help="AAXDA splitting parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "--alpha",
        help="AAXDA augmentation parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "--beta",
        help="TV regularization parameter",
        default=1.0,
        type=float,
    )
    parser.add_argument(
        "-cfreq",
        "--checkpoint_frequency",
        help="frequency at which checkpoint .h5 files are saved",
        default=20,
        type=int,
    )
    parser.add_argument(
        "-cf",
        "--checkpointfile",
        help="name of the checkpoint .h5 file to be loaded",
        default="checkpoint.h5",
        type=str,
    )
    parser.add_argument(
        "-cn",
        "--checkpointname",
        help="template name for any checkpoint file",
        default="checkpoint",
        type=str,
    )
    parser.add_argument(
        "-c",
        "--config_file",
        dest="config_file",
        type=str,
        default=None,
        help=".json configuration file (command line arguments override its content)",
    )
    parser.add_argument(
        "-d", "--data", help="activate data generation", action="store_true"
    )
    parser.add_argument(
        "-df",
        "--datafilename",
        help="name for the data file (without file extension)",
        default="data",
        type=str,
    )
    parser.add_argument(
        "--dpath",
        help="path to data folder (created if it does not exist)",
        default="data",
        type=str,
    )
    parser.add_argument(
        "--downsampling",
        help="Image downsampling factor (data generation)",
        default=1,
        type=int,
    )
    parser.add_argument(
        "-ext",
        "--extension",
        help="file extension for the checkpoint file; only used for the serial version of the algorithm (not active anymore)",
        default="h5",
        type=str,
        # choices={"h5", "zarr"},
    )
    parser.add_argument(
        "--imfile",
        help="full path to the ground truth image",
        type=str,
        default="img/boat.png",
    )
    parser.add_argument(
        "-ksize",
        "--kernel_size",
        help="size of the convolution kernel",
        type=int,
        default=8,
    )
    parser.add_argument(
        "--load", help="option to load checkpoint file", action="store_true"
    )
    parser.add_argument(
        "--logfile",
        help="full path and name of the log file, with extension",
        default="std.log",
        type=str,
    )
    parser.add_argument(
        "--M",
        help="max value of the image used to generate the synthetic data",
        default=30,
        type=int,
    )
    parser.add_argument(
        "--Nbi",
        help="number of burn-in iterations",
        default=0,
        type=int,
    )
    parser.add_argument(
        "--Nmc",
        help="number of Monte-Carlo iterations",
        default=50,
        type=int,
    )
    parser.add_argument(
        "--restart",
        help="restart iteration",
        default=0,
        type=int,
    )
    parser.add_argument(
        "--rfile",
        help="name of results file (w/o file extension)",
        default="final_results (when computing estimators)",
        type=str,
    )
    parser.add_argument("--prof", help="activate code profiling", action="store_true")
    parser.add_argument(
        "--rpath",
        help="path to results folder (created if it does not exist)",
        default="results",
        type=str,
    )
    parser.add_argument(
        "--sampler",
        help="name of the selected sampler (choice only active for the serial versions)",
        type=str,
        default="psgla",
        choices={"psgla", "myula"},
    )
    parser.add_argument(
        "--save",
        help="mode to save file: write samples to a single file or one file per process",
        default="process",
        choices={"process"},
    )
    parser.add_argument(
        "--seed",
        help="random seed used initialize the sampler; overriden when loading a checkpoint file",
        type=int,
        default=1234,
    )
    parser.add_argument(
        "--dataseed",
        help="random seed used initialize the sampler; overriden when loading a checkpoint file",
        type=int,
        default=1234,
    )
    parser.add_argument(
        "-v", "--verbose", help="increase output verbosity", action="store_true"
    )
    parser.add_argument(
        "--results_text_filename",
        help="name of a text file containing the quality metrics",
        type=str,
        default="metrics",
    )

    return parser
