"""Main script to launch the distributed SGS sampler instantiated for a linear
deconvolution problem under Poisson noise.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import logging

import h5py
import numpy as np
from mpi4py import MPI

import dsgs.operators.data as data
from dsgs.operators.distributed_convolutions import calculate_local_data_size
from dsgs.operators.linear_convolution import SyncLinearConvolution
from dsgs.samplers.parallel.spmd_psgla_poisson_deconvolution import SpmdPsglaSGS
from dsgs.utils.checkpoint import DistributedCheckpoint, SerialCheckpoint
from dsgs.utils.communications import local_split_range_nd


def main(
    comm,
    rank: int,
    bool_data: bool,
    results_path: str,
    imagefilename: str,
    filename: str,
    checkpointname: str,
    logger: logging.Logger,
    save_mode: str = "process",
    warmstart_iter: int = 0,
    downsampling: int = 1,
    checkpointflag: bool = False,
    profiling: bool = False,
    M=30,
    Nmc: int = 1000,
    rho: float = 1.0,
    alpha: float = 1.0,
    beta: float = 1.0,
    checkpoint_frequency: int = 500,
    monitor_frequency: int = 5,
    seed=1234,
    kernel_size: int = 8,
    sampler="psgla",
):
    r"""Main file to launch the distributed SGS sampler on synthetic data
    (Poisson deconvolution).

    Parameters
    ----------
    comm : mpi4py.MPI.Intrcommunicator
        MPI communicator to use multiple cores.
    rank : int
        MPI rank of the current process.
    bool_data : bool
        Flag indicating whether synthetic data should be generated or not.
    results_path : str
        Path to which the results (i.e., checkpoint files) will be saved.
    imagefilename : str
        Name of the ground truth image from which synthetic data have been
        generated.
    filename : str
        Name of the data file produced (data generation phase).
    checkpointname : str
        Root name for the ``.h5`` checkpoint files saved to disk.
    logger : logging.Logger
        Logger object to save information about the current run.
    save_mode : str, optional
        Mode to save the checkpoint (in a single file, or with one file per
        process). By default "process"
    warmstart_iter : int, optional
        Iteration index identifying the checkpoing file from which the
        sampler should be restarted, by default 0.
    downsampling : int, optional
        Downsampling factor of the original grountruth image to generate a
        smaller dataset, by default 1. The same value is adopted along each
        axis (i.e., a factor 2 means each axis is downsampled by a factor 2).
    checkpointflag : bool, optional
        Flag to indicate the sampler needs to be restarted from a checkpoint
        file, by default False.
    profiling : bool, optional
        Flag to activate code profiling, by default False.
    M : int, optional
        Maximum image intensity (data generation), by default 30. Controls the
        intensity level of the Poisson noise.
    Nmc : int, optional
        Number of samples to be generated, by default 1000.
    rho : float, optional
        Value of the AXDA splitting parameters, by default 1.0.
    alpha : float, optional
        Value of the AXDA augmentation parameters, by default 1.0.
    beta : float, optional
        Value of the TV regularization parameter, by default 1.0.
    checkpoint_frequency : int, optional
        Number of iterations after which a checkpoint file is saved to disk, by
        default 500.
    monitor_frequency : int, optional
        Iteration frequency at which information is displayed in the logger, by
        default 5.
    seed : int, optional
        Number to seed the random number generator, by default 1234.
    kernel_size : int, optional
        Horizontal / vertical size of the square convolution kernel, by default
        8.
    sampler : str, optional
        Name of the sampler considered, by default "psgla".

    Raises
    ------
    Checks whether the sampler is among the known options. ``Unknown sampler``.
    """
    size = comm.Get_size()
    ndims = 2

    # * setup communicator
    grid_size = MPI.Compute_dims(size, ndims)
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # * loading / generating synthetic data (as in Vono et al.)
    if bool_data:
        if rank == 0:
            logger.info("Data generation")

            # data  parameters
            kernel_std = 1

            # load image and generate kernel
            x = data.get_image(imagefilename, M)
            x = x[::downsampling, ::downsampling]
            N = np.array(x.shape, dtype="i")
            h = data.generate_2d_gaussian_kernel(kernel_size, kernel_std)
        else:
            N = np.empty((2,), dtype="i")
            h = np.empty(2 * [kernel_size], dtype="d")

        # send necessary info to each worker
        comm.Bcast([N, 2, MPI.INT], root=0)
        comm.Bcast([h, kernel_size**2, MPI.DOUBLE], root=0)

        if rank == 0:
            logger.info("Begin: write image to disk")
            logger.info("Image size {}x{}".format(*N))
            f = h5py.File(filename, "w")
            dset_N = f.create_dataset("N", [2], dtype="i")
            dset_h = f.create_dataset("h", 2 * [kernel_size], dtype="d")
            dset_M = f.create_dataset("M", [1], dtype="i")
            dset_x = f.create_dataset("x", N, dtype="d")
            dset_N[()] = N
            dset_h[()] = h
            dset_M[()] = M
            dset_x[()] = x
            f.close()
            del dset_N, dset_M, dset_h, dset_x
            del x
        comm.Barrier()

        if rank == 0:
            logger.info("End: write image to disk")
            logger.info("Begin: load image facets")

        # load image tile
        overlap_size = np.array(h.shape, dtype="i")
        overlap_size -= 1
        facet, tile_pixels = data.mpi_load_image_from_h5(
            comm, grid_size, ranknd, filename, N, overlap_size
        )

        if rank == 0:
            logger.info("End: load image facets")
            logger.info("Begin: generating local data")

        # parallel data generation
        local_data, local_clean_data, global_slice_data = data.generate_local_data(
            comm, cartcomm, grid_size, ranknd, facet, h, N, tile_pixels, backward=False
        )

        if rank == 0:
            logger.info("End: generating local data")
            logger.info("Begin: writing data to disk")

        # parallel data save
        # TODO: use checkpoint object here
        data_size = N + overlap_size
        data.mpi_save_data_to_h5(
            comm, filename, data_size, local_data, local_clean_data, global_slice_data
        )

        if rank == 0:
            logger.info("End: writing data to disk")
    else:
        # parallel data loading
        if rank == 0:
            logger.info("Begin: loading data and images")

        f = h5py.File(filename, "r+", driver="mpio", comm=MPI.COMM_WORLD)
        N = f["N"][()]
        M = f["M"][()]
        h = f["h"][()]
        f.close()

        overlap_size = np.array(ndims * [kernel_size], dtype="i")
        overlap_size -= 1
        data_size = N + overlap_size
        tile_pixels = local_split_range_nd(grid_size, N, ranknd)
        tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

        local_data_size, facet_size, facet_size_adj = calculate_local_data_size(
            tile_size, ranknd, overlap_size, grid_size, backward=False
        )
        local_data = data.mpi_load_data_from_h5(
            comm,
            ranknd,
            filename,
            data_size,
            local_data_size,
            tile_pixels,
            overlap_size,
            var_name="data",
            backward=False,
        )
        if rank == 0:
            logger.info("End: loading data and images")

        # convolution nodel and checkpoint
        sync_conv_model = SyncLinearConvolution(
            N, data_size, h, comm, grid_size, h.itemsize, False
        )

        if save_mode == "process":
            # using one checkpointer per process (multiple files)
            checkpointer = SerialCheckpoint("{}p{}_t=".format(checkpointname, rank))
        elif save_mode == "single":
            # ! not supported at the moment (need debugging + extension loading
            # ! / saving)
            # use a distributed checkpointer object (write in a single file)
            checkpointer = DistributedCheckpoint(comm, checkpointname)
        else:
            raise ValueError(
                r"Unkown save mode {}. Possible options are: 'single' or 'process'".format(
                    save_mode
                )
            )

        if rank == 0:
            logger.info("Parameters defined, setup sampler")

        if args.prof:
            pr = cProfile.Profile()
            pr.enable()

        hyperparameters = {
            "rho1": rho,
            "rho2": rho,
            "alpha1": alpha,
            "alpha2": alpha,
            "beta": beta,
        }

        spmd_sampler = SpmdPsglaSGS(
            M,
            local_data,
            sync_conv_model,
            hyperparameters,
            Nmc,
            seed,
            checkpointer,
            checkpoint_frequency,
            logger,
            warmstart_it=warmstart_iter,
            warmstart=checkpointflag,
        )

        spmd_sampler.sample()


if __name__ == "__main__":
    import argparse
    import cProfile
    import json
    import sys
    from os.path import basename, join, realpath, splitext
    from pathlib import Path

    import utils.args

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    ncores = comm.Get_size()

    cli_parser = utils.args.my_parser()
    args, unknown = cli_parser.parse_known_args()
    parser = argparse.ArgumentParser(parents=[cli_parser], add_help=False)

    # debugging option
    # args.config_file = "config_file.json"

    if args.config_file is not None:
        if ".json" in args.config_file:
            # The escaping of "\t" in the config file is necesarry as
            # otherwise Python will try to treat is as the string escape
            # sequence for ASCII Horizontal Tab when it encounters it
            # during json.load
            config = json.load(open(args.config_file))
            parser.set_defaults(**config)

            [
                parser.add_argument(arg)
                for arg in [arg for arg in unknown if arg.startswith("--")]
                if arg.split("--")[-1] in config
            ]

    args = parser.parse_args()

    # * debugging values
    # print(args)
    # args.imfile = "img/cameraman.png"
    # args.datafilename = "conv_data_cameraman_ds1_M30_k8"
    # args.rpath = "results_conv_cameraman_ds1_M30_k8_h5"
    # args.sampler = "psgla_aug_mml"
    # args.rpath = join(args.rpath, "spmd_" + args.sampler)
    # args.checkpointname = "checkpoint_"
    # args.restart = 10  # -1
    # args.downsampling = 1
    # args.data = False
    # args.load = False
    # args.save = "process"
    # args.checkpoint_frequency = 10
    # args.Nmc = 30
    # args.kernel_size = 8
    # args.M = 30
    # Path(args.rpath).mkdir(parents=True, exist_ok=True)
    # args.rho = 1.0
    # args.alpha = 1.0
    # args.beta = 0.2
    # args.logfile = join(args.rpath, "std.log")

    image_name = splitext(basename(realpath(args.imfile)))[0]
    args.rpath = join(
        args.rpath,
        "deconv_{0}_M{1}_ks{2}_seed{3}_n{4}".format(
            image_name, args.M, args.kernel_size, args.seed, ncores
        ),
        args.sampler,
    )

    if rank == 0:
        Path(args.rpath).mkdir(parents=True, exist_ok=True)
        Path(args.dpath).mkdir(parents=True, exist_ok=True)

        logpath = join(args.rpath, "logs")
        Path(logpath).mkdir(parents=True, exist_ok=True)
        if args.data:
            logfilename = join(
                logpath,
                "data_generation_{0}_M{1}_ks{2}_s{3}_Nmc{4}_r{5}_n{6}.log".format(
                    image_name,
                    args.M,
                    args.kernel_size,
                    args.seed,
                    args.Nmc,
                    args.restart,
                    ncores,
                ),
            )
        else:
            logfilename = join(
                logpath,
                "{6}_{0}_M{1}_ks{2}_s{3}_Nmc{4}_r{5}_n{7}.log".format(
                    image_name,
                    args.M,
                    args.kernel_size,
                    args.seed,
                    args.Nmc,
                    args.restart,
                    args.sampler,
                    ncores,
                ),
            )

        logger = logging.getLogger(__name__)
        logging.basicConfig(
            filename=logfilename,
            level=logging.INFO,
            filemode="w",
            format="%(asctime)s %(levelname)s %(message)s",
        )

        logger.info("Host: {}".format(MPI.Get_processor_name()))
        logger.info("Poisson deconvolution")
        logger.info(
            "Dataset: {0}, max intensity={1}, kernel size={2}, ncores={3}".format(
                image_name,
                args.M,
                args.kernel_size,
                ncores,
            )
        )

        if args.verbose:
            logger.info("Verbosity: on")
        if args.prof:
            logger.info("Code profiling: on")
        if args.data:
            logger.info("Generate data")
        else:
            logger.info(
                "AXDA sampling from observations in {}".format(args.datafilename)
            )
        if args.load:
            logger.info("Loading checkpoint: {}".format(args.checkpointfile))
        logger.info("Results saved to {}".format(args.rpath))

    else:
        logger = None

    datafilename = join(args.dpath, args.datafilename + ".h5")
    checkpointname = join(args.rpath, args.checkpointname)

    main(
        comm,
        rank,
        args.data,
        args.rpath,
        args.imfile,
        datafilename,
        checkpointname,
        logger,
        save_mode=args.save,
        warmstart_iter=args.restart,
        downsampling=args.downsampling,
        checkpointflag=args.load,
        profiling=args.prof,
        M=args.M,
        Nmc=args.Nmc,
        rho=args.rho,
        alpha=args.alpha,
        beta=args.beta,
        checkpoint_frequency=args.checkpoint_frequency,
        monitor_frequency=1,
        seed=args.seed,
        kernel_size=args.kernel_size,
        sampler=args.sampler,
    )

    pass
