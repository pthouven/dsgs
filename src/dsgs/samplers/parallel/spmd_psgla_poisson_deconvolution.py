# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from logging import Logger

import numpy as np
from mpi4py import MPI
from numba import jit

import dsgs.utils.communications as ucomm
from dsgs.functionals.prox import (
    kullback_leibler,
    l21_norm,
    prox_kullback_leibler,
    prox_l21norm,
    prox_nonegativity,
)
from dsgs.operators.distributed_convolutions import calculate_local_data_size
from dsgs.operators.jtv import chunk_gradient_2d, chunk_gradient_2d_adjoint
from dsgs.operators.linear_convolution import SyncLinearConvolution
from dsgs.samplers.spmd_sampler import BaseSPMDSampler
from dsgs.samplers.utils import sample_u
from dsgs.utils.checkpoint import DistributedCheckpoint, SerialCheckpoint
from dsgs.utils.communicators import SyncCartesianCommunicatorTV


def loading(
    checkpointer: DistributedCheckpoint,
    warmstart_iter: int,
    rng: np.random.Generator,
    global_slice_tile,
    global_slice_data,
):
    """Function interface around the :meth:`DistributedCheckpoint.load` method.

    Parameters
    ----------
    checkpointer : DistributedCheckpoint
        Checkpoint object.
    warmstart_iter : int
        Warm-start iteration (indicating the name of the file to be loaded).
    rng : np.random.Generator
        Random number generator.
    global_slice_tile : list of slice
        Slice to select a tile from the full image.
    global_slice_data : list of slice
        Slice to select a tile from the full image.

    Returns
    -------
    numpy.ndarray, int, float
        Variables required to restart the sampler.
    """
    # ! saving only estimator and last state of each variable
    dic = checkpointer.load(
        warmstart_iter,
        [(np.s_[-1], *global_slice_tile)]
        + 2 * [global_slice_data]
        + 2 * [(np.s_[:], *global_slice_tile)]
        + 7 * [np.s_[-1]],
        rng,
        "x",
        "z1",
        "u1",
        "z2",
        "u2",
        "beta",
        "rho1",
        "rho2",
        "alpha1",
        "alpha2",
        "potential",
        "iter",
    )

    return (
        dic["x"],
        dic["z1"],
        dic["u1"],
        dic["z2"],
        dic["u2"],
        dic["potential"],
        dic["beta"],
        dic["rho1"],
        dic["rho2"],
        dic["alpha1"],
        dic["alpha2"],
        dic["iter"],
    )


def loading_per_process(
    rank,
    checkpointer: SerialCheckpoint,
    warmstart_iter: int,
    rng: np.random.Generator,
):
    """Function interface around the :meth:`SerialCheckpoint.load` method to
    load the content of one file per process.

    Parameters
    ----------
    checkpointer : SerialCheckpoint
        Checkpoint object.
    warmstart_iter : int
        Warm-start iteration (indicating the name of the file to be loaded).
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray, int, float
        Variables required to restart the sampler.
    """
    # ! only estimator and last state of each variable has been saved to disk
    if rank == 0:
        dic = checkpointer.load(
            warmstart_iter,
            [np.s_[:]] + 4 * [np.s_[:]] + 3 * [np.s_[-1]],
            rng,
            "x",
            "z1",
            "u1",
            "z2",
            "u2",
            "regularization",
            "potential",
            "iter",
        )
    else:
        dic = checkpointer.load(
            warmstart_iter,
            [np.s_[:]] + 4 * [np.s_[:]],
            rng,
            "x",
            "z1",
            "u1",
            "z2",
            "u2",
        )
        dic["potential"] = 0.0
        dic["regularization"] = np.zeros((5,), dtype="d")
        dic["iter"] = 0

    return (
        dic["x"],
        dic["z1"],
        dic["u1"],
        dic["z2"],
        dic["u2"],
        dic["potential"],
        dic["regularization"],
        dic["iter"],
    )


def saving_per_process(
    rank,
    comm,
    checkpointer: SerialCheckpoint,
    iter_mc: int,
    rng: np.random.Generator,
    local_x,
    local_u1,
    local_z1,
    local_u2,
    local_z2,
    regularization,
    potential,
    counter,
    atime,
    asqtime,
    nsamples,
):
    """Save useful variables on a per-process basis (i.e., one file per
    process).

    Parameters
    ----------
    rank : int
        Rank of the current process.
    comm : _type_
        Communicator used.
    checkpointer : SerialCheckpoint
        Checkpointer object associated with the current process.
    iter_mc : int
        _description_
    rng : np.random.Generator
        Random number generator of the current process.
    local_x : numpy.ndarray
        Primal variable.
    local_u1 : numpy.ndarray
        Augmentation variable (data fidelity).
    local_z1 : numpy.ndarray
        Splitting variable (data fidelity).
    local_u2 : numpy.ndarray
        Augmentation variable (prior).
    local_z2 : numpy.ndarray
        Splitting variable (prior).
    beta : float
        Regularization parameter.
    potential : numpy.ndarray
        Objective function over the iterations.
    counter : int
        Number of iterations since the last checkpoint.
    atime : float
        Average iteration runtime (on root process).
    asqtime : float
        Average of square iteration runtime (on root process).
    nsamples : int
        Number of samples comsidered for the checkpoint.
    """
    root_id = 0
    it = iter_mc + 1

    # find index of the candidate MAP within the current checkpoint
    id_map = np.empty(1, dtype="i")
    if rank == root_id:
        id_map[0] = np.argmin(potential[:nsamples])
    comm.Bcast([id_map, 1, MPI.INT], root=0)

    # estimators to be saved to disk
    x_mmse = np.mean(local_x[:nsamples, ...], axis=0)
    x_sq_m = np.mean(local_x[:nsamples, ...] ** 2, axis=0)
    x_map = local_x[id_map[0]]

    # checkpointer configuration
    chunk_sizes_ = (
        # [(1, *local_x.shape[1:])]  # ! see if saving only the last point or more...
        [local_x.shape[1:]]
        + 3 * [(*local_x.shape[1:],)]
        + 2 * [local_z1.shape]
        + 2 * [(1, *local_x.shape[1:])]
    )

    checkpointer.save(
        it,
        chunk_sizes_,
        rng=rng,
        mode="w",
        rdcc_nbytes=1024**2 * 200,  # 200 MB cache
        x=local_x[-1],
        x_map=x_map,
        x_m=x_mmse,
        x_sq_m=x_sq_m,
        z1=local_z1,
        u1=local_u1,
        z2=local_z2,
        u2=local_u2,
    )

    if rank == 0:
        chunk_sizes_ = 6 * [None]
        checkpointer.save(
            it,
            chunk_sizes_,
            mode="a",
            rdcc_nbytes=1024**2 * 200,  # 200 MB cache
            asqtime=np.array(asqtime),
            atime=np.array(atime),
            iter=it,
            potential=potential[:nsamples],
            regularization=regularization[:nsamples],
            counter=counter,
        )
    pass


# ! a separate distributed tv operator is required (forward communication can
# ! be grouped with the convolution when considering a linear convolution, but
# ! not the adjoint!)
def gradient_x(
    model: SyncLinearConvolution,
    adjoint_tv_communicator: SyncCartesianCommunicatorTV,
    isfirst,
    islast,
    buffer_Hx,
    buffer_Gx,
    z1,
    u1,
    z2,
    u2,
    rho1,
    rho2,
    local_slice_tv,
    local_slice_tv_adj,
):
    # Hx and Gx already computed during the previous iteration: buffer_Hx and
    # Gx contain the right info in terms of those terms
    # local direct operator applied out of this function

    # local update step
    buffer_Hx[model.local_slice_conv_adj] += u1 - z1
    buffer_Hx[model.local_slice_conv_adj] /= rho1
    buffer_Gx[tuple((np.s_[:], *local_slice_tv_adj))] += u2 - z2
    buffer_Gx[tuple((np.s_[:], *local_slice_tv_adj))] /= rho2

    # communicate facet borders to neighbours Gx
    adjoint_tv_communicator.update_borders(buffer_Gx)

    # * local adjoint operator
    # convolution
    local_grad_x = model.adjoint(buffer_Hx)
    # 2d tv
    chunk_gradient_2d_adjoint(
        buffer_Gx[0],
        buffer_Gx[1],
        local_grad_x,
        isfirst,
        islast,
    )

    return local_grad_x


def sample_x(x, gamma_x, grad_x, rng):
    r"""PSGLA update step for the image :math:`x`.

    Parameters
    ----------
    x : numpy.array
        Input image.
    gamma_x : float
        PSGLA step size (gradient and stochastic perturbation).
    grad_x : numpy.array
        Gradient of the smooth part involved in
        :math:`-\log \pi_{\alpha, \beta}(x \mid y, (u_i, z_i)_{1\leq i \leq 2})`.
    rng : np.random.Generator
        Random number generator.

    Note
    ----
    The variable ``x`` is updated in-place.
    """
    # ! in-place update for x
    x += np.sqrt(2 * gamma_x) * rng.standard_normal(size=x.shape) - gamma_x * grad_x
    prox_nonegativity(x)
    return


def sample_z1(z1, y, Hx, u1: float, rho1: float, gamma1: float, rng):
    r"""PSGLA update step for the splitting variable :math:`z_1`.

    Parameters
    ----------
    z1 : numpy.array
        Current state of the splitting variable :math:`z_1`.
    y : numpy.array
        Observed data.
    Hx : numpy.array
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    u1 : numpy.array
        Current state of the augmentation variable :math:`u_1`.
    rho1 : float
        AXDA parameter.
    gamma1 : float
        PSGLA step size (gradient and stochastic perturbation).
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.array
        New state for the splitting variable :math:`z_1`.
    """
    grad_z1 = (z1 - u1 - Hx) / rho1
    z = z1 + np.sqrt(2 * gamma1) * rng.standard_normal(size=z1.shape) - gamma1 * grad_z1
    return prox_kullback_leibler(z, y, lam=gamma1)


def sample_z2(z2, Gx, u2, rho2: float, gamma2: float, lambda_gamma2: float, rng):
    r"""PSGLA update step for the splitting variable :math:`z_2`.

    Parameters
    ----------
    z2 : numpy.array
        Current state of the splitting variable :math:`z_2`.
    Gx : numpy.array
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    u2 : numpy.array
        Current state of the augmentation variable :math:`u_2`.
    rho2 : float
        AXDA splitting parameter.
    gamma2 : float
        PSGLA step size (gradient and stochastic perturbation).
    lambda_gamma2 : float
        Current value of the regularization parameter :math:`\lambda`
        multiplied by the PSGLA step size.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.array
        New state for the splitting variable :math:`z_2`.
    """
    grad_z2 = (z2 - u2 - Gx) / rho2
    z = z2 + np.sqrt(2 * gamma2) * rng.standard_normal(size=z2.shape) - gamma2 * grad_z2
    return prox_l21norm(z, lam=lambda_gamma2, axis=0)


def sample_beta(a: float, b: float, N, l21_z2: float, rng) -> float:
    r"""Sample the regularization parameter :math:`\lambda`.

    Parameters
    ----------
    a : float
        Hyper-parameter of the Gamma prior for the regularization parameter
        :math:`\lambda`.
    b : float
        Hyper-parameter of the Gamma prior for the regularization parameter
        :math:`\lambda`.
    N : numpy.array(dtype=int)
        Size of the image :math:`x`.
    l21_z2 : float
        Value of :math:`\|z_2\|_{2, 1}`.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    float
        New state for the regularization parameter.
    """
    return rng.gamma(a + 2 * np.prod(N), 1 / (b + l21_z2))


def sample_var(a: float, b: float, d, fX: float, rng) -> float:
    r"""Sample the regularization parameter :math:`\lambda`.

    Parameters
    ----------
    a : float
        Hyper-parameter of the Gamma prior for the regularization parameter
        :math:`\lambda`.
    b : float
        Hyper-parameter of the Gamma prior for the regularization parameter
        :math:`\lambda`.
    d : int
        Dimension in which the parameter lives.
    fX : float
        Value of the potential involved in the conditional distribution.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    float
        New state for the regularization parameter.
    """
    return 1 / rng.gamma(a + d, 1 / (b + fX))


@jit(nopython=True, cache=True)
def potential_function(y, Hx, Gx, z1, u1, z2, u2, rho1, rho2, alpha1, alpha2, beta):
    """Compute the current potential value for the AXDA model.

    Parameters
    ----------
    y : numpy.array
        Observed data.
    Hx : numpy.array
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    Gx : numpy.array
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    z1 : numpy.array
        Splitting variable :math:`z_1`.
    u1 : numpy.array
        Auxiliary augmentation variable :math:`u_1`.
    z2 : numpy.array
        Splitting variable :math:`z_2`.
    u2 : numpy.array
        Auxiliary augmentation variable :math:`u_2`.
    rho1 : float
        AXDA splitting parameter.
    rho2 : float
        AXDA splitting parameter.
    alpha1 : float
        AXDA augmentation parameter.
    alpha2 : float
        AXDA augmentation parameter.
    beta : float
        Value of the regularization parameter.

    Returns
    -------
    float
        Current potential value for the AXDA model.
    """
    potential = (
        kullback_leibler(z1, y)
        + np.sum(np.abs(Hx - z1 + u1) ** 2) / (2 * rho1)
        + np.sum((Gx - z2 + u2) ** 2) / (2 * rho2)
        + np.sum(np.abs(u1) ** 2) / (2 * alpha1)
        + np.sum(u2**2) / (2 * alpha2)
        + beta * l21_norm(z2)
    )

    return potential


@jit(nopython=True, cache=True)
def potentials_function(y, Hx, Gx, z1, u1, z2, u2):
    """Compute the value of the potentials invovled in the AXDA model.

    Parameters
    ----------
    y : numpy.array
        Observed data.
    Hx : numpy.array
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    Gx : numpy.array
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    z1 : numpy.array
        Splitting variable :math:`z_1`.
    u1 : numpy.array
        Auxiliary augmentation variable :math:`u_1`.
    z2 : numpy.array
        Splitting variable :math:`z_2`.
    u2 : numpy.array
        Auxiliary augmentation variable :math:`u_2`.
    rho1 : float
        AXDA splitting parameter.
    rho2 : float
        AXDA splitting parameter.
    alpha1 : float
        AXDA augmentation parameter.
    alpha2 : float
        AXDA augmentation parameter.
    beta : float
        Value of the regularization parameter.

    Returns
    -------
    float
        Current potential value for the AXDA model.
    """
    data_fidelity = kullback_leibler(z1, y)
    lrho1 = np.sum(np.abs(Hx - z1 + u1) ** 2) / 2
    lrho2 = np.sum((Gx - z2 + u2) ** 2) / 2
    lalpha1 = np.sum(np.abs(u1) ** 2) / 2
    lalpha2 = np.sum(u2**2) / 2
    prior = l21_norm(z2)

    return np.array([data_fidelity, lrho1, lrho2, lalpha1, lalpha2, prior], dtype="d")


class SpmdPsglaSGS(BaseSPMDSampler):
    def __init__(
        self,
        xmax,
        observations,
        model,
        hyperparameters,
        Nmc: int,
        seed: int,
        checkpointer,
        checkpointfreq: int,
        logger: Logger,
        warmstart_it: int = -1,
        warmstart: bool = False,
    ):
        # * model parameters (AXDA regularization parameter)
        self.max_sq_kernel = np.max(np.abs(model.fft_kernel)) ** 2

        # * auxiliary quantities for stochastic gradient updates
        # dimensions
        self.d_N = np.prod(model.image_size)
        # dimension of the proper space (removing 0s)
        # d_tv = (N[0] - 1) * N[1] + (N[1] - 1) * N[0]
        self.d_M = np.prod(model.data_size)

        # tile
        grid_size = MPI.Compute_dims(model.comm.Get_size(), model.ndims)
        grid_size = np.array(grid_size, dtype="i")
        self.tile_pixels = ucomm.local_split_range_nd(
            grid_size, model.image_size, model.ranknd
        )
        self.tile_size = self.tile_pixels[:, 1] - self.tile_pixels[:, 0] + 1

        # data
        # ! larger number of data points on the border (forward overlap)
        (
            self.local_data_size,
            self.facet_size,
            self.facet_size_adj,
        ) = calculate_local_data_size(
            self.tile_size, model.ranknd, model.overlap_size, grid_size, backward=False
        )

        # facet (convolution)
        self.offset = self.facet_size - self.tile_size
        self.offset_adj = self.facet_size_adj - self.tile_size

        # facet (tv)
        self.offset_tv = self.offset - (self.offset > 0).astype("i")
        self.offset_tv_adj = np.logical_and(model.ranknd > 0, grid_size > 1).astype("i")
        self.tv_facet_size_adj = self.tile_size + self.offset_tv_adj

        # * Useful slices (direct operators)
        # extract tile from local facet (direct conv. operator)
        self.local_slice_tile = ucomm.get_local_slice(
            model.ranknd, grid_size, self.offset, backward=False
        )
        # extract values from local conv facet to apply local gradient operator
        self.local_slice_tv = ucomm.get_local_slice(
            model.ranknd, grid_size, self.offset_tv, backward=False
        )

        # * Useful slices (adjoint operators)
        # set value of local convolution in the adjoint buffer
        self.local_slice_conv_adj = ucomm.get_local_slice(
            model.ranknd, grid_size, self.offset_adj, backward=True
        )
        # set value of local discrete gradient into the adjoint gradient buffer
        self.local_slice_tv_adj = ucomm.get_local_slice(
            model.ranknd, grid_size, self.offset_tv_adj, backward=True
        )

        # per-process checkpoints
        checkpoint_select = [np.s_[:]] + 4 * [np.s_[:]]
        chunk_sizes = (
            [tuple(self.tile_size)]
            + 2 * [tuple(self.local_data_size)]
            + 2 * [(1, *self.tile_size)]
        )

        # ! local parameter sizes
        parameter_sizes = {
            "x": self.tile_size,
            "u1": self.local_data_size,
            "z1": self.local_data_size,
            "u2": (2, *self.tile_size),
            "z2": (2, *self.tile_size),
        }

        self.xmax = xmax

        super(SpmdPsglaSGS, self).__init__(
            observations,
            model,
            parameter_sizes,
            hyperparameters,
            Nmc,
            seed,
            checkpointer,
            checkpointfreq,
            checkpoint_select,
            chunk_sizes,
            logger,
            warmstart_it=warmstart_it,
            warmstart=warmstart,
            updated_hyperparameters=[],
        )

        # ! auxiliary buffer for in-place updates / communications of the main
        # ! - variable x
        self.local_x = np.empty(self.facet_size, dtype="d")

        # * setup communication scheme
        # ! convolution (direct + adjoint) + direct TV covered by self.model
        # adjoint TV communicator
        # ! need a different object for the moment (because of the size required...)
        self.adjoint_tv_communicator = SyncCartesianCommunicatorTV(
            self.model.comm,
            self.model.cartcomm,
            self.model.grid_size,
            self.local_x.itemsize,
            self.tv_facet_size_adj,
            direction=True,
        )

    def _initialize_parameters(self):
        # TODO: need to create self.hyperparameter_batch later on (if
        # TODO - sampling or MML)

        for key in self.parameter_batch.keys():
            if key == "x":  # ! batch only kept for the main variable x
                self.parameter_batch[key][0] = self.local_rng.integers(
                    0, high=self.xmax, size=self.parameter_sizes[key], endpoint=True
                ).astype(float)
            else:
                self.parameter_batch[key] = self.local_rng.integers(
                    0, high=self.xmax, size=self.parameter_sizes[key], endpoint=True
                ).astype(float)

        # ! need to broadcast latest value of all the hyperparameters
        # ! load a matrix containing hyperparameters? (see the easiest to save)
        if self.rank == 0:
            for key in self.hyperparameters.keys():
                self.hyperparameter_batch[key][0] = self.hyperparameters[key]

        pass

    def _initialize_auxiliary_parameters(self, it: int):
        # called after initialize_parameters
        # it: position of the element initialized in the larger x buffer

        self.local_x[self.local_slice_tile] = self.parameter_batch["x"][it]

        # * setup auxiliary buffers
        # ! communicating facet borders to neighbours already done in-place
        # ! with the direction convolution operator
        # ! Hx, Gx updated whenever buffer_Hx and buffer_Gx are
        buffer_Hx = np.empty(self.facet_size_adj)
        buffer_Hx[self.local_slice_conv_adj] = self.model.forward(self.local_x)

        buffer_Gx = np.empty((2, *self.tv_facet_size_adj))
        (
            buffer_Gx[tuple((0, *self.local_slice_tv_adj))],
            buffer_Gx[tuple((1, *self.local_slice_tv_adj))],
        ) = chunk_gradient_2d(self.local_x[self.local_slice_tv], self.islast)

        # communicate facet borders to neighbours (Hx, Gx)
        # ! Hx updated in place
        self.model.adjoint_communicator.update_borders(buffer_Hx)
        # ! Gx updated in place
        self.adjoint_tv_communicator.update_borders(buffer_Gx)

        # * PSGLA step-sizes
        # TODO: replace by self.hyperparameter_batch["..."][it] when
        # TODO - hyperparameters are updated
        stepsize_x = 0.99 / (
            self.max_sq_kernel / self.hyperparameters["rho1"]
            + 8 / self.hyperparameters["rho2"]
        )
        stepsize_z1 = 0.99 * self.hyperparameters["rho1"]
        stepsize_z2 = 0.99 * self.hyperparameters["rho2"]

        aux = {
            "Hx": buffer_Hx,
            "Gx": buffer_Gx,
            "stepsize_x": stepsize_x,
            "stepsize_z1": stepsize_z1,
            "stepsize_z2": stepsize_z2,
        }
        return aux


    def _compute_potential(self, aux, local_potential, global_potential):
        local_potential[0] = potential_function(
            self.observations,
            aux["Hx"][self.local_slice_conv_adj],
            aux["Gx"][tuple((np.s_[:], *self.local_slice_tv_adj))],
            self.parameter_batch["z1"],
            self.parameter_batch["u1"],
            self.parameter_batch["z2"],
            self.parameter_batch["u2"],
            self.hyperparameters["rho1"],
            self.hyperparameters["rho2"],
            self.hyperparameters["alpha1"],
            self.hyperparameters["alpha2"],
            self.hyperparameters["beta"],
        )

        self.model.comm.Reduce(
            [local_potential, MPI.DOUBLE],
            [global_potential, MPI.DOUBLE],
            op=MPI.SUM,
            root=0,
        )

        pass


    def _sample_step(self, iter_mc: int, current_iter: int, past_iter: int, aux):
        # notational shortcuts (for in-place assignments)
        # ? can be defined just once? (to be double checked)
        # ! Hx, Gx updated whenever buffer_Hx and buffer_Gx are
        Hx = aux["Hx"][self.local_slice_conv_adj]
        Gx = aux["Gx"][tuple((np.s_[:], *self.local_slice_tv_adj))]

        # sample image x (update local tile)
        grad_x = gradient_x(
            self.model,
            self.adjoint_tv_communicator,
            self.isfirst,
            self.islast,
            aux["Hx"],
            aux["Gx"],
            self.parameter_batch["z1"],
            self.parameter_batch["u1"],
            self.parameter_batch["z2"],
            self.parameter_batch["u2"],
            self.hyperparameters["rho1"],
            self.hyperparameters["rho2"],
            self.local_slice_tv,
            self.local_slice_tv_adj,
        )

        sample_x(
            self.local_x[self.local_slice_tile],
            aux["stepsize_x"],
            grad_x,
            self.local_rng,
        )
        self.parameter_batch["x"][current_iter] = self.local_x[self.local_slice_tile]

        # communicate borders of each facet to appropriate neighbours
        # ! synchronous case: need to update Hx and Gx for the next step
        # ! local_x updated in-place here (border communication involved in
        # ! direct operator)
        aux["Hx"][self.local_slice_conv_adj] = self.model.forward(self.local_x)
        # ! Hx and Gx updated in-place whenever buffer_Hx, buffer_Gx are
        (
            aux["Gx"][tuple((0, *self.local_slice_tv_adj))],
            aux["Gx"][tuple((1, *self.local_slice_tv_adj))],
        ) = chunk_gradient_2d(self.local_x[self.local_slice_tv], self.islast)

        # communicate borders of buffer_Hx, buffer_Gx (adjoint op) for the
        # next iteration
        self.model.adjoint_communicator.update_borders(aux["Hx"])
        self.adjoint_tv_communicator.update_borders(aux["Gx"])

        # * sample auxiliary variables (z1, u1)
        self.parameter_batch["z1"] = sample_z1(
            self.parameter_batch["z1"],
            self.observations,
            Hx,
            self.parameter_batch["u1"],
            self.hyperparameters["rho1"],
            aux["stepsize_z1"],
            self.local_rng,
        )
        self.parameter_batch["u1"] = sample_u(
            self.parameter_batch["z1"],
            Hx,
            self.hyperparameters["rho1"],
            self.hyperparameters["alpha1"],
            self.local_rng,
        )

        # * sample auxiliary variables (z2, u2)
        self.parameter_batch["z2"] = sample_z2(
            self.parameter_batch["z2"],
            Gx,
            self.parameter_batch["u2"],
            self.hyperparameters["rho2"],
            aux["stepsize_z2"],
            self.hyperparameters["beta"] * aux["stepsize_z2"],
            self.local_rng,
        )

        self.parameter_batch["u2"] = sample_u(
            self.parameter_batch["z2"],
            Gx,
            self.hyperparameters["rho2"],
            self.hyperparameters["alpha2"],
            self.local_rng,
        )

        pass
