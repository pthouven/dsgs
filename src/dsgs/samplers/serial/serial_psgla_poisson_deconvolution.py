"""Serial sampler for supervised deconvolution under Poisson noise based on a
TV prior. Version based on PSGLA kernels :cite:p:`Salim2020`.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from logging import Logger

import numpy as np
from numba import jit

from dsgs.functionals.prox import (
    kullback_leibler,
    l21_norm,
    prox_kullback_leibler,
    prox_l21norm,
    prox_nonegativity,
)
from dsgs.operators.jtv import gradient_2d, gradient_2d_adjoint
from dsgs.operators.linear_convolution import SerialConvolution
from dsgs.samplers.serial_sampler import SerialSampler
from dsgs.samplers.utils import sample_u
from dsgs.utils.checkpoint import SerialCheckpoint


def gradient_x(
    Hx,
    Gx,
    serial_conv_model: SerialConvolution,
    z1,
    u1,
    z2,
    u2,
    rho1: float,
    rho2: float,
):
    # ! in-place update for Hx and Gx
    # Hx and Gx already computed during the previous iteration: Hx and
    # Gx contain the right info in terms of those terms
    # local direct operator applied out of this function

    # local update step
    Hx1 = Hx + u1 - z1
    Hx1 /= rho1
    Gx1 = Gx + u2 - z2
    Gx1 /= rho2

    # local adjoint operator
    grad_x = serial_conv_model.adjoint(Hx1)
    grad_x += gradient_2d_adjoint(Gx1[0], Gx1[1])

    return grad_x


def sample_x(x, gamma_x: float, grad_x, rng: np.random.Generator):
    r"""PSGLA update step for the image :math:`x`.

    Parameters
    ----------
    x : numpy.ndarray
        Input image.
    gamma_x : float
        PSGLA step size (gradient and stochastic perturbation).
    grad_x : numpy.ndarray
        Gradient of the smooth part of the potential (minus log-posterior).
    rng : numpy.random.Generator
        Random number generator.
    """
    x1 = x + np.sqrt(2 * gamma_x) * rng.standard_normal(size=x.shape) - gamma_x * grad_x
    prox_nonegativity(x1)
    return x1


def sample_z1(z1, y, Hx, u1, rho1: float, gamma1: float, rng: np.random.Generator):
    r"""PSGLA update step for the auxiliary variable :math:`z_1`.

    Parameters
    ----------
    z1 : numpy.ndarray
        Current state of the splitting variable :math:`z_1`.
    y : numpy.ndarray
        Observed data.
    Hx : numpy.ndarray
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    u1 : numpy.ndarray
        Current state of the augmentation variable :math:`u_1`.
    rho1 : float
        AXDA parameter.
    gamma1 : float
        PSGLA step size (gradient and stochastic perturbation).
    rng : numpy.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for the splitting variable :math:`z_1`.
    """
    grad_z1 = (z1 - u1 - Hx) / rho1
    z = z1 + np.sqrt(2 * gamma1) * rng.standard_normal(size=z1.shape) - gamma1 * grad_z1
    return prox_kullback_leibler(z, y, lam=gamma1)


def sample_z2(
    z2,
    Gx,
    u2,
    rho2: float,
    gamma2: float,
    lambda_gamma2: float,
    rng: np.random.Generator,
):
    r"""PSGLA update step for the splitting variable :math:`z_2`.

    Parameters
    ----------
    z2 : numpy.ndarray
        Current state of the splitting variable :math:`z_2`.
    Gx : numpy.ndarray
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    u2 : numpy.ndarray
        Current state of the augmentation variable :math:`u_2`.
    rho2 : float
        AXDA splitting parameter.
    gamma2 : float
        PSGLA step size (gradient and stochastic perturbation).
    lambda_gamma2 : float
        PSGLA step size (gradient and stochastic perturbation) multiplied by
        the current value of the regularization parameter :math:`\lambda`.
    lambda_ : float
        Current value of the regularization parameter :math:`\lambda`.
    rng : numpy.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for the splitting variable :math:`z_2`.
    """
    grad_z2 = (z2 - u2 - Gx) / rho2
    z = z2 + np.sqrt(2 * gamma2) * rng.standard_normal(size=z2.shape) - gamma2 * grad_z2
    return prox_l21norm(z, lam=lambda_gamma2, axis=0)


@jit(nopython=True, cache=True)
def potential_function(
    y,
    Hx,
    Gx,
    z1,
    u1,
    z2,
    u2,
    rho1: float,
    rho2: float,
    alpha1: float,
    alpha2: float,
    beta: float,
):
    """Compute the current value of the potential (minus log-posterior) for
    the AXDA model.

    Parameters
    ----------
    y : numpy.ndarray
        Observed data.
    Hx : numpy.ndarray
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    Gx : numpy.ndarray
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    z1 : numpy.ndarray
        splitting variable :math:`z_1`.
    u1 : numpy.ndarray
        Auxiliary augmentation variable :math:`u_1`.
    z2 : numpy.ndarray
        splitting variable :math:`z_2`.
    u2 : numpy.ndarray
        Auxiliary augmentation variable :math:`u_2`.
    rho1 : float
        AXDA splitting parameter.
    rho2 : float
        AXDA splitting parameter.
    alpha1 : float
        AXDA augmentation parameter.
    alpha2 : float
        AXDA augmentation parameter.
    beta : float
        Value of the regularization parameter.

    Returns
    -------
    float
        Current potential value for the AXDA model.
    """
    potential = (
        kullback_leibler(z1, y)
        + np.sum(np.abs(Hx - z1 + u1) ** 2) / (2 * rho1)
        + np.sum((Gx - z2 + u2) ** 2) / (2 * rho2)
        + np.sum(np.abs(u1) ** 2) / (2 * alpha1)
        + np.sum(u2**2) / (2 * alpha2)
        + beta * l21_norm(z2)
    )

    return potential


@jit(nopython=True, cache=True)
def compute_terms_potential(y, Hx, Gx, z1, u1, z2, u2):
    """Compute the value of the potentials invovled in the AXDA model.

    Parameters
    ----------
    y : numpy.array
        Observed data.
    Hx : numpy.array
        Pre-computed result of the convolution operator applied to the current
        image :math:`x`.
    Gx : numpy.array
        Pre-computed result of the discrete gradient operator applied to the
        current image :math:`x`.
    z1 : numpy.array
        Splitting variable :math:`z_1`.
    u1 : numpy.array
        Auxiliary augmentation variable :math:`u_1`.
    z2 : numpy.array
        Splitting variable :math:`z_2`.
    u2 : numpy.array
        Auxiliary augmentation variable :math:`u_2`.
    rho1 : float
        AXDA splitting parameter.
    rho2 : float
        AXDA splitting parameter.
    alpha1 : float
        AXDA augmentation parameter.
    alpha2 : float
        AXDA augmentation parameter.
    beta : float
        Value of the regularization parameter.

    Returns
    -------
    float
        Current potential value for the AXDA model.
    """
    data_fidelity = kullback_leibler(z1, y)
    lrho1 = np.sum(np.abs(Hx - z1 + u1) ** 2) / 2
    lrho2 = np.sum((Gx - z2 + u2) ** 2) / 2
    lalpha1 = np.sum(np.abs(u1) ** 2) / 2
    lalpha2 = np.sum(u2**2) / 2
    prior = l21_norm(z2)

    return np.array([data_fidelity, lrho1, lrho2, lalpha1, lalpha2, prior], dtype="d")


class PsglaSGS(SerialSampler):
    def __init__(
        self,
        xmax,
        observations,
        model: SerialConvolution,
        hyperparameters,
        Nmc: int,
        seed: int,
        checkpointer: SerialCheckpoint,
        checkpointfreq: int,
        logger: Logger,
        warmstart_it=-1,
        warmstart=False,
        save_batch=False,
    ):
        data_size = model.data_size
        N = model.image_size
        parameter_sizes = {
            "x": N,
            "u1": data_size,
            "z1": data_size,
            "u2": (2, *N),
            "z2": (2, *N),
        }
        chunk_sizes = 5 * [None]  # chunk sizes for x, u1, z1, u2, z2
        checkpoint_select = len(parameter_sizes) * [
            np.s_[:]  # ! saving only last sample to disk for all these variables
        ]

        self.xmax = xmax

        super(PsglaSGS, self).__init__(
            observations,
            model,
            parameter_sizes,
            hyperparameters,
            Nmc,
            seed,
            checkpointer,
            checkpointfreq,
            checkpoint_select,
            chunk_sizes,
            logger,
            warmstart_it=warmstart_it,
            warmstart=warmstart,
            save_batch=save_batch,
            updated_hyperparameters=[],
        )

    def _initialize_parameters(self):
        # TODO: take fixed values for the hyperparameters (defined in the main)
        for key in self.parameter_batch.keys():
            self.parameter_batch[key][0] = self.rng.integers(
                0, high=self.xmax, size=self.parameter_sizes[key], endpoint=True
            ).astype(float)

        for key in self.hyperparameter_batch.keys():
            self.hyperparameter_batch[key][0] = self.hyperparameters[key]

        pass

    def _initialize_auxiliary_parameters(self, it: int):
        Hx = self.model.forward(self.parameter_batch["x"][it])
        Gx = np.empty((2, *self.model.image_size))
        (
            Gx[0],
            Gx[1],
        ) = gradient_2d(self.parameter_batch["x"][it])

        # PSGLA step-sizes
        # ! only if the hyperparameters are kept fixed
        stepsize_x = 0.99 / (
            np.max(np.abs(self.model.fft_kernel)) ** 2
            / self.hyperparameter_batch["rho1"][0]
            + 8 / self.hyperparameter_batch["rho2"][0]
        )
        stepsize_z1 = 0.99 * self.hyperparameter_batch["rho1"][0]
        stepsize_z2 = 0.99 * self.hyperparameter_batch["rho2"][0]

        return {
            "Hx": Hx,
            "Gx": Gx,
            "stepsize_x": stepsize_x,
            "stepsize_z1": stepsize_z1,
            "stepsize_z2": stepsize_z2,
        }

    def _compute_potential(self, aux, it):
        potential = potential_function(
            self.observations,
            aux["Hx"],
            aux["Gx"],
            self.parameter_batch["z1"][it],
            self.parameter_batch["u1"][it],
            self.parameter_batch["z2"][it],
            self.parameter_batch["u2"][it],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["alpha1"][0],
            self.hyperparameter_batch["alpha2"][0],
            self.hyperparameter_batch["beta"][0],
        )

        return potential

    def _sample_step(self, iter_mc, current_iter, past_iter, aux):

        # * sample image x
        grad_x = gradient_x(
            aux["Hx"],
            aux["Gx"],
            self.model,
            self.parameter_batch["z1"][past_iter],
            self.parameter_batch["u1"][past_iter],
            self.parameter_batch["z2"][past_iter],
            self.parameter_batch["u2"][past_iter],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["rho2"][0],
        )

        # ! beware in-place assignment: x_mc[current_iter] first properly
        # ! assigned to x_mc[past_iter] (in the previous iteration)
        self.parameter_batch["x"][current_iter] = sample_x(
            self.parameter_batch["x"][past_iter],
            aux["stepsize_x"],
            grad_x,
            self.rng,
        )

        # * sample auxiliary variables (z1, u1)
        aux["Hx"] = self.model.forward(self.parameter_batch["x"][current_iter])
        self.parameter_batch["z1"][current_iter] = sample_z1(
            self.parameter_batch["z1"][past_iter],
            self.observations,
            aux["Hx"],
            self.parameter_batch["u1"][past_iter],
            self.hyperparameter_batch["rho1"][0],
            aux["stepsize_z1"],
            self.rng,
        )
        self.parameter_batch["u1"][current_iter] = sample_u(
            self.parameter_batch["z1"][current_iter],
            aux["Hx"],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["alpha1"][0],
            self.rng,
        )

        # * sample auxiliary variables (z2, u2)
        (aux["Gx"][0], aux["Gx"][1]) = gradient_2d(
            self.parameter_batch["x"][current_iter]
        )

        self.parameter_batch["z2"][current_iter] = sample_z2(
            self.parameter_batch["z2"][past_iter],
            aux["Gx"],
            self.parameter_batch["u2"][past_iter],
            self.hyperparameter_batch["rho2"][0],
            aux["stepsize_z2"],
            aux["stepsize_z2"] * self.hyperparameter_batch["beta"][0],
            self.rng,
        )

        self.parameter_batch["u2"][current_iter] = sample_u(
            self.parameter_batch["z2"][current_iter],
            aux["Gx"],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["alpha2"][0],
            self.rng,
        )

        pass
