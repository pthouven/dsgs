"""Serial sampler for supervised deconvolution under Poisson noise based on a
TV prior. Version based on MYULA kernels :cite:p:`Durmus2018`.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from logging import Logger

import numpy as np
from numba import jit

from dsgs.functionals.prox import (
    kullback_leibler,
    prox_kullback_leibler,
    prox_nonegativity,
    prox_tv_chambolle,
)
from dsgs.operators.jtv import tv
from dsgs.operators.linear_convolution import SerialConvolution
from dsgs.samplers.serial_sampler import SerialSampler
from dsgs.samplers.transition_kernels.myula import MYULA
from dsgs.samplers.utils import sample_u
from dsgs.utils.checkpoint import SerialCheckpoint


def sample_x(
    cconv_model: SerialConvolution,
    u1,
    z1,
    u2,
    z2,
    u3,
    z3,
    rho1: float,
    rho2: float,
    rho3: float,
    rng: np.random.Generator,
):
    r"""Sample the main parameter of the model (image).

    Parameters
    ----------
    cconv_model : SerialConvolution
        Serial circular convolution model.
    u1 : numpy.ndarray
        Augmentation variable.
    z1 : numpy.ndarray
        Splitting variable.
    u2 : numpy.ndarray
        Augmentation variable.
    z2 : numpy.ndarray
        Splitting variable.
    u3 : numpy.ndarray
        Augmentation variable.
    z3 : numpy.ndarray
        Splitting variable.
    rho1 : float
        AXDA splitting parameter.
    rho2 : float
        AXDA splitting parameter.
    rho3 : float
        AXDA splitting parameter.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    x_ : numpy.ndarray
        New sample for the main variable.
    Hx : numpy.ndarray
        Result of the convolution operator applied to ``x_``.

    Note
    ----
    Sample the image after 0-padding (all the problem formulated with an image
    of the size of the full convolution.
    """
    cov = 1 / (np.abs(cconv_model.fft_kernel) ** 2 / rho1 + 1 / rho2 + 1 / rho3)
    Fx = (
        np.fft.rfftn(z1 - u1, cconv_model.data_size)
        * np.conj(cconv_model.fft_kernel)
        / rho1
    )
    Fx += np.fft.rfftn((z2 - u2) / rho2 + (z3 - u3) / rho3, cconv_model.data_size)
    Fx *= cov
    Fx += np.sqrt(0.5 * cov) * (
        rng.standard_normal(size=cconv_model.fft_kernel.shape)
        + 1j * rng.standard_normal(size=cconv_model.fft_kernel.shape)
    )
    x_ = np.fft.irfftn(Fx, cconv_model.data_size)
    Hx = cconv_model.forward(x_)
    return x_, Hx


def sample_z1(z1, y, Hx, u1, rho1: float, myula1: MYULA, rng: np.random.Generator):
    r"""Sample first splitting variable ``z1``.

    Parameters
    ----------
    z1 : numpy.ndarray
        Splitting variable.
    y : numpy.ndarray
        Input observations.
    Hx : numpy.ndarray
        Result of the convolution operator applied to current ``x``.
    u1 : numpy.ndarray
        Augmentation variable.
    rho1 : float
        Splitting parameter.
    gamma1 : float
        P-MYULA parameter.
    lambda1 : float
        P-MYULA parameter.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for ``z1``.
    """
    gradH1 = (z1 - u1 - Hx) / rho1
    proxH1 = prox_kullback_leibler(z1, y, lam=myula1.params[1])
    z = myula1.make_transition(z1, gradH1, proxH1, rng)
    return z


def sample_z2(
    z2,
    x,
    u2,
    rho2: float,
    beta_: float,
    myula2: MYULA,
    rng: np.random.Generator,
):
    r"""Sample second splitting variable ``z2``.

    Parameters
    ----------
    z2 : numpy.ndarray
        Splitting variable.
    x : numpy.ndarray
        Current image sample.
    u2 : numpy.ndarray
        Augmentation variable.
    rho2 : float
        Splitting parameter.
    gamma2 : float
        P-MYULA parameter.
    lambda2 : float
        P-MYULA parameter.
    beta_ : float
        Current value of the regularization parameter.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for ``z2``.
    """
    gradH2 = (z2 - u2 - x) / rho2
    # ! Condat with 1e4 iterations and Chambolle with 10 lead roughly to the
    # ! same order of magnitude for the reg. parameter later on
    # proxH2 = prox_tv_primal_dual(
    #     z2, 0.01, lam=beta_ * lambda2, tol=1e-5, max_iter=100, verbose=False, rho=1.99
    # )[0]
    proxH2 = prox_tv_chambolle(z2, lam=beta_ * myula2.params[1], tol=1e-5, max_iter=10)[
        0
    ]
    z = myula2.make_transition(z2, gradH2, proxH2, rng)
    return z


def sample_z3(z3, x, u3, rho3: float, myula3: MYULA, rng: np.random.Generator):
    r"""Sample third splitting variable ``z3``.

    Parameters
    ----------
    z3 : numpy.ndarray
        Splitting variable
    x : numpy.ndarray
        Current image sample.
    u3 : numpy.ndarray
        Augmentation variable.
    rho3 : float
        Splitting parameter.
    myula3 : dsgs.samplers.transition_kernels.myula.MYULA
        MYULA transition kernel.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for ``z3``.
    """
    gradH3 = (z3 - x - u3) / rho3
    proxH3 = z3.copy()
    prox_nonegativity(proxH3)
    z = myula3.make_transition(z3, gradH3, proxH3, rng)
    return z


# TODO: check if this needs to be adapted to be consistent with Matakos2013
@jit(nopython=True, cache=True)
def potential_function(
    y,
    Hx,
    x,
    z1,
    u1,
    z2,
    u2,
    z3,
    u3,
    rho1: float,
    rho2: float,
    rho3: float,
    alpha1: float,
    alpha2: float,
    alpha3: float,
    beta: float,
):
    """Compute the current value of the potential (minus log-posterior).

    Parameters
    ----------
    y : numpy.ndarray
        Input observations.
    Hx : numpy.ndarray
        Result of the convolution operator applied on ``x``.
    x : numpy.ndarray
        Current value of the image ``x``.
    z1 : numpy.ndarray
        Splitting variable 1 (convolution).
    u1 : numpy.ndarray
        Augmentation variable 1.
    z2 : numpy.ndarray
        Splitting variable 2 (TV).
    u2 : numpy.ndarray
        Augmentation variable 2.
    z3 : numpy.ndarray
        Splitting variable 3 (non-negativity).
    u3 : numpy.ndarray
        Augmentation variable 3.
    rho1 : float
        Splitting parameter.
    rho2 : float
        Splitting parameter.
    rho3 : float
        Splitting parameter.
    alpha1 : float
        Augmentation parameter.
    alpha2 : float
        Augmentation parameter.
    alpha3 : float
        Augmentation parameter.
    beta : float
        Current regularization parameter.

    Returns
    -------
    float
        Current value of the potential.
    """
    potential = (
        kullback_leibler(z1, y)
        + np.sum(np.abs(Hx - z1 + u1) ** 2) / (2 * rho1)
        + np.sum((x - z2 + u2) ** 2) / (2 * rho2)
        + np.sum((x - z3 + u3) ** 2) / (2 * rho3)
        + np.sum(np.abs(u1) ** 2) / (2 * alpha1)
        + np.sum(u2**2) / (2 * alpha2)
        + np.sum(u3**2) / (2 * alpha3)
        + beta * tv(z2)
    )

    return potential


@jit(nopython=True, cache=True)
def potentials_function(
    y,
    Hx,
    x,
    z1,
    u1,
    z2,
    u2,
    z3,
    u3,
):
    """Compute value of the terms involved in the minus log-posterior.

    Parameters
    ----------
    y : numpy.ndarray
        Input observations.
    Hx : numpy.ndarray
        Result of the convolution operator applied on ``x``.
    x : numpy.ndarray
        Current value of the image ``x``.
    z1 : numpy.ndarray
        Splitting variable 1 (convolution).
    u1 : numpy.ndarray
        Augmentation variable 1.
    z2 : numpy.ndarray
        Splitting variable 2 (TV).
    u2 : numpy.ndarray
        Augmentation variable 2.
    z3 : numpy.ndarray
        Splitting variable 3 (non-negativity).
    u3 : numpy.ndarray
        Augmentation variable 3.

    Returns
    -------
    numpy.ndarray
        Current value of the different terms involved in the potential.
    """
    data_fidelity = kullback_leibler(z1, y)
    lrho1 = np.sum(np.abs(Hx - z1 + u1) ** 2) / 2
    lrho2 = np.sum((x - z2 + u2) ** 2) / 2
    lrho3 = np.sum((x - z3 + u3) ** 2) / 2
    lalpha1 = np.sum(np.abs(u1) ** 2) / 2
    lalpha2 = np.sum(u2**2) / 2
    lalpha3 = np.sum(u3**2) / 2
    prior = tv(z2)

    return np.array(
        [data_fidelity, lrho1, lrho2, lrho3, lalpha1, lalpha2, lalpha3, prior],
        dtype="d",
    )


class MyulaSGS(SerialSampler):
    def __init__(
        self,
        xmax,
        observations,
        model: SerialConvolution,
        hyperparameters,
        Nmc: int,
        seed: int,
        checkpointer: SerialCheckpoint,
        checkpointfreq: int,
        logger: Logger,
        warmstart_it=-1,
        warmstart=False,
        save_batch=False,
    ):
        data_size = model.data_size
        N = model.image_size
        parameter_sizes = {
            "x": N,
            "u1": data_size,
            "z1": data_size,
            "u2": N,
            "z2": N,
            "u3": N,
            "z3": N,
        }
        chunk_sizes = 7 * [None]  # chunk sizes for x, u1, z1, u2, z2, u3, z3
        checkpoint_select = len(parameter_sizes) * [
            np.s_[:]  # ! saving only last sample to disk for all these variables
        ]

        self.xmax = xmax

        super(MyulaSGS, self).__init__(
            observations,
            model,
            parameter_sizes,
            hyperparameters,
            Nmc,
            seed,
            checkpointer,
            checkpointfreq,
            checkpoint_select,
            chunk_sizes,
            logger,
            warmstart_it=warmstart_it,
            warmstart=warmstart,
            save_batch=save_batch,
            updated_hyperparameters=[],
        )

    def _initialize_parameters(self):
        for key in self.parameter_batch.keys():
            self.parameter_batch[key][0] = self.rng.integers(
                0, high=self.xmax, size=self.parameter_sizes[key], endpoint=True
            ).astype(float)

        for key in self.hyperparameter_batch.keys():
            self.hyperparameter_batch[key][0] = self.hyperparameters[key]

        pass

    def _initialize_auxiliary_parameters(self, it: int):
        Hx = self.model.forward(self.parameter_batch["x"][it])

        return {
            "Hx": Hx,
            "myula1": MYULA(1 / self.hyperparameter_batch["rho1"][0]),
            "myula2": MYULA(1 / self.hyperparameter_batch["rho2"][0]),
            "myula3": MYULA(1 / self.hyperparameter_batch["rho3"][0]),
        }

    def _compute_potential(self, aux, it):
        potential = potential_function(
            self.observations,
            aux["Hx"],
            self.parameter_batch["x"][it],
            self.parameter_batch["z1"][it],
            self.parameter_batch["u1"][it],
            self.parameter_batch["z2"][it],
            self.parameter_batch["u2"][it],
            self.parameter_batch["z3"][it],
            self.parameter_batch["u3"][it],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["rho3"][0],
            self.hyperparameter_batch["alpha1"][0],
            self.hyperparameter_batch["alpha2"][0],
            self.hyperparameter_batch["alpha3"][0],
            self.hyperparameter_batch["beta"][0],
        )

        return potential

    def _sample_step(self, iter_mc, current_iter, past_iter, aux):
        # * sample image x
        self.parameter_batch["x"][current_iter], aux["Hx"] = sample_x(
            self.model,
            self.parameter_batch["u1"][past_iter],
            self.parameter_batch["z1"][past_iter],
            self.parameter_batch["u2"][past_iter],
            self.parameter_batch["z2"][past_iter],
            self.parameter_batch["u3"][past_iter],
            self.parameter_batch["z3"][past_iter],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["rho3"][0],
            self.rng,
        )

        # * sample auxiliary variables (z1, u1)
        self.parameter_batch["z1"][current_iter] = sample_z1(
            self.parameter_batch["z1"][past_iter],
            self.observations,
            aux["Hx"],
            self.parameter_batch["u1"][past_iter],
            self.hyperparameter_batch["rho1"][0],
            aux["myula1"],
            self.rng,
        )
        self.parameter_batch["u1"][current_iter] = sample_u(
            self.parameter_batch["z1"][current_iter],
            aux["Hx"],
            self.hyperparameter_batch["rho1"][0],
            self.hyperparameter_batch["alpha1"][0],
            self.rng,
        )

        # * sample auxiliary variables (z2, u2)
        self.parameter_batch["z2"][current_iter] = sample_z2(
            self.parameter_batch["z2"][past_iter],
            self.parameter_batch["x"][current_iter],
            self.parameter_batch["u2"][past_iter],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["beta"][0],
            aux["myula2"],
            self.rng,
        )

        self.parameter_batch["u2"][current_iter] = sample_u(
            self.parameter_batch["z2"][current_iter],
            self.parameter_batch["x"][current_iter],
            self.hyperparameter_batch["rho2"][0],
            self.hyperparameter_batch["alpha2"][0],
            self.rng,
        )

        # * sample auxiliary variables (z3, u3)
        self.parameter_batch["z3"][current_iter] = sample_z3(
            self.parameter_batch["z3"][past_iter],
            self.parameter_batch["x"][current_iter],
            self.parameter_batch["u3"][past_iter],
            self.hyperparameter_batch["rho3"][0],
            aux["myula3"],
            self.rng,
        )

        self.parameter_batch["u3"][current_iter] = sample_u(
            self.parameter_batch["z3"][current_iter],
            self.parameter_batch["x"][current_iter],
            self.hyperparameter_batch["rho3"][0],
            self.hyperparameter_batch["alpha3"][0],
            self.rng,
        )

        pass
