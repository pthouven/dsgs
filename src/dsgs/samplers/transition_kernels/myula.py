"""Implementation of MYULA transition kernel :cite:p:`Durmus2018`.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np

from dsgs.samplers.transition_kernels.base_kernel import BaseMCMCKernel


def default_myula_parameters(
    lipschitz_constant, stepsize_factor=0.5, regularization_factor=1.0
):
    r"""Default recommendations from :cite:p:`Durmus2018` to set the stepsize
    and the regularization parameters involved in the MYULA transition kernel.

    Parameters
    ----------
    lipschitz_constant : float
        Lipschitz constant :math:`L_f` of the smooth function :math:`f`.
    stepsize_factor : float, optional
        Multiplicative factor :math:`s \in (0, 1]` to adjust the stepsize
        :math:`\gamma`, by default 0.5. The resulting stepsize statisfies
        :math:`0 < \gamma = s\lambda (L_f \lambda + 1)^{-1}`. For the default
        choice :math:`\lambda = L_f^{-1}`, take
        :math:`\gamma \in [L_f^{-1} / 20,L_f^{-1}/4]`.
    regularization_factor : float, optional
        Multiplicative factor :math:`r \in (0, 1]` to adjust the Moreau-Yosida
        smoothing parameter :math:`\lambda = r L_f^{-1}`, by default 1.

    Returns
    -------
    params : numpy.ndarray
        Stepsize :math:`gamma` and regularization :math:`\lambda` of the MYULA
        kernel (in position 0 and 1, respectively).
    _c : numpy.ndarray
        Auxiliary constants invovled in the

    Raises
    ------
    ValueError
        The parameters ``stepsize_factor`` and ``regularization_factor``
        both need to be <= 1.
    """
    if np.minimum(stepsize_factor, regularization_factor) > 1:
        raise ValueError(
            "`stepsize_factor` and `regularization_factor` both need to be <= 1."
        )
    regularization = (
        regularization_factor / lipschitz_constant
    )  # np.minimum(..., 2), as in Vidal2019?
    stepsize = stepsize_factor / (lipschitz_constant + 1 / regularization)
    c1 = np.sqrt(2 * stepsize)
    c2 = stepsize / regularization
    return np.array([stepsize, regularization]), np.array([c1, c2])


class MYULA(BaseMCMCKernel):
    r"""MYULA transition kernel :cite:p:`Durmus2018` associated with a target
    density of the form

    .. math::
        \pi(x) \propto \exp( -f(x) - g(x)),

    with :math:`f \in \Gamma_0 (\mathbb{R}^N)` an :math:`L_f`-smooth function
    and :math:`g \in \Gamma_0 (\mathbb{R}^N)`.

    Attributes
    ----------
    stepsize_factor : float
        Stepsize factor :math:`s` reducing the default stepsize adopted for the
        MYULA kernel: :math:`\gamma = s\lambda (L_f \lambda + 1)^{-1}`. By
        default 0.5.
    regularization_factor : float
        Multiplicative factor :math:`r \in (0, 1]` to adjust the Moreau-Yosida
        smoothing parameter :math:`\lambda = r L_f^{-1}` of :math:`g`, by
        default 1.
    _c[0] : float
        Precomputed value ``stepsize / regularization``.
    _c[1] : float
        Precomputed value ``np.sqrt(2 * stepsize)``.
    """

    def __init__(
        self, lipschitz_constant, stepsize_factor=0.5, regularization_factor=1.0
    ):
        r"""Constructor

        Parameters
        ----------
        lipschitz_constant : float
            Lipschitz constant :math:`L_f` of :math:`\nabla f`.
        stepsize_factor : float, optional
            Stepsize factor :math:`s` reducing the default stepsize adopted for
            the MYULA kernel, by default 0.5
        regularization_factor : float, optional
            Multiplicative factor :math:`r \in (0, 1]` to adjust the
            Moreau-Yosida smoothing parameter :math:`\lambda = r L_f^{-1}` of
            :math:`g`, by default 1.
        """
        self.params, self._c = default_myula_parameters(
            lipschitz_constant,
            stepsize_factor=stepsize_factor,
            regularization_factor=regularization_factor,
        )
        self.stepsize_factor = stepsize_factor
        self.regularization_factor = regularization_factor

    def reset_lipschitz_constant(self, lipschitz_constant):
        """Update the value of the Lipschitz constant underlying the MYULA
        transition.

        Parameters
        ----------
        lipschitz_constant : float
            New value for the Lipschitz constant.
        """
        self.params, self._c = default_myula_parameters(
            lipschitz_constant,
            stepsize_factor=self.stepsize_factor,
            regularization_factor=self.regularization_factor,
        )

    def reset_params(self, stepsize, regularization):
        r"""Reset base parameters of the kernel to any specified value. To be
        used with care.

        Parameters
        ----------
        stepsize : float
            New stepsize parameter.
        regularization : float
            New regularization parameter.
        """
        self.params[0] = stepsize
        self.params[1] = regularization
        self._c[0] = np.sqrt(2 * stepsize)
        self._c[1] = stepsize / regularization

    def make_transition(self, current_state, gradf, prox_lambda_g, rng):
        r"""Application of the MYULA transition kernel

        .. math ::
            x^{(k+1)} = \big( 1 - \frac{\gamma}{\lambda} \big) x^{(k)} - \gamma
            \nabla f (x^{(k)}) + \frac{\gamma}{\lambda} \text{prox}_{\gamma g}
            (x^{(k)}) + \sqrt{2 \gamma} \xi^{(k+1)},

        with :math:`x^{(k)}` the current state at iteration :math:`k`,
        :math:`\lambda > 0` the regularization parameter and
        :math:`\gamma \in \big(0, \lambda (L_f \lambda + 1)^{-1} \big]` the
        stepsize. Typical values :math:`\lambda` and :math:`gamma` are

        .. math::
            \lambda = L_f^{-1}, or \lambda \in [5 L_f^{-1}, 10 L_f^{-1}],
            \quad
            \gamma \in \big[ \lambda \big( 5 (L_f\lambda + 1) \big)^{-1},
            \lambda (L_f\lambda + 1)^{-1} \big)

        Parameters
        ----------
        current_state : numpy.ndarray
            Current state of the variable.
        gradf : numpy.ndarray
            Gradient of the function :math:`f`.
        prox_lambda_g : numpy.ndarray
            Gradient of the function :math:`\gamma g`.
        rng : numpy.random.Generator
            Random number generator.

        Returns
        -------
        numpy.ndarray
            New state after MYULA transition.
        """

        return (
            (1 - self._c[1]) * current_state
            - self.params[0] * gradf
            + self._c[1] * prox_lambda_g
            + self._c[0] * rng.standard_normal(size=current_state.shape)
        )
