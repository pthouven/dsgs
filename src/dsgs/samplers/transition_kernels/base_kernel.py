"""Interface for the base (abstract) MCMC transition kernels.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from abc import ABC, abstractmethod


class BaseMCMCKernel(ABC):
    """Abstract implementation of a generic transition kernel.

    Attributes
    ----------
    params : numpy.ndarray
        Parameters upon which the implementation of the transition kernel
        depends, by default None.
    """

    def __init__(self, params=None):
        r"""BaseMCMCKernel constructor.

        Parameters
        ----------
        params : numpy.ndarray
            Parameters upon which the implementation of the transition kernel
            depends, by default None.
        """
        self.params = params

    def reset_params(self, params):
        r"""Reset base parameters of the kernel.

        Parameters
        ----------
        params : numpy.ndarray
            New state of the parameters upon which the implementation of the
            transition kernel depends.
        """
        self.params = params

    @abstractmethod
    def make_transition(self, current_state, rng):  # pragma: no cover
        r"""Implementation of the transition from a given ``current_state``.

        Parameters
        ----------
        current_state : numpy.ndarray
            Current state of the variable to be updated by the transition
            kernel.
        rng : numpy.Random.Generator
            Random number generator.

        Returns
        -------
        NotImplemented

        Note
        ----
        The method needs to be implemented in any class inheriting from
        BaseMCMCKernel.
        """
        return NotImplemented
