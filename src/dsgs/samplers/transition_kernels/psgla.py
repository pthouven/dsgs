"""Implementation of the PSGLA (or SPLA) transition kernel
:cite:p:`Salim2020`.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
from numba import jit

from dsgs.samplers.transition_kernels.base_kernel import BaseMCMCKernel


@jit(nopython=True, cache=True)
def psgla_mean(current_state, grad_f, stepsize):
    """Compute the mean of the PSGLA :cite:p:`Salim2020` transition kernel.

    Parameters
    ----------
    current_state : numpy.ndarray
        Current state of the Markiv chain.
    grad_f : numpy.ndarray
        Gradient of the potential ("neg-log-pdf") of the distribution to be
        sampled from.
    stepsize : float
        Stepsize adopted for the PSGLA transition kernel.

    Returns
    -------
    numpy.ndarray
        Mean if the Gaussian perturbation involved in PSGLA.

    Notes
    -----
    Function implemented separately to allow JIT compilation with `numba`.
    """
    return current_state - stepsize * grad_f


class PSGLA(BaseMCMCKernel):
    r"""PSGLA transition kernel :cite:p:`Salim2020` associated with a target
    density of the form

    .. math::
            \pi(x) \propto \exp( -f(x) - g(x)),

    with :math:`f \in \Gamma_0 (\mathbb{R}^N)` an :math:`L_f`-smooth function
    and :math:`g \in \Gamma_0 (\mathbb{R}^N)`.

    Attributes
    ----------
    stepsize : float
        Stepsize of the PSGLA kernel (should be strictly lower than
        :math:`L_f`, the Lipschitz constant of the smooth part of the log-potential).
    _c : float
        Precomputed value ``np.sqrt(2 * stepsize)``.
    """

    def __init__(self, stepsize):
        super(PSGLA, self).__init__(params=np.array([stepsize], dtype="d"))
        self._c = np.sqrt(2 * stepsize)

    def reset_params(self, stepsize):
        r"""Reset base parameters of the kernel.

        Parameters
        ----------
        stepsize : float
            New stepsize value.
        """
        self.params = np.array([stepsize], dtype="d")
        self._c = np.sqrt(2 * stepsize)

    def make_transition(self, current_state, grad_f, prox_g, rng):
        r"""Application of the SPLA transition kernel

        .. math ::
            x^{(k+1)} = \text{prox}_{\gamma g} \big( x^{(k)} - \gamma \nabla f
            (x^{(k)}) + \sqrt{2 \gamma} \xi^{(k+1)} \big),

        with :math:`x^{(k)}` the current state at iteration :math:`k`, and
        :math:`\gamma \in (0, L_f^{-1})` the
        stepsize.

        Parameters
        ----------
        current_state : numpy.ndarray
            Current state of the variable to be sampled.
        grad_f : numpy.ndarray
            Gradient of the smooth part of the potential.
        prox_g : function
            Function implementing the proximal operator of the non-smooth part
            of the potential (must admit a keyword argument ``lam`` for a
            multiplicative constant).
        rng : numpy.random.Generator
            Random number generator.

        Returns
        -------
        numpy.ndarray
            New state of the variable. (check in-place updates?)
        """
        x = psgla_mean(current_state, grad_f, self.params[0])
        x += self._c * rng.standard_normal(size=current_state.shape)
        return prox_g(x, lam=self.params[0])

    def make_transition_inplace(self, current_state, grad_f, prox_g, rng):
        r"""Application of the SPLA transition kernel (current state modified in
        place)

        .. math ::
            x^{(k+1)} = \text{prox}_{\gamma g} \big( x^{(k)} - \gamma \nabla f
            (x^{(k)}) + \sqrt{2 \gamma} \xi^{(k+1)} \big),

        with :math:`x^{(k)}` the current state at iteration :math:`k`, and
        :math:`\gamma \in (0, L_f^{-1})` the
        stepsize.

        Parameters
        ----------
        current_state : numpy.ndarray
            Current state of the variable to be sampled.
        grad_f : numpy.ndarray
            Gradient of the smooth part of the potential.
        prox_g : function
            In-place function implementing the proximal operator of the
            non-smooth part of the potential (must admit a keyword argument
            ``lam`` for a multiplicative constant).
        rng : numpy.random.Generator
            Random number generator.

        Returns
        -------
        numpy.ndarray
            New state of the variable. (check in-place updates?)
        """
        x = psgla_mean(current_state, grad_f, self.params[0])
        x += self._c * rng.standard_normal(size=current_state.shape)
        prox_g(x, lam=self.params[0])
        return x
