"""Abstract serial sampler class.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from logging import Logger
from time import perf_counter

import numpy as np
from numpy.random import default_rng
from tqdm import tqdm

from dsgs.operators.linear_operator import LinearOperator
from dsgs.samplers.base_sampler import BaseSampler
from dsgs.utils.checkpoint import SerialCheckpoint


class SerialSampler(BaseSampler):
    r"""Abstract serial sampler class.

    Attributes
    ----------
    observations : numpy.ndarray
        Observed data.
    model : LinearOperator
        Model object representing the measurement operator.
    hyperparameters : Dict
        Dictionary containing the value of the different hyperparameters.
    Nmc : int
        Number of Monte-Carlo iterations.
    seed : int
        Seed used to set the random number generator.
    checkpointer : BaseCheckpoint
        Serial checkpoint object.
    checkpointfreq : int
        Frequency at which checkpoints are saved to disk.
    checkpoint_select : List
        List of slices describing which elements of the checkpoint buffer
        are saved to disk.
    chunk_sizes : List
        Shape of the chunks saved into the .h5 checkpoint file.
    logger : Logger
        Logger object.
    warmstart_it : int, optional
        Iteration at which the sampler needs to be restarted, by default
        -1.
    warmstart : bool, optional
        Flag indicating whether the sampler needs to be restarted from a
        previous state, by default False.
    updated_hyperparameters : List, optional
        List of strings corrresponding to the name of hyperparameters
        updated along the Monte-Carlo iterations, by default [].
    save_batch : bool, optional
        Flag controlling if all the samples of the main parameter ``x``
        need to be saved to disk, by default False.
    parameter_batch : Dict
        Dictionary in which the model parameters updated by the algorithm are
        stored for checkpointing.
    hyperparameter_batch : Dict
        Dictionary in which the model hyperparameters are stored.

    Note
    ----
    The following virtual methods need to be implemented in any daughter class:

    - :py:meth:`dsgs.samplers.base_sampler.BaseSampler._initialize_parameters`
    - :py:meth:`dsgs.samplers.base_sampler.BaseSampler._initialize_auxiliary_parameters`
    - :py:meth:`dsgs.samplers.base_sampler.BaseSampler._compute_potential`
    - :py:meth:`dsgs.samplers.base_sampler.BaseSampler._sample_step`
    """

    def __init__(
        self,
        observations: np.ndarray,
        model: LinearOperator,
        parameter_sizes,
        hyperparameters,
        Nmc: int,
        seed: int,
        checkpointer: SerialCheckpoint,
        checkpointfreq: int,
        checkpoint_select,
        chunk_sizes,
        logger: Logger,
        warmstart_it=-1,
        warmstart=False,
        updated_hyperparameters=[],
        save_batch=False,
    ):
        """SerialSampler constructor.

        Parameters
        ----------
        observations : numpy.ndarray
            Observed data.
        model : LinearOperator
            Model object representing the measurement operator.
        hyperparameters : Dict
            Dictionary containing the value of the different hyperparameters.
        Nmc : int
            Number of Monte-Carlo iterations.
        seed : int
            Seed used to set the random number generator.
        checkpointer : BaseCheckpoint
            Serial checkpoint object.
        checkpointfreq : int
            Frequency at which checkpoints are saved to disk.
        checkpoint_select : List
            List of slices describing which elements of the checkpoint buffer
            are saved to disk.
        chunk_sizes : List
            Shape of the chunks saved into the .h5 checkpoint file.
        logger : Logger
            Logger object.
        warmstart_it : int, optional
            Iteration at which the sampler needs to be restarted, by default
            -1.
        warmstart : bool, optional
            Flag indicating whether the sampler needs to be restarted from a
            previous state, by default False.
        updated_hyperparameters : List, optional
            List of strings corrresponding to the name of hyperparameters
            updated along the Monte-Carlo iterations, by default [].
        save_batch : bool, optional
            Flag controlling if all the samples of the main parameter ``x``
            need to be saved to disk, by default False.
        """
        super(SerialSampler, self).__init__(
            observations,
            model,
            parameter_sizes,
            hyperparameters,
            Nmc,
            seed,
            checkpointer,
            checkpointfreq,
            checkpoint_select,
            chunk_sizes,
            logger,
            warmstart_it=warmstart_it,
            warmstart=warmstart,
            updated_hyperparameters=updated_hyperparameters,
        )

        self.save_batch = save_batch

    def _setup_rng(self):
        """Set the random number generator with the seed used to instantiate
        the object.
        """
        self.rng = default_rng(self.seed)
        pass

    def _load(self):
        """Load state of sampled variables from a checkpoint .h5 file."""

        # ! checkpoint_select indicates how the variables have been saved to
        # ! disk (full checkpoint, or only last element)
        select = self.checkpoint_select + (2 + len(self.hyperparameter_batch)) * [
            np.s_[-1]
        ]
        dic_var = self.checkpointer.load(
            self.warmstart_it,
            select,
            self.rng,
            *list(self.parameter_batch.keys()),
            *list(self.hyperparameter_batch.keys()),
            "potential",
            "iter",
        )

        for key in self.parameter_batch.keys():
            self.parameter_batch[key][-1] = dic_var[key]

        for key in self.hyperparameter_batch.keys():
            self.hyperparameter_batch[key][-1] = dic_var[key]

        pass

    def _save(self, iter_mc: int, counter: int, nsamples: int, potential):
        """Save current state of sampled variables and useful quantities to a
        checkpoint .h5 file.

        Parameters
        ----------
        iter_mc : int
            Current iteration index.
        counter : int
            Number of iterations since the last checkpoint.
        nsamples : int
            Usual number of iterations between two consecutive checkpoints.
        potential : numpy.ndarray
            Evolution of the log-posterior over the checkpoint window.
        """
        # ! if the full batch for the parameter "x" needs to be saved to disk
        chunk_sizes = []
        last_sample = {}
        if self.save_batch:
            last_sample.update({"x_batch": self.parameter_batch["x"]})
            # ! chunk size of x needs to be placed in first position (to be improved)
            chunk_sizes = [(1, *self.parameter_batch["x"].shape[1:])]

        # TODO: save MMSE, MAP and last sample only (not mean of squares)
        chunk_sizes = (
            chunk_sizes
            + 3 * self.chunk_sizes
            + (5 + len(self.hyperparameter_batch)) * [None]
        )  # last sample / MMSE / MAP + atime, asqtime, potential, iter, counter

        # last sample for warmstart
        for key in self.parameter_batch.keys():
            last_sample.update(
                {"{}".format(key): self.parameter_batch[key][nsamples - 1]}
            )

        # mean
        est_mmse = {}

        for key in self.parameter_batch.keys():
            est_mmse.update(
                {
                    "{}_m".format(key): np.mean(
                        self.parameter_batch[key][:nsamples], axis=0
                    )
                }
            )

        # MAP estimator (based on local batch)
        id_map = np.argmin(potential[:nsamples])
        est_map = {}
        for key in self.parameter_batch.keys():
            est_map.update({"{}_map".format(key): self.parameter_batch[key][id_map]})

        self.checkpointer.save(
            iter_mc + 1,
            chunk_sizes,
            rng=self.rng,
            **last_sample,
            **est_mmse,
            **est_map,
            **self.hyperparameter_batch,
            atime=self.atime,
            asqtime=self.asqtime,
            potential=potential[:nsamples],
            iter=iter_mc + 1,
            counter=counter,
        )

        pass

    def sample(self):
        """Run the sampler for the prescribed number of iterations."""

        counter = 0
        time_ = 0.0
        sqtime_ = 0.0

        aux, potential = self._initialize_sampler()

        self.logger.info(
            r"t: {0:1.3e} | obj[t]: {1:1.3e}".format(
                self.start_iter - 1,
                potential[
                    self._it
                ],  # ! _it: position of the intialization in the buffer
            )
        )

        pbar = tqdm(total=self.Nmc - 1, desc="Sampling", unit="it")
        pbar.update(self.start_iter)
        self.logger.info("Start serial sampler from t={}".format(self.start_iter))

        for iter_mc in range(self.start_iter, self.Nmc):
            counter += 1
            t_start = perf_counter()

            # notational shortcuts (for in-place assignments)
            past_iter = (iter_mc - 1) % self.checkpointfreq
            current_iter = iter_mc % self.checkpointfreq

            # update state of parameters
            self._sample_step(iter_mc, current_iter, past_iter, aux)

            # update timing
            t_stop = perf_counter()
            elapsed_time = t_stop - t_start
            time_ += elapsed_time
            sqtime_ += elapsed_time**2

            # evolution of the potential, :math:`-\log p(x | y)`
            # if np.mod(iter_mc, monitor_frequency) == 0:
            potential[current_iter] = self._compute_potential(
                aux, current_iter
            )  # ! to check (required for mml) (put potentials in aux if required)
            self.logger.info(
                r"t: {0:1.3e} | obj[t]: {1:1.3e}".format(
                    iter_mc, potential[current_iter]
                )
            )

            # * checkpoint
            if np.mod(iter_mc + 1, self.checkpointfreq) == 0:
                self.logger.info("Writing checkpoint")
                self.atime[0] = time_ / counter
                self.asqtime[0] = sqtime_ / counter
                self._save(iter_mc, counter, self.checkpointfreq, potential)
                time_ = 0.0
                sqtime_ = 0.0
                counter = 0

            pbar.update()

        # * finalize sampler
        pbar.close()

        # ! if Nmc is a multiple of checkpoint_frequency, do not trigger final
        # ! checkpoint, as it will be redundant with the previous one
        if np.mod(self.Nmc, self.checkpointfreq) > 0:
            self.logger.info("Writing checkpoint")
            self.atime[0] = time_ / counter
            self.asqtime[0] = sqtime_ / counter
            self._save(iter_mc, counter, self.Nmc % self.checkpointfreq, potential)

        pass
