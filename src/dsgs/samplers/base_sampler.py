"""Abstract base sampler class.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from abc import ABC, abstractmethod
from logging import Logger
from typing import Dict, List

import numpy as np

from dsgs.operators.linear_operator import LinearOperator
from dsgs.utils.checkpoint import BaseCheckpoint


class BaseSampler(ABC):
    r"""Base sampler class implementing part of the parameter initialization.

    Attributes
    ----------
    observations : numpy.ndarray
        Observed data.
    model : LinearOperator
        Model object representing the measurement operator.
    hyperparameters : Dict
        Dictionary containing the value of the different hyperparameters.
    Nmc : int
        Number of Monte-Carlo iterations.
    seed : int
        Seed used to set the random number generator.
    checkpointer : BaseCheckpoint
        Serial checkpoint object.
    checkpointfreq : int
        Frequency at which checkpoints are saved to disk.
    checkpoint_select : List
        List of slices describing which elements of the checkpoint buffer
        are saved to disk.
    chunk_sizes : List
        Shape of the chunks saved into the .h5 checkpoint file.
    logger : Logger
        Logger object.
    warmstart_it : int, optional
        Iteration at which the sampler needs to be restarted, by default
        -1.
    warmstart : bool, optional
        Flag indicating whether the sampler needs to be restarted from a
        previous state, by default False.
    updated_hyperparameters : List, optional
        List of strings corrresponding to the name of hyperparameters
        updated along the Monte-Carlo iterations, by default [].
    atime : numpy.ndarray
        Array containing the average runtime per iteration over a single
        checkpoint cycle.
    asqtime : numpy.ndarray
        Array containing the average squared runtime per iteration over
        a single checkpoint cycle. The value is used to compute the
        standard deviation of the runtime per iteration over the full
        chain.
    parameter_batch : Dict
        Dictionary containing the batch of parameters sampled over the
        course of the sampler.
    hyperparameter_batch : Dict
        Dictionary containing the batch of hyperparameters computed over
        the course of the sampler.
    start_iter : int
        Iteration from which the sampler is started.

    Note
    ----
        The following virtual methods need to be implemented in any daughter class:

        - :meth:`dsgs.samplers.base_sampler.BaseSampler._setup_rng`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._initialize_parameters`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._initialize_auxiliary_parameters`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._load`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._save`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._compute_potential`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler._sample_step`,
        - :meth:`dsgs.samplers.base_sampler.BaseSampler.sample`.
    """

    def __init__(
        self,
        observations: np.ndarray,
        model: LinearOperator,
        parameter_sizes: Dict,
        hyperparameters: Dict,
        Nmc: int,
        seed: int,
        checkpointer: BaseCheckpoint,
        checkpointfreq: int,
        checkpoint_select: List,
        chunk_sizes: List,
        logger: Logger,
        warmstart_it=-1,
        warmstart=False,
        updated_hyperparameters=[],
    ):
        """BaseSampler constructor.

        Parameters
        ----------
        observations : numpy.ndarray
            Observed data.
        model : LinearOperator
            Model object representing the measurement operator involed in the
            likelihood function.
        parameter_sizes : Dict[Tuple[int]]
            Dictionary containing the size of the parameters to be sampled.
        hyperparameters : Dict
            Dictionary containing the value of the (possibly fixed) scalar
            hyperparameters.
        Nmc : int
            Number of Monte-Carlo iterations.
        seed : int
            Seed used to set the random number generator.
        checkpointer : BaseCheckpoint
            Checkpointer object.
        checkpointfreq : int
            Frequency at which checkpoints are saved to disk.
        checkpoint_select : List
            List of slices describing which elements of the checkpoint buffer
            are saved to disk.
        chunk_sizes : List
            Shape of the chunks saved into the .h5 checkpoint file.
        logger : Logger
            Logger object.
        warmstart_it : int, optional
            Iteration at which the sampler needs to be restarted, by default
            -1.
        warmstart : bool, optional
            Flag indicating whether the sampler needs to be restarted from a
            previous state, by default False.
        updated_hyperparameters : List, optional
            List of strings corrresponding to the name of hyperparameters
            updated along the Monte-Carlo iterations, by default [].
        """
        self.observations = observations
        self.model = model
        self.parameter_sizes = parameter_sizes
        self.hyperparameters = hyperparameters
        self.Nmc = Nmc
        self.seed = seed
        self.checkpointer = checkpointer
        # checkpointfreq needs to be >= 2 (or need to handle checkpoints
        # differently)
        self.checkpointfreq = checkpointfreq
        self.checkpoint_select = checkpoint_select
        self.chunk_sizes = chunk_sizes
        self.logger = logger
        self.warmstart_it = warmstart_it
        self.warmstart = warmstart

        self.updated_hyperparameters = updated_hyperparameters
        self.fixed_hyperparameters = [
            k for k in (self.hyperparameters.keys() - self.updated_hyperparameters)
        ]

        # monitoring variables (iteration counter + timing)
        self.atime = np.zeros((1,), dtype="d")
        self.asqtime = np.zeros((1,), dtype="d")

        # batch of samples for checkpoint (dictionary)
        # ! batch quantiles to be integrated in the sampler (Kormode)
        # ! if warmstart, just provide the sizes in  parameters?
        self.parameter_batch = {}
        for key in parameter_sizes.keys():
            self.parameter_batch.update(
                {key: np.empty((self.checkpointfreq, *parameter_sizes[key]), dtype="d")}
            )

        self.hyperparameter_batch = {}
        # if self.fixed_hyperparameters:
        #     for key in hyperparameters.keys():
        #         self.hyperparameter_batch.update(
        #             {key: np.array([hyperparameters[key]], dtype="d")}
        #         )
        # else:
        #     for key in hyperparameters.keys():
        #         self.hyperparameter_batch.update(
        #             {key: np.empty((self.checkpointfreq,), dtype="d")}
        #         )

        for key in self.fixed_hyperparameters:
            self.hyperparameter_batch.update(
                {key: np.array([hyperparameters[key]], dtype="d")}
            )

        for key in self.updated_hyperparameters:
            self.hyperparameter_batch.update(
                {key: np.empty((self.checkpointfreq,), dtype="d")}
            )
        # ? idem, but only for sampled hyperparameters

    @abstractmethod
    def _setup_rng(self):  # pragma: no cover
        """Set the random number generator with the seed used to instantiate
        the object.

        Returns
        -------
        NotImplemented
        """
        # TODO: changes expected between serial and paralell versions
        return NotImplemented

    @abstractmethod
    def _initialize_parameters(self):  # pragma: no cover
        """Initialize all the sampled parameters.

        Returns
        -------
        NotImplemented
        """
        # TODO: specify a full function to update the parameters of interest

        return NotImplemented

    @abstractmethod
    def _initialize_auxiliary_parameters(self, it: int):  # pragma: no cover
        """Initialize and cache auxiliary variables reused multiple times in
        the sampler.

        Parameters
        ----------
        it : int
            Index in the parameter buffer from which the auxiliary parameters
            are initialized.

        Returns
        -------
        NotImplemented
        """
        # TODO: specify a full function to initialize auxiliary parameters of interest

        return NotImplemented

    def _initialize_sampler(self):
        """Initialize all the parameters from the sampler, either from a
        checkpoint file, or from scratch.

        Returns
        -------
        aux : dict
            Auxiliary variables cached for the next sampling steps.
        potential : numpy.ndarray
            Array storing the value of the potential along the iterations, with
            the first value initialized to the current potential value.
        potential_ : float
            Current value of the potential function.
        """
        potential = np.zeros((self.checkpointfreq,), dtype="d")
        self._setup_rng()
        if self.warmstart:
            self._load()
            self.start_iter = self.warmstart_it
            self._it = -1  # iteration from which auxiliary parameters are initialized
            aux = self._initialize_auxiliary_parameters(self._it)
            potential_ = self._compute_potential(aux, self._it)
            potential[-1] = potential_
        else:
            self._initialize_parameters()
            self.start_iter = 1
            self._it = 0
            aux = self._initialize_auxiliary_parameters(self._it)
            potential_ = self._compute_potential(aux, self._it)
            potential[0] = potential_
        return aux, potential

    @abstractmethod
    def _load(self):  # pragma: no cover
        """Load state of sampled variables from a checkpoint .h5 file.

        Returns
        -------
        NotImplemented.
        """
        return NotImplemented

    @abstractmethod
    def _save(
        self, iter_mc: int, counter: int, nsamples: int, potential
    ):  # pragma: no cover
        """Save current state of sampled variables and useful quantities to a
        checkpoint .h5 file.

        Parameters
        ----------
        iter_mc : int
            Current iteration index.
        counter : int
            Number of iterations since the last checkpoint.
        nsamples : int
            Usual number of iterations between two consecutive checkpoints.
        potential : numpy.ndarray
            Evolution of the log-posterior over the checkpoint window.

        Returns
        -------
        NotImplemented
        """
        return NotImplemented

    @abstractmethod
    def _compute_potential(self, aux, it: int):  # pragma: no cover
        """Compute the different terms involved in the log-posterior, with the
        value of the overall potential function.

        Parameters
        ----------
        aux : dict
            Auxiliary variables cached from the previous steps.
        it : int
            Index in the parameter buffer from which the auxiliary parameters
            are initialized.

        Returns
        -------
        NotImplemented
        """
        #
        # compute the different terms involved in the log-posterior, return
        # the sum and each separate term individually, using any pre-computed
        # auxiliary variables (avoid duplicate costly computations)
        #
        return NotImplemented

    @abstractmethod
    def _sample_step(
        self, iter_mc: int, current_iter: int, past_iter: int, aux
    ):  # pragma: no cover
        """Implementation of a single iteration of the sampler.

        Parameters
        ----------
        iter_mc : int
            Current iteration index.
        current_iter : int
            Current iteration (modulo checkpoint size).
        past_iter : int
            Previous iteration (modulo checkpoint size).
        aux : dict
            Auxiliary variables cached from the previous steps.

        Returns
        -------
        NotImplemented
        """
        return NotImplemented

    @abstractmethod
    def sample(self):  # pragma: no cover
        """Run the sampler for the prescribed number of iterations.

        Returns
        -------
        NotImplemented
        """
        # ! Changes between serial and parallel version
        # ! (some tasks only required on the root process)
        #
        # need auxiliary function for the kernel object to compute all the
        # quantitites they require (expression for gradients, prox operators, ...)
        #
        return NotImplemented
