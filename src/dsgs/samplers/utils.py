"""Utility function to sample from the conditional distribution of the
augmentation variable :math:`u` involved in the AXDA model :cite:p:`Vono2021`.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np


def sample_u(z, Hx, rho: float, alph: float, rng: np.random.Generator):
    r"""Sample augmentation variable :math:`u`.

    Parameters
    ----------
    z : numpy.ndarray
        Splitting variable
    Hx : numpy.ndarray
        Result of a splitting linear operator applied to the current image
        ``x``.
    rho : float
        AXDA splitting parameter.
    alph : float
        Augmentation parameter.
    rng : np.random.Generator
        Random number generator.

    Returns
    -------
    numpy.ndarray
        New state for the augmentation :math:`u` considered.
    """
    normalization_factor = np.sqrt(rho + alph)
    mu = alph * (z - Hx) / normalization_factor**2
    return mu + rng.standard_normal(size=mu.shape) * (
        np.sqrt(rho * alph) / normalization_factor
    )
