"""Abstract distributed sampler class using a Single Program Multiple Data
(SPMD) implementation.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

from abc import ABC, abstractmethod
from logging import Logger
from time import perf_counter

import numpy as np
from mpi4py import MPI
from numpy.random import SeedSequence, default_rng
from tqdm import tqdm


class BaseSPMDSampler(ABC):
    r"""Base SPMD sampler class.

    Implements part of the initialization process, the main sampling loop and
    the checkpointing mechanism.

    Attributes
    ----------
    observations : numpy.ndarray
        Observed data.
    model : LinearOperator
        Model object representing the measurement operator.
    parameter_sizes : Dict[Tuple[int]]
        Dictionary containing the size of the parameters to be sampled.
    hyperparameters : Dict
        Dictionary containing the value of the (possibly fixed) scalar
        hyperparameters.
    Nmc : int
        Number of Monte-Carlo iterations.
    seed : int
        Seed used to set the random number generator.
    checkpointer : DistributedCheckpoint
        Distributed checkpointer.
    checkpointfreq : int
        Frequency at which checkpoints are saved to disk.
    checkpoint_select : List
        List of slices describing which elements of the checkpoint buffer
        are saved to disk.
    chunk_sizes : List
        Shape of the chunks saved into the .h5 checkpoint file.
    logger : Logger
        Logger object.
    warmstart_it : int, optional
        Iteration at which the sampler needs to be restarted, by default
        -1.
    warmstart : bool, optional
        Flag indicating whether the sampler needs to be restarted from a
        previous state, by default False.
    updated_hyperparameters : List[string], optional
        List of strings corrresponding to the name of the hyperparameters
        updated along the Monte-Carlo iterations, by default [].
    atime : numpy.ndarray
        Array containing the average runtime per iteration over a single
        checkpoint cycle.
    asqtime : numpy.ndarray
        Array containing the average squared runtime per iteration over
        a single checkpoint cycle. The value is used to compute the
        standard deviation of the runtime per iteration over the full
        chain.
    parameter_batch : Dict
        Dictionary containing the batch of parameters sampled over the
        course of the sampler.
    hyperparameter_batch : Dict
        Dictionary containing the batch of hyperparameters computed over
        the course of the sampler.
    ncores : int
        Number of MPI process involved.
    rank : int
        Linear rank of the current MPI process.
    ndims : int
        Number of axes of the problem considered.
    grid_size : int
        Number of MPI processes along each axis of the N-d Cartesian grid of
        MPI processes considered.
    ranknd : numpy.ndarray[int]
        N-dimensional rank of the process in the N-d Cartesian grid of MPI
        processes considered.
    isfirst : numpy.ndarray[bool]
        Indicates whether the current process is the first one along each axis
        of the N-d grid of MPI processes considered.
    islast : numpy.ndarray[bool]
        Indicates whether the current process is the last one along each axis
        of the N-d grid of MPI processes considered.
    local_seed : int
        Seed used to initialize the random number generator on the current MPI
        process.
    local_rng : numpy.random.Generator
        Random number generator for the current MPI process.
    local_potential : numpy.ndarray
        Part of the potential computed on the current MPI process.
    global_potential : numpy.ndarray
        Value of the potential function (summing contributions from all workers
        ).
    start_iter : int
        Iteration index at which the sampler starts (larger than 1 if the
        sampler is restarted from a previous run).

    Note
    ----
    The following virtual methods need to be implemented in any daughter class:

    - :meth:`dsgs.samplers.spmd_sampler.BaseSPMDSampler._initialize_parameters`,
    - :meth:`dsgs.samplers.spmd_sampler.BaseSPMDSampler._initialize_auxiliary_parameters`,
    - :meth:`dsgs.samplers.spmd_sampler.BaseSPMDSampler._compute_potential`,
    - :meth:`dsgs.samplers.spmd_sampler.BaseSPMDSampler._sample_step`.
    """

    def __init__(
        self,
        observations,
        model,
        parameter_sizes,
        hyperparameters,
        Nmc: int,
        seed,
        checkpointer,
        checkpointfreq: int,
        checkpoint_select,
        chunk_sizes,
        logger: Logger,
        warmstart_it=-1,
        warmstart=False,
        updated_hyperparameters=[],
    ):
        """Constructor of the BaseSPMDSampler class.

        Parameters
        ----------
        observations : numpy.ndarray
            Observed data.
        model : LinearOperator
            Model object representing the measurement operator.
        parameter_sizes : Dict[Tuple[int]]
            Dictionary containing the size of the parameters to be sampled.
        hyperparameters : Dict
            Dictionary containing the value of the (possibly fixed) scalar
            hyperparameters.
        Nmc : int
            Number of Monte-Carlo iterations.
        seed : int
            Seed used to set the random number generator.
        checkpointer : DistributedCheckpoint
            Distributed checkpointer.
        checkpointfreq : int
            Frequency at which checkpoints are saved to disk.
        checkpoint_select : List
            List of slices describing which elements of the checkpoint buffer
            are saved to disk.
        chunk_sizes : List
            Shape of the chunks saved into the .h5 checkpoint file.
        logger : Logger
            Logger object.
        warmstart_it : int, optional
            Iteration at which the sampler needs to be restarted, by default
            -1.
        warmstart : bool, optional
            Flag indicating whether the sampler needs to be restarted from a
            previous state, by default False.
        updated_hyperparameters : List[string], optional
            List of strings corrresponding to the name of the hyperparameters
            updated along the Monte-Carlo iterations, by default [].
        """
        # ! local parameter sizes (needs to be computed before-hand, possibly in the model object)
        self.observations = observations
        self.model = model
        self.parameter_sizes = parameter_sizes  # dict with name and sizes of parameters
        self.hyperparameters = hyperparameters  # dict
        self.Nmc = Nmc
        self.seed = seed
        self.checkpointer = checkpointer
        self.checkpointfreq = checkpointfreq  # needs to be >= 2 (or need to handle checkpoints differently)
        self.checkpoint_select = checkpoint_select
        self.chunk_sizes = chunk_sizes  # size of the chunks to be save to .h5, to be provided as a dictionary (for parameters and hyperparameters)
        self.logger = logger
        self.warmstart_it = warmstart_it
        self.warmstart = warmstart

        # name of updated and fixed hyperparameters
        self.updated_hyperparameters = updated_hyperparameters
        self.fixed_hyperparameters = [
            k for k in (self.hyperparameters.keys() - self.updated_hyperparameters)
        ]

        # MPI-based info
        self.ncores = model.comm.Get_size()
        self.rank = model.comm.Get_rank()
        self.ndims = model.ndims
        self.grid_size = MPI.Compute_dims(self.ncores, self.ndims)

        # Cartesian topology communicator and nD rank
        self.ranknd = model.ranknd
        self.grid_size = np.array(self.grid_size, dtype="i")
        self.isfirst = self.ranknd == 0
        self.islast = self.ranknd == (self.grid_size - 1)

        # monitoring variables (iteration counter + timing)
        self.atime = np.zeros((1,), dtype="d")
        self.asqtime = np.zeros((1,), dtype="d")

        # batch of samples for checkpoint (dictionary)
        self.parameter_batch = {}
        for key in parameter_sizes.keys():
            if key == "x":  # ! batch only kept for the main variable x
                self.parameter_batch.update(
                    {
                        key: np.empty(
                            (self.checkpointfreq, *parameter_sizes[key]), dtype="d"
                        )
                    }
                )
            else:
                self.parameter_batch.update(
                    {key: np.empty(parameter_sizes[key], dtype="d")}
                )

        if self.rank == 0:
            self.hyperparameter_batch = {}
            # if self.fixed_hyperparameters:
            #     for key in hyperparameters.keys():
            #         self.hyperparameter_batch.update(
            #             {key: np.array([hyperparameters[key]], dtype="d")}
            #         )
            # else:
            #     for key in hyperparameters.keys():
            #         self.hyperparameter_batch.update(
            #             {key: np.empty((self.checkpointfreq,), dtype="d")}
            #         )

            for key in self.fixed_hyperparameters:
                self.hyperparameter_batch.update(
                    {key: np.array([hyperparameters[key]], dtype="d")}
                )
            for key in self.updated_hyperparameters:
                self.hyperparameter_batch.update(
                    {key: np.empty((self.checkpointfreq,), dtype="d")}
                )
        else:
            self.hyperparameter_batch = {}

        pass

    def _setup_rng(self):
        child_seed = None
        if self.rank == 0:
            ss = SeedSequence(self.seed)
            # Spawn off nworkers child SeedSequences to pass to child processes.
            child_seed = ss.spawn(self.ncores)
        self.local_seed = self.model.comm.scatter(child_seed, root=0)
        self.local_rng = default_rng(self.local_seed)
        pass

    @abstractmethod
    def _initialize_parameters(self):  # pragma: no cover
        """Initialize all the sampled parameters.

        Returns
        -------
        NotImplemented
        """
        # TODO: specify a full function to update the parameters of interest

        return NotImplemented

    @abstractmethod
    def _initialize_auxiliary_parameters(self, it: int):  # pragma: no cover
        """Initialize and cache auxiliary variables reused multiple times in
        the sampler.

        Parameters
        ----------
        it : int
            Index in the parameter buffer from which the auxiliary parameters
            are initialized.

        Returns
        -------
        NotImplemented
        """
        # TODO: specify a full function to initialize auxiliary parameters of interest

        return NotImplemented

    @abstractmethod
    def _compute_potential(
        self, aux, local_potential, global_potential
    ):  # pragma: no cover
        """Compute the different terms involved in the log-posterior, with the
        value of the overall potential function.

        Parameters
        ----------
        aux : dict
            Auxiliary variables cached from the previous steps.
        it : int
            Index in the parameter buffer from which the auxiliary parameters
            are initialized.
        local_potential : float
            Current value of the local contribution to the global potential.
        global_potential : float
            Value of the global potential (aggregated over all workers). Correct
            value only stored on root worker (0).

        Returns
        -------
        NotImplemented
        """
        #
        # compute the different terms involved in the log-posterior, return
        # the sum and each separate term individualy, using any pre-computed
        # auxiliary variables (avoid duplicate costly computations)
        #
        return NotImplemented

    @abstractmethod
    def _sample_step(
        self, iter_mc: int, current_iter: int, past_iter: int, aux
    ):  # pragma: no cover
        """Implementation of a single iteration of the sampler.

        Parameters
        ----------
        iter_mc : int
            Current iteration index.
        current_iter : int
            Current iteration (modulo checkpoint size).
        past_iter : int
            Previous iteration (modulo checkpoint size).
        aux : dict
            Auxiliary variables cached from the previous steps.

        Returns
        -------
        NotImplemented
        """
        return NotImplemented

    def _initialize_sampler(self):
        """Initialize all the parameters from the sampler, either from a
        checkpoint file, or from scratch.

        Returns
        -------
        aux : dict
            Auxiliary variables cached for the next sampling steps.
        potential : numpy.ndarray
            Array storing the value of the potential along the iterations, with
            the first value initialized to the current potential value.
        potential_ : float
            Current value of the potential function.
        """
        self.local_potential = np.empty((1), dtype="d")
        # self.local_potentials = np.empty((len(self.hyperparameters) + 1), dtype="d")
        self.start_iter = np.empty((1), dtype="i")
        if self.rank == 0:
            potential = np.zeros((self.checkpointfreq,), dtype="d")
            self.global_potential = np.empty((1), dtype="d")
            # self.global_potentials = np.empty((len(self.hyperparameters) + 1), dtype="d")
        else:
            potential = None
            self.global_potential = None
            # self.global_potentials = None

        self._setup_rng()

        if self.warmstart:
            self._load()
            self.start_iter[0] = self.warmstart_it
            _it = -1  # iteration from which aux. params are initialized
        else:
            self._initialize_parameters()
            self.start_iter[0] = 1
            _it = 0

        aux = self._initialize_auxiliary_parameters(_it)
        # ! computation of the potential implies communications
        self._compute_potential(aux, self.local_potential, self.global_potential)

        if self.rank == 0:
            potential[_it] = self.global_potential[0]

        return aux, potential

    def _load(self):
        """Load state of sampled variables from a checkpoint .h5 file."""
        # ! only estimator and last state of each variable has been saved to disk

        # ! checkpoint_select indicates how the variables have been saved to
        # ! disk (full checkpoint, or only last element)
        if self.rank == 0:
            select = self.checkpoint_select + (2 + len(self.hyperparameter_batch)) * [
                np.s_[-1]
            ]
            dic_var = self.checkpointer.load(
                self.warmstart_it,
                select,
                self.local_rng,
                *list(self.parameter_batch.keys()),
                *list(self.hyperparameter_batch.keys()),
                "potential",
                "iter",
            )
            for key in self.hyperparameter_batch.keys():
                self.hyperparameter_batch[key][-1] = dic_var[key]
        else:
            dic_var = self.checkpointer.load(
                self.warmstart_it,
                self.checkpoint_select,
                self.local_rng,
                *list(self.parameter_batch.keys()),
            )
            for key in self.hyperparameter_batch.keys():
                self.hyperparameter_batch[key] = 0.0

        for key in self.parameter_sizes.keys():
            if key == "x":  # ! batch only kept for the main variable x
                self.parameter_batch[key][-1] = dic_var[key]
            else:
                self.parameter_batch[key] = dic_var[key]
        pass

    def _save(self, iter_mc: int, counter: int, nsamples: int, potential):
        """Save current state of sampled variables and useful quantities to a
        checkpoint .h5 file (one file per process).

        Parameters
        ----------
        iter_mc : int
            Current iteration index.
        counter : int
            Number of iterations since the last checkpoint.
        nsamples : int
            Usual number of iterations between two consecutive checkpoints.
        potential : numpy.ndarray
            Evolution of the log-posterior over the checkpoint window.
        """
        # ! save last sample for all variables, compute MMSE and MAP for x only
        chunk_sizes = self.chunk_sizes + 2 * [self.chunk_sizes[0]]

        # last sample for warmstart
        last_sample = {}
        for key in self.parameter_batch.keys():
            if key == "x":
                # ! only samples from the main variable are kept in memory
                last_sample.update(
                    {"{}".format(key): self.parameter_batch[key][nsamples - 1]}
                )
            else:
                # ! for other parameters, only the last state is kept
                last_sample.update({"{}".format(key): self.parameter_batch[key]})

        # mean
        est_mmse = {}

        for key in ["x"]:  # self.parameter_batch.keys():
            est_mmse.update(
                {
                    "{}_m".format(key): np.mean(
                        self.parameter_batch[key][:nsamples], axis=0
                    )
                }
            )

        # MAP estimator (based on local batch)
        id_map = np.empty(1, dtype="i")
        if self.rank == 0:
            id_map[0] = np.argmin(potential[:nsamples])
        self.model.comm.Bcast([id_map, 1, MPI.INT], root=0)

        est_map = {}
        for key in ["x"]:
            est_map.update(
                {
                    "{}_map".format(key): np.mean(
                        self.parameter_batch[key][id_map], axis=0
                    )
                }
            )

        self.checkpointer.save(
            iter_mc + 1,
            chunk_sizes,
            rng=self.local_rng,
            mode="w",
            rdcc_nbytes=1024**2 * 200,  # 200 MB cache
            **last_sample,
            **est_mmse,
            **est_map,
        )

        if self.rank == 0:
            # saving hyperparameters, atime, asqtime, potential, iter, counter
            chunk_sizes_ = (len(self.hyperparameter_batch) + 5) * [None]
            self.checkpointer.save(
                iter_mc + 1,
                chunk_sizes_,
                **self.hyperparameter_batch,
                mode="a",
                rdcc_nbytes=1024**2 * 200,  # 200 MB cache
                atime=self.atime,
                asqtime=self.asqtime,
                potential=potential[:nsamples],
                iter=iter_mc + 1,
                counter=counter,
            )
        pass

    def sample(self):
        """Run the sampler for the prescribed number of iterations."""
        counter = 0
        time_ = 0.0
        sqtime_ = 0.0

        aux, potential = self._initialize_sampler()

        if self.rank == 0:
            self.logger.info(
                r"t: {0:1.3e} | obj[t]: {1:1.3e}".format(
                    self.start_iter[0] - 1,
                    self.global_potential[0],
                )
            )
            pbar = tqdm(total=self.Nmc - 1, desc="Sampling", unit="it")
            pbar.update(self.start_iter[0])
            # manager = enlighten.get_manager()
            # pbar = manager.counter(total=Nmc, desc="Sampling")
            self.logger.info("Start SPMD sampler from t={}".format(self.start_iter[0]))

            # auxiliary variables to measure timing (average + std) per
            # iteration
            counter = 0
            time_ = 0.0
            sqtime_ = 0.0
        else:
            # ! won't be used on other worker, only serve as placeholder for
            # ! the save function
            time_ = 0.0
            sqtime_ = 0.0
            counter = 1

        for iter_mc in range(self.start_iter[0], self.Nmc):
            if self.rank == 0:
                counter += 1
                t_start = perf_counter()

            # notational shortcuts (for in-place assignments)
            past_iter = (iter_mc - 1) % self.checkpointfreq
            current_iter = iter_mc % self.checkpointfreq

            # update state of parameters
            self._sample_step(iter_mc, current_iter, past_iter, aux)

            # update timing
            if self.rank == 0:
                t_stop = perf_counter()
                elapsed_time = t_stop - t_start
                time_ += elapsed_time
                sqtime_ += elapsed_time**2

            # evolution of the potential, :math:`-\log p(x | y)`
            # if np.mod(iter_mc, monitor_frequency) == 0:
            self._compute_potential(aux, self.local_potential, self.global_potential)

            if self.rank == 0:
                potential[current_iter] = self.global_potential[0]
                self.logger.info(
                    r"t: {0:1.3e} | obj[t]: {1:1.3e}".format(
                        iter_mc, self.global_potential[0]
                    )
                )

            # * checkpoint
            if np.mod(iter_mc + 1, self.checkpointfreq) == 0:
                if self.rank == 0:
                    self.logger.info("Writing checkpoint")
                    self.atime[0] = time_ / counter
                    self.asqtime[0] = sqtime_ / counter
                self._save(iter_mc, counter, self.checkpointfreq, potential)
                if self.rank == 0:
                    time_ = 0.0
                    sqtime_ = 0.0
                    counter = 0

            if self.rank == 0:
                pbar.update()

        # * finalize sampler
        if self.rank == 0:
            pbar.close()

        # ! if Nmc is a multiple of checkpoint_frequency, do not trigger the
        # ! final checkpoint (redundant with the previous one)
        if np.mod(self.Nmc, self.checkpointfreq) > 0:
            if self.rank == 0:
                self.logger.info("Writing checkpoint")
                self.atime[0] = time_ / counter
                self.asqtime[0] = sqtime_ / counter
            self._save(iter_mc, counter, self.Nmc % self.checkpointfreq, potential)

        pass
