# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../../src"))
import sphinx_rtd_theme

# -- Project information -----------------------------------------------------

project = "dsgs"
copyright = "2023, P.-A. Thouvenin, A. Repetti and P. Chainais"
author = "P.-A. Thouvenin, A. Repetti and P. Chainais"

# The full version, including alpha/beta/rc tags
release = "1.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # "sphinx.ext.autodoc",  # Core library for html generation from docstrings
    # "sphinx.ext.autosummary",  # Create neat summary tables
    "autoapi.extension",
    # "sphinx.ext.autodoc",  # Include documentation from docstrings
    "sphinx.ext.coverage",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx_rtd_theme",
    "sphinxcontrib.bibtex",
    "sphinxcontrib.apa",
    "sphinx.ext.mathjax",  # Render math via JavaScript
    "sphinx.ext.inheritance_diagram",  # Include inheritance diagrams
    "sphinx_copybutton",  # Add copy button to code sections from the doc
    # Official Sphinx extensions
    # https://www.sphinx-doc.org/en/master/usage/extensions/index.html
    # "sphinx.ext.autodoc",  # Include documentation from docstrings
    # "sphinx.ext.autosectionlabel",  # Allow reference sections using its title
    # "sphinx.ext.autosummary",  # Generate autodoc summaries
    # "sphinx.ext.coverage",  # Collect doc coverage stats
    # "sphinx.ext.doctest",  # Test snippets in the documentation
    # "sphinx.ext.duration",  # Measure durations of Sphinx processing
    # "sphinx.ext.extlinks",  # Markup to shorten external links
    # "sphinx.ext.githubpages",  # Publish HTML docs in GitHub Pages
    # "sphinx.ext.graphviz",  # Add Graphviz graphs
    # "sphinx.ext.ifconfig",  # Include content based on configuration
    # "sphinx.ext.imgconverter",  # A reference image converter using Imagemagick
    # "sphinx.ext.inheritance_diagram",  # Include inheritance diagrams
    # "sphinx.ext.intersphinx",  # Link to other projects’ documentation
    # "sphinx.ext.linkcode",  # Add external links to source code
    # "sphinx.ext.imgmath",  # Render math as images
    # "sphinx.ext.mathjax",  # Render math via JavaScript
    # "sphinx.ext.jsmath",  # Render math via JavaScript
    # "sphinx.ext.napoleon",  # Support for NumPy and Google style docstrings
    # "sphinx.ext.todo",  # Support for todo items # .. todo:: directive
    # "sphinx.ext.viewcode",  # Add links to highlighted source code
    # Non-official Sphinx extensions need to be installed
    # https://github.com/sphinx-contrib/
    # "sphinxcontrib.bibtex",  # Sphinx extension for BibTeX style citations
    # "sphinxcontrib.proof",  # Sphinx extension to typeset theorems, proofs
    # Non-official Sphinx extension for matplotlib plots
    # https://matplotlib.org/stable/api/sphinxext_plot_directive_api.html?highlight=plot_directive#module-matplotlib.sphinxext.plot_directive
    # "matplotlib.sphinxext.plot_directive",  # .. plot:: directive for plt.plot
]
# autosummary_generate = True  # Turn on sphinx.ext.autosummary

autoapi_dirs = ["../../src/dsgs"]
autoapi_options = [
    "members",
    "undoc-members",
    "private-members",
    "show-inheritance",
    "show-module-summary",
    "imported-members",
]
autodoc_typehints = "description"
autoapi_keep_files = True
autoapi_generate_api_docs = True
# autoapi_python_class_content = "both"  # Which docstring to insert into the content of a class.
autoapi_ignore = ["*/pnp_model/*", "*/utils/memory.py", "*/archive/*"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "_build",
    "_templates",
    "sandbox",
    "tests",
    "drafts",
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"  # "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []  # ["_static"]

# sphinx_rtd_theme html options
# html_theme_options = {
#     "logo_only": False,
#     "display_version": True,
#     "prev_next_buttons_location": "bottom",
#     "style_external_links": False,
#     # "vcs_pageview_mode": "",
#     # 'style_nav_header_background': 'white',
#     # Toc options
#     "collapse_navigation": True,
#     "sticky_navigation": True,
#     "navigation_depth": 2,
#     "includehidden": True,
#     "titles_only": False,
# }

# html_extra_path = ["../coverage_html_report"]

# -- Extension configuration -------------------------------------------------

# Bibliography
# sphinxcontrib-bibtex https://sphinxcontrib-bibtex.readthedocs.io/en/latest/usage.html
# sphinxcontrib-apa https://pypi.org/project/sphinxcontrib-apa/
bibtex_bibfiles = ["strings_all_ref.bib", "biblio.bib"]
bibtex_encoding = "utf-8-sig"
bibtex_default_style = "alpha"
bibtex_reference_style = "author_year"
# bibtex_reference_style = "alpha"  # alpha, plain , unsrt, and unsrtalpha

# matplotlib.sphinxext.plot_directive
# https://matplotlib.org/stable/api/sphinxext_plot_directive_api.html
# plot_include_source = True
# plot_html_show_source_link =
# plot_pre_code =
# plot_basedir =
# plot_formats =
# plot_html_show_formats =
# plot_rcparams =
# plot_apply_rcparams =
# plot_working_directory =
# plot_template =

# Napoleon settings
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_preprocess_types = False
napoleon_type_aliases = None
napoleon_attr_annotations = True
