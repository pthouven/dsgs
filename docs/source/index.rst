``dsgs`` documentation
======================

This library provides the Python implementation of a distributed split-Gibbs sampler (SGS) :cite:p:`Vono2019tsp` with specific applications to imaging inverse problems. The library currently contains codes to reproduce the experiments reported in :cite:p:`Thouvenin2023`.


.. toctree::
   :maxdepth: 1
   :caption: Installation

   setup
   biblio


.. toctree::
   :maxdepth: 1
   :caption: Development

   autoapi/index
   Gitlab repository<https://gitlab.cristal.univ-lille.fr/pthouven/dsgs>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
