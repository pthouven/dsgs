#!/bin/bash
# Run the proposed Split-Gibbs Sampler (SGS) over all datasets and
# number of MPI cores considered in the weak scaling experiment from
# Section 6.3.
#
# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

# * Notes
#
# ! Parameter passed as explicit command line arguments override those from
# ! the configuration .json file
#
# - Results from the proposed distributed SGS sampler are saved in a folder
#   named with the convention
#
# "<dpath>/<image_name>_M<max_intensity>_ks<kernel_size>_seed<seed>_n<ncores>",
#
# with:
#
# <dpath>           relative path to the synthetic datasets
# <image_name>      name of the ground truth image from which the dataset has
#                   been generated
# <max_intensity>   max. intensity of the ground truth image used
# <kernel_size>     size of the convolution kernel for the dataset considered
# <seed>            seed used to initialize the random number generator
#                   underlying the sampler
# <ncores>          number of MPI cores used for the proposed distributed SGS
#
# - The experiment corresponding to the configuration file
#   "house_M30_ks3.json", already run with the serial sampler from the
#   "strong_scaling_experiment.sh" script, is not repeated here.
#
# ! This script can only be run once the synthetic data have been generated
# ! (see "generate_data.sh")

# relative path where the datasets will be retrieved
dpath=data

# root of the path where the results will be saved
rpath=results

# summary results file
sampler=psgla           # sampler selected
restart=-1              # iteration index identifying the warmstart checkpoint
loadflag=''             # '--load' if loading checkpoint from disk, '' otherwise
rfile=final_results
results_text_filename=weak_scaling_metrics


# * configs/house512_M30_ks7.json, distributed sampler with 4 cores
configfile=configs/house512_M30_ks7.json
ncores=7

echo Weak scaling experiment: ${configfile}, ncores=${ncores};

# run distributed SGS sampler
checkpointname=checkpoint_n=${ncores}_
mpiexec -n ${ncores} python -m mpi4py ../../src/dsgs/main_spmd_poisson_deconvolution.py \
--config_file=${configfile} \
--checkpointname=${checkpointname} \
--sampler=${sampler} \
--dpath=${dpath} --rpath=${rpath} \
--verbose --restart=${restart} ${loadflag}

# compute and append estimation quality results to a single .txt file
checkpointname=checkpoint_n=${ncores}
mpiexec -n ${ncores} python -m mpi4py ../../src/dsgs/main_metrics_spmd.py \
--config_file=${configfile} \
--checkpointname=${checkpointname} \
--results_text_filename=${results_text_filename} \
--dpath=${dpath} \
--rpath=${rpath} --rfile=${rfile}


# * configs/house1022_M30_ks11.json, distributed sampler with 16 cores
configfile=configs/house1022_M30_ks11.json
ncores=16

echo Weak scaling experiment: ${configfile}, ncores=${ncores};

# run distributed SGS sampler
checkpointname=checkpoint_n=${ncores}_
mpiexec -n ${ncores} python -m mpi4py ../../src/dsgs/main_spmd_poisson_deconvolution.py \
--config_file=${configfile} \
--checkpointname=${checkpointname} \
--sampler=${sampler} \
--dpath=${dpath} --rpath=${rpath} \
--verbose --restart=${restart} ${loadflag}

# compute and append estimation quality results to a single .txt file
checkpointname=checkpoint_n=${ncores}
mpiexec -n ${ncores} python -m mpi4py ../../src/dsgs/main_metrics_spmd.py \
--config_file=${configfile} \
--checkpointname=${checkpointname} \
--results_text_filename=${results_text_filename} \
--dpath=${dpath} \
--rpath=${rpath} --rfile=${rfile}
