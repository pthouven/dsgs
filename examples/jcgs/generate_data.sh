#!/bin/bash
# Generate the synthetic datasets for all the experiments considered in Section
# 6.3.
#
# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

# ! Parameter passed as explicit command line arguments to the Python scripts
# ! override the same fields specified in the .json configuration file

# root path where the generated datasets are saved
dpath=data

# declare -a experiments_configs=("configs/tests/test_config.json")

for configfile in configs/*.json; do
# for configfile in "${experiments_configs[@]}"; do
    echo Generating dataset from ${configfile};
    python ../../src/dsgs/main_serial_poisson_deconvolution.py \
    --config_file=${configfile} --dpath=${dpath} \
    --verbose --data;
done
