"""Test basic MPI communications.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest
from mpi4py import MPI

import dsgs.utils.communications as ucomm

# pytestmark = pytest.mark.mpi


@pytest.fixture
def comm():
    return MPI.COMM_WORLD


@pytest.fixture
def size(comm):
    return comm.Get_size()


@pytest.fixture
def rank(comm):
    return comm.Get_rank()


def test_communication_1d(comm, rank, size):
    N = np.array([20, 20], dtype="i")  # overall image size
    M = np.array([2, 2], dtype="i")  # size of convolution kernel
    overlap_size = M - 1  # overlap size
    ndims = 2
    grid_size = [size, 1]

    # * create Cartesian topology communicator
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    grid_size = np.array(grid_size, dtype="i")
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype=int)

    local_tile = ucomm.local_split_range_nd(grid_size, N, ranknd)
    tile_size = local_tile[:, 1] - local_tile[:, 0] + 1
    local_indices = ucomm.local_split_range_nd(
        grid_size, N, ranknd, overlap=overlap_size
    )
    facet_size = local_indices[:, 1] - local_indices[:, 0] + 1
    facet = np.full(facet_size, rank + 1, dtype="i")

    # * defining custom types to communicate non-contiguous arrays in the
    # * directions considered
    sendsubarray = []
    recvsubarray = []
    resizedsendsubarray = []
    resizedrecvsubarray = []

    sizes = facet_size  # size of local array
    sM = sizes - overlap_size

    # * comm. along each dimension
    for k in range(ndims):
        if overlap_size[k] > 0:
            # send buffer
            subsizes = np.r_[sizes[:k], overlap_size[k], sizes[k + 1 :]]
            starts = np.r_[
                np.zeros(k, dtype="i"),
                sM[k],
                np.zeros(ndims - k - 1, dtype="i"),
            ]
            sendsubarray.append(
                MPI.INT.Create_subarray(sizes, subsizes, starts, order=MPI.ORDER_C)
            )
            resizedsendsubarray.append(
                sendsubarray[-1].Create_resized(0, overlap_size[k] * facet.itemsize)
            )
            resizedsendsubarray[-1].Commit()

            # recv buffer
            subsizes = np.r_[sizes[:k], overlap_size[k], sizes[k + 1 :]]
            starts = np.zeros(ndims, dtype="i")
            recvsubarray.append(
                MPI.INT.Create_subarray(sizes, subsizes, starts, order=MPI.ORDER_C)
            )
            resizedrecvsubarray.append(
                recvsubarray[-1].Create_resized(0, overlap_size[k] * facet.itemsize)
            )
            resizedrecvsubarray[-1].Commit()
        else:
            resizedsendsubarray.append(None)
            resizedrecvsubarray.append(None)

    # * rank of processes involved in the communications
    src = ndims * [MPI.PROC_NULL]
    dest = ndims * [MPI.PROC_NULL]

    for d in range(ndims):
        if overlap_size[d] > 0:
            [src[d], dest[d]] = cartcomm.Shift(d, 1)

    # * communications
    for d in range(ndims):
        comm.Sendrecv(
            [facet, 1, resizedsendsubarray[d]],
            dest[d],
            recvbuf=[facet, 1, resizedrecvsubarray[d]],
            source=src[d],
        )

    (
        dst,
        sr,
        isvalid_dest,
        isvalid_src,
        sizes_dest,
        sizes_src,
        start_src,
    ) = ucomm.isvalid_communication(ranknd, grid_size, overlap_size, N)

    s0 = np.zeros(ndims * (ndims - 1) + 1)

    for k in range(sizes_src.shape[0]):
        if isvalid_src[k]:
            sel = tuple(
                [
                    np.s_[start_src[k, d] : start_src[k, d] + sizes_src[k, d]]
                    for d in range(ndims)
                ]
            )
            s0[k] = np.sum(facet[sel])

    assert np.isclose(np.sum(facet), (rank + 1) * np.prod(tile_size) + np.sum(s0))

    # print("Worker {}: {}".format(rank, facet))

    comm.Barrier()
    for d in range(ndims):
        if overlap_size[d] > 0:
            resizedsendsubarray[d].Free()
            resizedrecvsubarray[d].Free()
    comm.Barrier()


def test_communication_2d(comm, size, rank):
    N = np.array([20, 20], dtype="i")  # overall image size
    M = np.array([2, 2], dtype="i")  # size of convolution kernel
    overlap_size = M - 1  # overlap size
    ndims = 2
    grid_size = MPI.Compute_dims(size, ndims)

    # * create Cartesian topology communicator
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    grid_size = np.array(grid_size, dtype="i")
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype=int)

    local_tile = ucomm.local_split_range_nd(grid_size, N, ranknd)
    tile_size = local_tile[:, 1] - local_tile[:, 0] + 1
    local_indices = ucomm.local_split_range_nd(
        grid_size, N, ranknd, overlap=overlap_size
    )
    facet_size = local_indices[:, 1] - local_indices[:, 0] + 1
    facet = np.full(facet_size, rank + 1, dtype="i")

    # * defining custom types to communicate non-contiguous arrays in the
    # directions considered
    sendsubarray = []
    recvsubarray = []
    resizedsendsubarray = []
    resizedrecvsubarray = []

    sizes = facet_size  # size of local array
    sM = sizes - overlap_size

    # * comm. along each dimension
    for k in range(ndims):
        if overlap_size[k] > 0:
            # send buffer
            subsizes = np.r_[sizes[:k], overlap_size[k], sizes[k + 1 :]]
            starts = np.r_[
                np.zeros(k, dtype="i"),
                sM[k],
                np.zeros(ndims - k - 1, dtype="i"),
            ]
            sendsubarray.append(
                MPI.INT.Create_subarray(sizes, subsizes, starts, order=MPI.ORDER_C)
            )
            resizedsendsubarray.append(
                sendsubarray[-1].Create_resized(0, overlap_size[k] * facet.itemsize)
            )
            resizedsendsubarray[-1].Commit()

            # recv buffer
            subsizes = np.r_[sizes[:k], overlap_size[k], sizes[k + 1 :]]
            starts = np.zeros(ndims, dtype="i")
            recvsubarray.append(
                MPI.INT.Create_subarray(sizes, subsizes, starts, order=MPI.ORDER_C)
            )
            resizedrecvsubarray.append(
                recvsubarray[-1].Create_resized(0, overlap_size[k] * facet.itemsize)
            )
            resizedrecvsubarray[-1].Commit()
        else:
            resizedsendsubarray.append(None)
            resizedrecvsubarray.append(None)

    # * rank of processes involved in the communications
    src = ndims * [MPI.PROC_NULL]
    dest = ndims * [MPI.PROC_NULL]

    for d in range(ndims):
        if overlap_size[d] > 0:
            [src[d], dest[d]] = cartcomm.Shift(d, 1)

    # * communications
    for d in range(ndims):
        comm.Sendrecv(
            [facet, 1, resizedsendsubarray[d]],
            dest[d],
            recvbuf=[facet, 1, resizedrecvsubarray[d]],
            source=src[d],
        )

    (
        dst,
        sr,
        isvalid_dest,
        isvalid_src,
        sizes_dest,
        sizes_src,
        start_src,
    ) = ucomm.isvalid_communication(ranknd, grid_size, overlap_size, N)

    s0 = np.zeros(ndims * (ndims - 1) + 1)

    for k in range(sizes_src.shape[0]):
        if isvalid_src[k]:
            sel = tuple(
                [
                    np.s_[start_src[k, d] : start_src[k, d] + sizes_src[k, d]]
                    for d in range(ndims)
                ]
            )
            s0[k] = np.sum(facet[sel])

    assert np.isclose(np.sum(facet), (rank + 1) * np.prod(tile_size) + np.sum(s0))

    comm.Barrier()
    for d in range(ndims):
        if overlap_size[d] > 0:
            resizedsendsubarray[d].Free()
            resizedrecvsubarray[d].Free()
    comm.Barrier()


# mpiexec -n 2 python -m unittest tests/test_communications.py
# mpiexec -n 2 python -m unittest tests/test_mpi.py
