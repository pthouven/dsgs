"""Test MPI model objects (SyncLinearConvolution).
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import h5py
import numpy as np
import pytest
from mpi4py import MPI

import dsgs.operators.convolutions as uconv
import dsgs.utils.communications as ucomm
from dsgs.operators.data import generate_2d_gaussian_kernel
from dsgs.operators.distributed_convolutions import create_local_to_global_slice
from dsgs.operators.linear_convolution import SyncLinearConvolution
from dsgs.utils.communicators import SyncCartesianCommunicator

pytestmark = pytest.mark.mpi


@pytest.fixture
def comm():
    return MPI.COMM_WORLD


@pytest.fixture
def size(comm):
    return comm.Get_size()


@pytest.fixture
def rank(comm):
    return comm.Get_rank()


@pytest.fixture
def seed():
    return 123


@pytest.fixture
def ndims():
    return 2


@pytest.fixture
def image_size():
    # overall image size
    return np.array([20, 20], dtype="i")


@pytest.fixture
def kernel_size():
    # size of convolution
    return np.array([4, 4], dtype="i")


@pytest.fixture
def overlap_size(kernel_size):
    # overlap size kernel
    return kernel_size - 1


@pytest.fixture
def kernel(kernel_size):
    # square convolution kernel
    return generate_2d_gaussian_kernel(kernel_size[0], 0.1)


def test_SyncCartesianCommunicator_throws_exceptions(
    comm, size, ndims, kernel, overlap_size
):
    """
    Check error triggered when ``grid_size`` and ``facet_size`` do not contain
    the same number of elements.
    """
    grid_size_ = (*MPI.Compute_dims(size, ndims),)
    grid_size = np.array(grid_size_, dtype="i")
    facet_size = np.array([], dtype="i")

    cartcomm = comm.Create_cart(
        dims=grid_size_,
        periods=ndims * [False],
        reorder=False,
    )

    with pytest.raises(ValueError) as excinfo:
        SyncCartesianCommunicator(
            comm,
            cartcomm,
            grid_size,
            kernel.itemsize,
            facet_size,
            overlap_size,
            direction=False,
        )
    assert (
        "`grid_size` and `facet_size` must contain the same number of element."
        in str(excinfo.value)
    )


def test_SyncLinearConvolution_throws_exceptions(
    comm, size, ndims, image_size, overlap_size, kernel, kernel_size
):
    """Testing errors thrown by the object."""
    data_size = image_size + overlap_size
    grid_size_ = MPI.Compute_dims(size, ndims)

    # checking number of elements in image_size is consistent with
    # data_size
    with pytest.raises(ValueError) as excinfo:
        SyncLinearConvolution(
            image_size[1:],
            data_size,
            kernel,
            comm,
            grid_size_,
            kernel.itemsize,
            False,
            direction=False,
        )
    assert "image_size and data_size must have the same number of elements" in str(
        excinfo.value
    )

    # checking kernel has a number of axis consistent with image_size and
    # data_size
    with pytest.raises(ValueError) as excinfo:
        SyncLinearConvolution(
            image_size,
            data_size,
            kernel[0, :],
            comm,
            grid_size_,
            kernel.itemsize,
            False,
            direction=False,
        )
    assert "kernel should have ndims = len(image_size) dimensions" in str(excinfo.value)

    # checking kernel contains real entries
    with pytest.raises(TypeError) as excinfo:
        SyncLinearConvolution(
            image_size,
            data_size,
            (1 + 1j) * kernel,
            comm,
            grid_size_,
            kernel.itemsize,
            False,
            direction=False,
        )
    assert "only real-valued kernel supported" in str(excinfo.value)


def test_SyncLinearConvolution_linear_2d_backward(
    comm, size, ndims, image_size, kernel, kernel_size, overlap_size
):
    """Testing 2d distributed linear convolution model with backward facet
    overlap."""
    data_size = image_size + overlap_size
    circular_boundaries = False
    backward = True
    grid_size_ = MPI.Compute_dims(size, ndims)
    grid_size = np.array(grid_size_, dtype="i")

    # * distributed convolution model
    conv_model = SyncLinearConvolution(
        image_size,
        data_size,
        kernel,
        comm,
        grid_size_,
        kernel.itemsize,
        circular_boundaries,
        direction=backward,
    )

    assert conv_model.direct_communicator.removed is False

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, conv_model.ranknd, backward=backward
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    # local_data_size, facet_size, facet_size_adj = calculate_local_data_size(
    #     tile_size, conv_model.ranknd, overlap_size, grid_size, backward=backward
    # )

    # facet size (convolution)
    facet_pixels = ucomm.local_split_range_nd(
        grid_size,
        image_size,
        conv_model.ranknd,
        overlap=overlap_size,
        backward=backward,
    )
    facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
    offset = facet_size - tile_size

    # * setup useful slices
    local_slice_tile = ucomm.get_local_slice(
        conv_model.ranknd, grid_size, offset, backward=backward
    )  # extract tile from local conv facet
    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels,
        conv_model.ranknd,
        overlap_size,
        conv_model.local_data_size,
        backward=backward,
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local image and kernel
    local_image = np.empty(conv_model.facet_size, dtype="d")
    local_image[local_slice_tile] = local_rng.standard_normal(size=tile_size)

    # * applying distributed direct operator
    local_data = conv_model.forward(local_image)

    # * save to an h5 test file (parallel writing)
    f = h5py.File("convolution_test.h5", "w", driver="mpio", comm=comm)

    dset_image = f.create_dataset("x", image_size, dtype="d")
    dset_image[global_slice_tile] = local_image[local_slice_tile]

    dset_data = f.create_dataset("y", data_size, dtype="d")
    dset_data[global_slice_data] = local_data

    dset_kernel = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        dset_kernel[:] = kernel
    f.close()
    del f, dset_image, dset_data, dset_kernel

    # * compare to full convolution
    g = h5py.File("convolution_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.fft.rfftn(h0, data_size)
        y = uconv.fft_conv(x0, H0, data_size)
        # print(np.allclose(y, y0))
        assert np.allclose(y, y0)
    g.close()


def test_SyncLinearConvolution_linear_2d_forward(
    comm, size, ndims, image_size, kernel, kernel_size, overlap_size
):
    """Testing 2d distributed linear convolution model with forward facet
    overlap."""
    data_size = image_size + overlap_size
    circular_boundaries = False
    backward = False
    grid_size_ = MPI.Compute_dims(size, ndims)
    grid_size = np.array(grid_size_, dtype="i")

    # * distributed convolution model
    conv_model = SyncLinearConvolution(
        image_size,
        data_size,
        kernel,
        comm,
        grid_size_,
        kernel.itemsize,
        circular_boundaries,
        direction=backward,
    )

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, conv_model.ranknd, backward=backward
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    # local_data_size, facet_size, facet_size_adj = calculate_local_data_size(
    #     tile_size, conv_model.ranknd, overlap_size, grid_size, backward=backward
    # )

    # facet size (convolution)
    facet_pixels = ucomm.local_split_range_nd(
        grid_size,
        image_size,
        conv_model.ranknd,
        overlap=overlap_size,
        backward=backward,
    )
    facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
    offset = facet_size - tile_size

    # * setup useful slices
    local_slice_tile = ucomm.get_local_slice(
        conv_model.ranknd, grid_size, offset, backward=backward
    )  # extract tile from local conv facet
    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels,
        conv_model.ranknd,
        overlap_size,
        conv_model.local_data_size,
        backward=backward,
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local image and kernel
    local_image = np.empty(conv_model.facet_size, dtype="d")
    local_image[local_slice_tile] = local_rng.standard_normal(size=tile_size)

    # * applying distributed direct operator
    local_data = conv_model.forward(local_image)

    # * save to an h5 test file (parallel writing)
    f = h5py.File("convolution_test.h5", "w", driver="mpio", comm=comm)

    dset_image = f.create_dataset("x", image_size, dtype="d")
    dset_image[global_slice_tile] = local_image[local_slice_tile]

    dset_data = f.create_dataset("y", data_size, dtype="d")
    dset_data[global_slice_data] = local_data

    dset_kernel = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        dset_kernel[:] = kernel
    f.close()
    del f, dset_image, dset_data, dset_kernel

    # * compare to full convolution
    g = h5py.File("convolution_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.fft.rfftn(h0, data_size)
        y = uconv.fft_conv(x0, H0, data_size)
        # print(np.allclose(y, y0))
        assert np.allclose(y, y0)
    g.close()


def test_adjoint_distributed_convolution_backward(
    comm, size, ndims, image_size, kernel, kernel_size, overlap_size
):
    """Testing adjoint distributed convolution with backward facet
    overlap for the direct operator."""
    data_size = image_size + overlap_size
    circular_boundaries = False
    backward = True
    grid_size_ = MPI.Compute_dims(size, ndims)
    grid_size = np.array(grid_size_, dtype="i")

    # * distributed convolution model
    conv_model = SyncLinearConvolution(
        image_size,
        data_size,
        kernel,
        comm,
        grid_size_,
        kernel.itemsize,
        circular_boundaries,
        direction=backward,
    )

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, conv_model.ranknd, backward=backward
    )
    # tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # * setup useful slices
    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels,
        conv_model.ranknd,
        overlap_size,
        conv_model.local_data_size,
        backward=backward,
    )
    # indexing into local data
    # ! for direct operator with backward overlap, adjoint operator has
    # ! forward overlap (local slice data has forward overlap here)
    local_slice_data = ucomm.get_local_slice(
        conv_model.ranknd, grid_size, conv_model.offset_adj, backward=not backward
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local data and kernel
    facet_adj = np.empty(conv_model.facet_size_adj, dtype="d")
    facet_adj[local_slice_data] = local_rng.standard_normal(
        size=conv_model.local_data_size
    )

    # * apply distributed adjoint operator
    local_x = conv_model.adjoint(facet_adj)

    # * save to an h5 test file (parallel writing)
    f = h5py.File("2d_convolution_adj_test.h5", "w", driver="mpio", comm=comm)

    # TODO to be generalized to nD
    dset = f.create_dataset("x", image_size, dtype="d")
    dset[global_slice_tile] = local_x

    yh5 = f.create_dataset("y", data_size, dtype="d")
    yh5[global_slice_data] = facet_adj[local_slice_data]

    hh5 = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        hh5[:] = kernel
    f.close()  # ! flush done automatically when closing the file
    comm.Barrier()

    # * test parallel load convolution result
    g = h5py.File(
        "2d_convolution_adj_test.h5", "r+", driver="mpio", comm=MPI.COMM_WORLD
    )
    dset = g["x"]
    loaded_x = np.zeros(local_x.shape)
    dset.read_direct(
        loaded_x,
        global_slice_tile,
        (np.s_[:], np.s_[:]),
    )

    # * debugging section
    # comm.Barrier()
    # print(
    #     "Process {}: |local_x - loaded_x| = {}".format(
    #         rank, np.linalg.norm(local_x - loaded_x)
    #     )
    # )
    g.close()

    # * compare to full convolution
    g = h5py.File("2d_convolution_adj_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.conj(np.fft.rfftn(h0, data_size))
        x = uconv.fft_conv(y0, H0, data_size)[: image_size[0], : image_size[1]]
        consistency_test = np.allclose(x0, x)
        # print("Consistency serial / parallel convolution: {}".format(consistency_test))
        assert consistency_test
    g.close()


def test_adjoint_distributed_convolution_forward(
    comm, size, ndims, image_size, kernel, kernel_size, overlap_size
):
    """Testing adjoint distributed convolution with backward facet
    overlap for the direct operator."""
    data_size = image_size + overlap_size
    circular_boundaries = False
    backward = False
    grid_size_ = MPI.Compute_dims(size, ndims)
    grid_size = np.array(grid_size_, dtype="i")

    # * distributed convolution model
    conv_model = SyncLinearConvolution(
        image_size,
        data_size,
        kernel,
        comm,
        grid_size_,
        kernel.itemsize,
        circular_boundaries,
        direction=backward,
    )

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, conv_model.ranknd, backward=backward
    )
    # tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # * setup useful slices
    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels,
        conv_model.ranknd,
        overlap_size,
        conv_model.local_data_size,
        backward=backward,
    )
    # indexing into local data
    # ! for direct operator with forward overlap, adjoint operator has
    # ! backward overlap
    local_slice_data = ucomm.get_local_slice(
        conv_model.ranknd, grid_size, conv_model.offset_adj, backward=not backward
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local data and kernel
    facet_adj = np.empty(conv_model.facet_size_adj, dtype="d")
    facet_adj[local_slice_data] = local_rng.standard_normal(
        size=conv_model.local_data_size
    )

    # * apply distributed adjoint operator
    local_x = conv_model.adjoint(facet_adj)

    # * save to an h5 test file (parallel writing)
    f = h5py.File("2d_convolution_adj_test.h5", "w", driver="mpio", comm=comm)

    # TODO to be generalized to nD
    dset = f.create_dataset("x", image_size, dtype="d")
    dset[global_slice_tile] = local_x

    yh5 = f.create_dataset("y", data_size, dtype="d")
    yh5[global_slice_data] = facet_adj[local_slice_data]

    hh5 = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        hh5[:] = kernel
    f.close()  # ! flush done automatically when closing the file
    comm.Barrier()

    # * test parallel load convolution result
    g = h5py.File(
        "2d_convolution_adj_test.h5", "r+", driver="mpio", comm=MPI.COMM_WORLD
    )
    dset = g["x"]
    loaded_x = np.zeros(local_x.shape)
    dset.read_direct(
        loaded_x,
        global_slice_tile,
        (np.s_[:], np.s_[:]),
    )

    # * debugging section
    # comm.Barrier()
    # print(
    #     "Process {}: |local_x - loaded_x| = {}".format(
    #         rank, np.linalg.norm(local_x - loaded_x)
    #     )
    # )
    g.close()

    # * compare to full convolution
    g = h5py.File("2d_convolution_adj_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.conj(np.fft.rfftn(h0, data_size))
        x = uconv.fft_conv(y0, H0, data_size)[: image_size[0], : image_size[1]]
        consistency_test = np.allclose(x0, x)
        # print("Consistency serial / parallel convolution: {}".format(consistency_test))
        assert consistency_test
    g.close()


# mpiexec -n 2 python -m pytest tests/mpi/test_mpi_operators.py
