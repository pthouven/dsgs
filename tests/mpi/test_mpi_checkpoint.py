"""Test distributed checkpoint functionalities.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest
from mpi4py import MPI

import dsgs.utils.communications as ucomm
from dsgs.operators.data import generate_2d_gaussian_kernel
from dsgs.utils.checkpoint import DistributedCheckpoint

pytestmark = pytest.mark.mpi


@pytest.fixture
def comm():
    return MPI.COMM_WORLD


# ! tmp_path seems to hang forever in MPI tests: why?
@pytest.fixture
def checkpointer(comm):  # tmp_path
    return DistributedCheckpoint(
        comm,
        "mpi_checkpoint_test",  # tmp_path / "mpi_checkpoint_test",
        cname="gzip",
        clevel=5,
        shuffle=1,
    )


@pytest.fixture
def local_rng(comm):
    rank = comm.Get_rank()
    size = comm.Get_size()
    if rank == 0:
        ss = np.random.SeedSequence(1234)
        # Spawn off nworkers child SeedSequences to pass to child processes.
        child_seed = np.array(ss.spawn(size))
    else:
        child_seed = None

    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)
    return local_rng


@pytest.fixture
def overlap_size():
    return np.array([2, 2], dtype="i")


@pytest.fixture
def kernel():
    return generate_2d_gaussian_kernel(3, 0.1)


def test_distributed_checkpoint_rng(checkpointer, local_rng):
    root_process = 0
    fileid = "1"
    rank = checkpointer.comm.Get_rank()

    # * backup for rng state
    checkpointer.save(fileid, [None], [None], [None], rng=local_rng)
    a = local_rng.normal()

    # print("Process {}: a={}".format(rank, a))

    # * load (warm-start for rng state)
    checkpointer.load(fileid, [None], local_rng)

    b = local_rng.normal()

    # print("Process {}: a={}, b={}, b=a? {}".format(rank, a, b, np.allclose(b, a)))

    # Check a=b on all processes
    local_consistency_check = np.array([np.allclose(b, a)])
    global_consistency_check = np.array([False])
    assert local_consistency_check[0]

    # Reduce "local_consistency_check" on the root
    checkpointer.comm.Reduce(
        [local_consistency_check, MPI.C_BOOL],
        [global_consistency_check, MPI.C_BOOL],
        op=MPI.LAND,
        root=root_process,
    )

    if rank == root_process:
        assert global_consistency_check


def test_distributed_checkpoint_from_root(checkpointer, local_rng, kernel):
    root_process = 0
    fileid = "_from_root"
    rank = checkpointer.comm.Get_rank()

    # save data from root process only
    if rank == root_process:
        a = 3
    else:
        a = None

    checkpointer.save_from_process(
        root_process,
        fileid,
        2 * [np.s_[:]],
        [None, None],
        rng=local_rng,
        kernel=kernel,
        a=a,
        mode="w",
    )

    if rank == root_process:
        b = local_rng.standard_normal(2)

    loaded_values = checkpointer.load_from_process(
        root_process,
        fileid,
        2 * [np.s_[:]],
        local_rng,
        "kernel",
        "a",
    )

    if rank == root_process:
        b2 = local_rng.standard_normal(2)
        assert np.isclose(a, loaded_values["a"])
        assert np.allclose(b2, b)
        assert np.allclose(kernel, loaded_values["kernel"])


def test_distirbuted_checkpoint_from_root_size_one_ndarray(checkpointer):
    root_process = 0
    fileid = "_from_root"
    rank = checkpointer.comm.Get_rank()

    # save data from root process only
    if rank == root_process:
        a = np.full((1,), 3.0)
    else:
        a = None

    checkpointer.save_from_process(
        root_process,
        fileid,
        [np.s_[:]],
        [None],
        rng=None,
        a=a,
        mode="w",
    )

    loaded_values = checkpointer.load_from_process(
        root_process,
        fileid,
        [np.s_[:]],
        None,
        "a",
    )

    if rank == root_process:
        assert np.isclose(a, loaded_values["a"])


def test_checkpoint_variables(checkpointer, local_rng, kernel, overlap_size):
    root_process = 0
    fileid = "2"
    ndims = 2
    grid_size = MPI.Compute_dims(checkpointer.comm.Get_size(), ndims)
    rank = checkpointer.comm.Get_rank()
    backward_overlap = True

    # * Cartesian topology communicator and nD rank
    cartcomm = checkpointer.comm.Create_cart(
        dims=grid_size, periods=ndims * [False], reorder=False
    )
    ranknd = cartcomm.Get_coords(rank)

    grid_size = np.array(grid_size, dtype="i")
    ranknd = np.array(ranknd, dtype="i")

    # full image size
    image_size = np.array([20, 20], dtype="i")  # overall image size
    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, backward=backward_overlap
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1
    # facet size (convolution)
    facet_pixels = ucomm.local_split_range_nd(
        grid_size,
        image_size,
        ranknd,
        overlap=overlap_size,
        backward=backward_overlap,
    )
    facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
    offset = facet_size - tile_size

    # * setup useful slices
    local_slice_tile = ucomm.get_local_slice(
        ranknd, grid_size, offset, backward=backward_overlap
    )  # extract tile from local conv facet

    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )

    # * local image
    x = np.empty(facet_size, dtype="d")
    x[local_slice_tile] = local_rng.standard_normal(size=tile_size)

    # * save
    # image (all processes save a chunk)
    checkpointer.save(
        fileid,
        [image_size, np.ones(1, dtype="i")],
        [global_slice_tile, np.s_[:]],
        [None, None],
        x=x[local_slice_tile],
        a=1,
    )
    # kernel (from root process only)
    checkpointer.save_from_process(
        root_process, fileid, [np.s_[:]], [None], kernel=kernel
    )

    # * load
    # image
    loaded_dic = checkpointer.load(fileid, [global_slice_tile], None, "x")
    # kernel
    loaded_kernel = checkpointer.load_from_process(
        root_process, fileid, [np.s_[:]], None, "kernel"
    )

    # * check consistency
    local_consistency_x = np.array(np.allclose(x[local_slice_tile], loaded_dic["x"]))

    # reduce "local_consistency_x" on the root
    global_consistency_x = np.array([False])
    checkpointer.comm.Reduce(
        [local_consistency_x, MPI.C_BOOL],
        [global_consistency_x, MPI.C_BOOL],
        op=MPI.LAND,
        root=root_process,
    )

    consistency_kernel = False
    if rank == root_process:
        consistency_kernel = np.allclose(loaded_kernel["kernel"], kernel)
        assert consistency_kernel and global_consistency_x


# mpiexec -n 2 python -m pytest tests/mpi/test_mpi_checkpoint.py
# running all tests tagged with pytest.mark.mpi
# mpiexec -n 2 python -m pytest -m mpi
