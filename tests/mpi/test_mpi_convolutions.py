# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import h5py
import numpy as np
import pytest
from mpi4py import MPI

import dsgs.operators.convolutions as uconv
import dsgs.utils.communications as ucomm
from dsgs.operators.data import generate_2d_gaussian_kernel
from dsgs.operators.distributed_convolutions import (
    calculate_local_data_size,
    create_local_to_global_slice,
)

# TODO: simplify the different notation used

pytestmark = pytest.mark.mpi


@pytest.fixture
def comm():
    return MPI.COMM_WORLD


@pytest.fixture
def size(comm):
    return comm.Get_size()


@pytest.fixture
def rank(comm):
    return comm.Get_rank()


@pytest.fixture
def seed():
    return 123


@pytest.fixture
def ndims():
    return 2


@pytest.fixture
def image_size():
    # overall image size
    return np.array([20, 20], dtype="i")


@pytest.fixture
def kernel_size():
    # size of convolution
    return np.array([4, 4], dtype="i")


@pytest.fixture
def overlap_size(kernel_size):
    # overlap size kernel
    return kernel_size - 1


@pytest.fixture
def kernel(kernel_size):
    # square convolution kernel
    return generate_2d_gaussian_kernel(kernel_size[0], 0.1)


def test_distributed_convolution_backward(
    comm, rank, size, ndims, image_size, kernel, kernel_size, overlap_size, seed
):
    """Testing distributed convolution with backward facet overlap."""

    data_size = image_size + overlap_size
    grid_size = MPI.Compute_dims(size, ndims)

    # * Cartesian topology communicator and nD rank
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, backward=True
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    local_data_size = (
        tile_size + (ranknd == grid_size - 1) * overlap_size
    )  # ! backward overlap

    # facet size (convolution)
    facet_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, overlap=overlap_size, backward=True
    )
    facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
    local_conv_size = facet_size + overlap_size
    offset = facet_size - tile_size

    # * setup useful slices
    local_slice_tile = ucomm.get_local_slice(
        ranknd, grid_size, offset, backward=True
    )  # extract tile from local conv facet

    local_slice_valid_conv = ucomm.slice_valid_coefficients(
        ranknd, grid_size, overlap_size
    )  # extract valid coefficients after local convolutions

    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    # ! needs to be changed bepending on direction of the overlap?
    global_slice_data = tuple(
        [
            np.s_[tile_pixels[d, 0] : tile_pixels[d, 0] + local_data_size[d]]
            for d in range(ndims)
        ]
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local image and kernel
    local_image = np.empty(facet_size, dtype="d")
    local_image[local_slice_tile] = local_rng.standard_normal(size=tile_size)
    ft_kernel = np.fft.rfftn(kernel, local_conv_size)

    # * setup communication scheme
    # direct convolution
    (
        dest,
        src,
        resizedsendsubarray,
        resizedrecvsubarray,
    ) = ucomm.setup_border_update(
        cartcomm,
        ndims,
        local_image.itemsize,
        facet_size,
        overlap_size,
        backward=True,
    )

    # * setup auxiliary buffers
    # communicate facet borders to neighbours (x)
    ucomm.mpi_update_borders(
        comm, local_image, dest, src, resizedsendsubarray, resizedrecvsubarray
    )

    # TODO: condition to be improved (do a function to free custom types)
    # * free custom types
    for d in range(ndims):
        if overlap_size[d] > 1:
            resizedsendsubarray[d].Free()
            resizedrecvsubarray[d].Free()

    local_data = uconv.fft_conv(local_image, ft_kernel, local_conv_size)[
        local_slice_valid_conv
    ]

    # * save to an h5 test file (parallel writing)
    f = h5py.File("convolution_test.h5", "w", driver="mpio", comm=comm)

    dset_image = f.create_dataset("x", image_size, dtype="d")
    dset_image[global_slice_tile] = local_image[local_slice_tile]

    dset_data = f.create_dataset("y", data_size, dtype="d")
    dset_data[global_slice_data] = local_data

    dset_kernel = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        dset_kernel[:] = kernel
    f.close()
    del f, dset_image, dset_data, dset_kernel

    # * compare to full convolution
    g = h5py.File("convolution_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.fft.rfftn(h0, data_size)
        y = uconv.fft_conv(x0, H0, data_size)
        # print(np.allclose(y, y0))
        assert np.allclose(y, y0)
    g.close()


def test_distributed_convolution_forward(
    comm, rank, size, ndims, image_size, kernel, kernel_size, overlap_size, seed
):
    """Testing distributed convolution with forward facet overlap."""

    data_size = image_size + overlap_size
    grid_size = MPI.Compute_dims(size, ndims)

    # * Cartesian topology communicator and nD rank
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, backward=False
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    local_data_size = tile_size + (ranknd == 0) * overlap_size  # ! forward overlap
    # local_data_size = (
    #         tile_size + (ranknd == grid_size - 1) * overlap_size
    #     )  # ! backward overlap

    # facet size (convolution)
    facet_pixels = ucomm.local_split_range_nd(
        grid_size,
        image_size,
        ranknd,
        overlap=overlap_size,
        backward=False,
    )
    facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
    local_conv_size = facet_size + overlap_size
    offset = facet_size - tile_size
    # facet_size_adj = (
    #     local_data_size + np.logical_and(ranknd > 0, grid_size > 1) * overlap_size
    # )
    # offset_adj = facet_size_adj - tile_size

    # * setup useful slices
    local_slice_tile = ucomm.get_local_slice(
        ranknd, grid_size, offset, backward=False
    )  # extract tile from local conv facet (backward overlap by default)
    # slice_local_tile = tuple([np.s_[: tile_size[d]] for d in range(ndims)])  # ! forward overlap
    local_slice_valid_conv = ucomm.slice_valid_coefficients(
        ranknd, grid_size, overlap_size
    )  # extract valid coefficients after local convolutions

    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    # ! needs to be changes bepending on direction of the overlap
    # ! create function: global slice convolution
    global_slice_data = tuple(
        [
            np.s_[
                tile_pixels[d, 0]
                + (ranknd[d] > 0) * overlap_size[d] : tile_pixels[d, 0]
                + (ranknd[d] > 0) * overlap_size[d]
                + local_data_size[d]
            ]
            for d in range(ndims)
        ]
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local image and kernel
    local_image = np.empty(facet_size, dtype="d")
    local_image[local_slice_tile] = local_rng.standard_normal(size=tile_size)
    ft_kernel = np.fft.rfftn(kernel, local_conv_size)

    # * setup communication scheme
    # direct convolution
    (
        dest,
        src,
        resizedsendsubarray,
        resizedrecvsubarray,
    ) = ucomm.setup_border_update(
        cartcomm,
        ndims,
        local_image.itemsize,
        facet_size,
        overlap_size,
        backward=False,
    )

    # * setup auxiliary buffers
    # communicate facet borders to neighbours (x)
    ucomm.mpi_update_borders(
        comm, local_image, dest, src, resizedsendsubarray, resizedrecvsubarray
    )

    # TODO: condition to be improved (do a function to free custom types)
    # * free custom types
    for d in range(ndims):
        if overlap_size[d] > 1:
            resizedsendsubarray[d].Free()
            resizedrecvsubarray[d].Free()

    local_data = uconv.fft_conv(local_image, ft_kernel, local_conv_size)[
        local_slice_valid_conv
    ]

    # * save to an h5 test file (parallel writing)
    f = h5py.File("convolution_test.h5", "w", driver="mpio", comm=comm)

    dset_image = f.create_dataset("x", image_size, dtype="d")
    dset_image[global_slice_tile] = local_image[local_slice_tile]

    dset_data = f.create_dataset("y", data_size, dtype="d")
    dset_data[global_slice_data] = local_data

    dset_kernel = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        dset_kernel[:] = kernel
    f.close()
    del f, dset_image, dset_data, dset_kernel

    # * compare to full convolution
    g = h5py.File("convolution_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.fft.rfftn(h0, data_size)
        y = uconv.fft_conv(x0, H0, data_size)
        # print(np.allclose(y, y0))
        assert np.allclose(y, y0)
    g.close()


def test_adjoint_distributed_convolution_backward(
    comm, rank, size, ndims, image_size, kernel, kernel_size, overlap_size, seed
):
    """Testing adjoint distributed convolution with backward facet
    overlap for the direct operator."""

    data_size = image_size + overlap_size
    grid_size = MPI.Compute_dims(size, ndims)

    # * Cartesian topology communicator and nD rank
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, backward=True
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    local_data_size = tile_size + (ranknd == grid_size - 1) * overlap_size

    # facet size (adjoint convolution)
    facet_size_adj = local_data_size + (ranknd < grid_size - 1) * overlap_size
    local_conv_size = facet_size_adj + overlap_size
    offset_adj = (facet_size_adj - local_data_size).astype(int)

    # * useful slices
    # indexing into local data
    # ! for direct operator with backward overlap, adjoint operator has
    # ! forward overlap (local slice data has forward overlap here)
    local_slice_data = ucomm.get_local_slice(
        ranknd, grid_size, offset_adj, backward=False
    )

    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels, ranknd, overlap_size, local_data_size, backward=True
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local data and kernel
    facet_adj = np.empty(facet_size_adj, dtype="d")
    facet_adj[local_slice_data] = local_rng.standard_normal(size=local_data_size)
    ft_kernel = np.fft.rfftn(kernel, local_conv_size)

    # * setup communication scheme
    # ! for the adjoint communication scheme, take overlap direction
    # ! opposite to the one taken for the direct operator
    (
        dest_adj,
        src_adj,
        resizedsendsubarray_adj,
        resizedrecvsubarray_adj,
    ) = ucomm.setup_border_update(
        cartcomm,
        ndims,
        facet_adj.itemsize,
        facet_size_adj,
        overlap_size,
        backward=False,
    )

    # * communications
    # adjoint operator
    ucomm.mpi_update_borders(
        comm,
        facet_adj,
        dest_adj,
        src_adj,
        resizedsendsubarray_adj,
        resizedrecvsubarray_adj,
    )

    # * free custom types
    for d in range(ndims):
        if overlap_size[d] > 1:
            resizedsendsubarray_adj[d].Free()
            resizedrecvsubarray_adj[d].Free()

    # * local linear convolution
    local_x = uconv.fft_conv(facet_adj, np.conj(ft_kernel), local_conv_size)[
        : tile_size[0], : tile_size[1]
    ]

    # * save to an h5 test file (parallel writing)
    f = h5py.File("2d_convolution_adj_test.h5", "w", driver="mpio", comm=comm)

    # TODO to be generalized to nD
    dset = f.create_dataset("x", image_size, dtype="d")
    dset[global_slice_tile] = local_x

    yh5 = f.create_dataset("y", data_size, dtype="d")
    yh5[global_slice_data] = facet_adj[local_slice_data]

    hh5 = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        hh5[:] = kernel
    f.close()  # ! flush done automatically when closing the file
    comm.Barrier()

    # * test parallel load convolution result
    g = h5py.File(
        "2d_convolution_adj_test.h5", "r+", driver="mpio", comm=MPI.COMM_WORLD
    )
    dset = g["x"]
    loaded_x = np.zeros(local_x.shape)
    dset.read_direct(
        loaded_x,
        global_slice_tile,
        (np.s_[:], np.s_[:]),
    )

    # * debugging section
    # comm.Barrier()
    # print(
    #     "Process {}: |local_x - loaded_x| = {}".format(
    #         rank, np.linalg.norm(local_x - loaded_x)
    #     )
    # )
    g.close()

    # * compare to full convolution
    g = h5py.File("2d_convolution_adj_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.conj(np.fft.rfftn(h0, data_size))
        x = uconv.fft_conv(y0, H0, data_size)[: image_size[0], : image_size[1]]
        consistency_test = np.allclose(x0, x)
        # print("Consistency serial / parallel convolution: {}".format(consistency_test))
        assert consistency_test
    g.close()


def test_adjoint_distributed_convolution_forward(
    comm, rank, size, ndims, image_size, kernel, kernel_size, overlap_size, seed
):
    """Testing adjoint distributed convolution with forward facet
    overlap for the direct operator."""

    data_size = image_size + overlap_size
    grid_size = MPI.Compute_dims(size, ndims)

    # * Cartesian topology communicator and nD rank
    cartcomm = comm.Create_cart(dims=grid_size, periods=ndims * [False], reorder=False)
    ranknd = cartcomm.Get_coords(rank)
    ranknd = np.array(ranknd, dtype="i")
    grid_size = np.array(grid_size, dtype="i")

    # tile size
    tile_pixels = ucomm.local_split_range_nd(
        grid_size, image_size, ranknd, backward=False
    )
    tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

    # data size
    local_data_size, facet_size, facet_size_adj = calculate_local_data_size(
        tile_size, ranknd, overlap_size, grid_size, backward=False
    )
    local_conv_size = facet_size_adj + overlap_size
    offset_adj = (facet_size_adj - local_data_size).astype(int)

    # * useful slices
    # indexing into local data
    # ! for direct operator with forward overlap, adjoint operator has
    # ! backward overlap
    local_slice_data = ucomm.get_local_slice(
        ranknd, grid_size, offset_adj, backward=True
    )

    # indexing into global arrays
    global_slice_tile = tuple(
        [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
    )
    global_slice_data = create_local_to_global_slice(
        tile_pixels, ranknd, overlap_size, local_data_size, backward=False
    )

    # * parallel rng
    child_seed = None
    if rank == 0:
        ss = np.random.SeedSequence(seed)
        child_seed = ss.spawn(size)
    local_seed = comm.scatter(child_seed, root=0)
    local_rng = np.random.default_rng(local_seed)

    # * local data and kernel
    facet_adj = np.empty(facet_size_adj, dtype="d")
    facet_adj[local_slice_data] = local_rng.standard_normal(size=local_data_size)
    ft_kernel = np.fft.rfftn(kernel, local_conv_size)

    # * setup communication scheme
    # ! for the adjoint communication scheme, take overlap direction
    # ! opposite to the one taken for the direct operator
    (
        dest_adj,
        src_adj,
        resizedsendsubarray_adj,
        resizedrecvsubarray_adj,
    ) = ucomm.setup_border_update(
        cartcomm,
        ndims,
        facet_adj.itemsize,
        facet_size_adj,
        overlap_size,
        backward=True,
    )

    # * communications
    # adjoint operator
    ucomm.mpi_update_borders(
        comm,
        facet_adj,
        dest_adj,
        src_adj,
        resizedsendsubarray_adj,
        resizedrecvsubarray_adj,
    )

    # * free custom types
    for d in range(ndims):
        if overlap_size[d] > 1:
            resizedsendsubarray_adj[d].Free()
            resizedrecvsubarray_adj[d].Free()

    # * local linear convolution
    local_x = uconv.fft_conv(facet_adj, np.conj(ft_kernel), local_conv_size)[
        : tile_size[0], : tile_size[1]
    ]

    # * save to an h5 test file (parallel writing)
    f = h5py.File("2d_convolution_adj_test.h5", "w", driver="mpio", comm=comm)

    # TODO to be generalized to nD
    dset = f.create_dataset("x", image_size, dtype="d")
    dset[global_slice_tile] = local_x

    yh5 = f.create_dataset("y", data_size, dtype="d")
    yh5[global_slice_data] = facet_adj[local_slice_data]

    hh5 = f.create_dataset("h", kernel_size, dtype="d")
    if rank == 0:
        hh5[:] = kernel
    f.close()  # ! flush done automatically when closing the file
    comm.Barrier()

    # * test parallel load convolution result
    g = h5py.File(
        "2d_convolution_adj_test.h5", "r+", driver="mpio", comm=MPI.COMM_WORLD
    )
    dset = g["x"]
    loaded_x = np.zeros(local_x.shape)
    dset.read_direct(
        loaded_x,
        global_slice_tile,
        (np.s_[:], np.s_[:]),
    )

    # * debugging section
    # comm.Barrier()
    # print(
    #     "Process {}: |local_x - loaded_x| = {}".format(
    #         rank, np.linalg.norm(local_x - loaded_x)
    #     )
    # )
    g.close()

    # * compare to full convolution
    g = h5py.File("2d_convolution_adj_test.h5", "r+", driver="mpio", comm=comm)
    if rank == 0:
        y0 = g["y"][()]
        x0 = g["x"][()]
        h0 = g["h"][()]

        H0 = np.conj(np.fft.rfftn(h0, data_size))
        x = uconv.fft_conv(y0, H0, data_size)[: image_size[0], : image_size[1]]
        consistency_test = np.allclose(x0, x)
        # print("Consistency serial / parallel convolution: {}".format(consistency_test))
        assert consistency_test
    g.close()


# mpiexec -n 2 python -m unittest tests/test_mpi_convolutions.py
# mpiexec -n 2 python -m mpi4py tests/test_mpi_convolutions.py
# mpiexec -n numprocs python -m mpi4py pyfile [arg] ...
