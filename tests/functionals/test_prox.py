# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import numpy as np

import dsgs.functionals.prox as prox


class TestProx(unittest.TestCase):
    def setUp(self):
        self.rng = np.random.default_rng(1234)
        N = [5, 3]
        self.x = self.rng.normal(loc=0.0, scale=1.0, size=N)
        self.y = self.rng.normal(loc=0.0, scale=1.0, size=N)

    def test_hard_thresholding(self):
        z0 = prox.hard_thresholding(1.0, 0.0)
        z1 = prox.hard_thresholding(1.0, 2.0)
        self.assertTrue(np.isclose(z0, 1.0))
        self.assertTrue(np.isclose(z1, 0.0))

    def test_prox_nonegativity(self):
        z = self.x.copy()
        prox.prox_nonegativity(z)
        self.assertTrue(np.allclose(z[z > 0], self.x[self.x > 0]))

    def test_proj_box(self):
        low = np.zeros(1)
        high = 0.5 * np.ones(1)
        z = prox.proj_box(self.x, low, high)
        self.assertTrue(np.all(z >= 0) and np.all(z <= 0.5))

    def test_proj_box_error_message(self):
        low = 2 * np.ones(1)
        high = np.ones(1)
        with self.assertRaises(ValueError) as context:
            prox.proj_box(self.x, low, high)
        self.assertTrue(
            "Each entry in `low` should be lower than or equal to the corresponding entry in `high`"
            in str(context.exception)
        )

    def test_prox_kullback_leibler(self):
        z = prox.prox_kullback_leibler(
            np.zeros_like(self.x), np.zeros_like(self.y), lam=1
        )
        self.assertTrue(np.allclose(z, 0.0))

    def test_prox_kullback_leibler_negative_regularizer(self):
        with self.assertRaises(ValueError):
            prox.prox_kullback_leibler(self.x, self.y, lam=-1)

    def test_prox_l21_norm(self):
        z = prox.prox_l21norm(np.zeros_like(self.x), lam=1, axis=0)
        self.assertTrue(np.allclose(z, 0.0))

    def test_prox_l21_norm_negative_regularizer(self):
        with self.assertRaises(ValueError):
            prox.prox_l21norm(self.x, lam=-1, axis=0)

    def test_prox_l21_norm_conj(self):
        r"""Check that the proximity operator of the dual function is properly
        implemented by checking the Moreau identity

        ..math:
            \text{prox}_{\lambda f} (x) + \lambda \text{prox}_{\lambda^{-1} f^*} (\lambda^{-1} x) = x.
        """
        lam = 0.33
        z1 = prox.prox_l21norm(self.x, lam=lam, axis=0)
        z2 = lam * prox.prox_l21norm_conj(self.x / lam, lam=1 / lam, axis=0)
        z = z1 + z2
        self.assertTrue(np.allclose(z, self.x))


class TestFunctions(unittest.TestCase):
    def setUp(self):
        self.rng = np.random.default_rng(5678)
        N = [5, 3]
        self.x = self.rng.normal(loc=0.0, scale=1.0, size=N)
        self.y = self.rng.normal(loc=0.0, scale=1.0, size=N)

    def test_l21_norm(self):
        z = prox.l21_norm(self.x, axis=0)
        self.assertTrue(np.allclose(z, np.sum(np.sqrt(np.sum(self.x**2, axis=0)))))

    def test_kullback_leibler(self):
        z = prox.kullback_leibler(self.x, np.zeros_like(self.y))
        self.assertTrue(np.allclose(z, np.sum(self.x)))


if __name__ == "__main__":
    unittest.main()
