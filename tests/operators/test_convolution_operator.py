# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest
import scipy.signal as sg

from dsgs.operators.convolutions import fft2_conv
from dsgs.operators.data import generate_2d_gaussian_kernel
from dsgs.operators.linear_convolution import SerialConvolution
from dsgs.operators.padding import pad_array


@pytest.fixture
def rng():
    return np.random.default_rng(1234)


@pytest.fixture
def kernel():
    """Convolution kernel"""
    return generate_2d_gaussian_kernel(3, 0.1)


@pytest.fixture
def image_size():
    return np.array([10, 7], dtype="i")


@pytest.fixture
def data_size(image_size, kernel):
    return image_size + np.array(kernel.shape, dtype="i") - 1


@pytest.fixture
def x(image_size, rng):
    return rng.normal(loc=0.0, scale=1.0, size=image_size)


@pytest.fixture
def lconv_model(image_size, kernel, data_size):
    """Linear convolution model."""
    return SerialConvolution(image_size, kernel, data_size)


@pytest.fixture
def cconv_model(image_size, kernel, data_size):
    """Circular convolution model."""
    return SerialConvolution(image_size, kernel, image_size)


def test_SerialConvolution_throws_exceptions(image_size, kernel, data_size):
    # inconsistent number of elements between image_size and data_size
    with pytest.raises(ValueError) as excinfo:
        SerialConvolution(image_size[1:], kernel, data_size)
    assert "image_size and data_size must have the same number of elements" in str(
        excinfo.value
    )

    # inconsistent number of axis for kernel, compared to prescribed image
    # and data sizes
    with pytest.raises(ValueError) as excinfo:
        SerialConvolution(image_size, kernel[0, :], data_size)
    assert "kernel should have ndims = len(image_size) dimensions" in str(excinfo.value)

    # pass complex-valued kernel (only real kernel supported for now)
    with pytest.raises(TypeError) as excinfo:
        SerialConvolution(image_size, (1 + 1j) * kernel, data_size)
    assert "only real-valued kernel supported" in str(excinfo.value)


def test_SeriaConvModel_lconv_direct_operator(x, lconv_model, kernel):
    """Check consistency between proposed implementation and
    ``scipy.signal.convolve``.
    """
    Hx = lconv_model.forward(x)
    Hx_scipy = sg.convolve(x, kernel, mode="full")
    assert np.allclose(Hx, Hx_scipy)


def test_SeriaConvModel_lconv_adjoint(x, lconv_model, rng):
    """Check correctness adjoint operator (linear convolution)."""
    y = rng.standard_normal(lconv_model.data_size)
    Hx = lconv_model.forward(x)
    Hadj_y = lconv_model.adjoint(y)
    sp1 = np.sum(Hx * y)  # usual scalar product
    sp2 = np.sum(x * Hadj_y)
    assert np.isclose(sp1, sp2)


def test_SerialConvolution_cconv_consistency(x, cconv_model, rng):
    """Check consistency with ``dsgs.operators.operators.fft2_conv``."""
    padded_kernel = pad_array(
        cconv_model.kernel, cconv_model.data_size, padmode="after"
    )
    y_, ft_kernel = fft2_conv(x, padded_kernel, cconv_model.data_size)
    Hx = cconv_model.forward(x)
    assert np.allclose(Hx, y_)


def test_SeriaConvModel_cconv_adjoint(x, cconv_model, rng):
    """Check correctness adjoint operator (circular convolution)."""
    y = rng.standard_normal(cconv_model.data_size)
    Hx = cconv_model.forward(x)
    Hadj_y = cconv_model.adjoint(y)
    sp1 = np.sum(Hx * y)  # usual scalar product
    sp2 = np.sum(x * Hadj_y)
    assert np.isclose(sp1, sp2)
