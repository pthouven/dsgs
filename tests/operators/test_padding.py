# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import numpy as np

import dsgs.operators.padding as up


class TestPadding(unittest.TestCase):
    def test_dimension_error(self):
        with self.assertRaises(ValueError):
            up.pad_array(np.ones(2), [0, 0])
        with self.assertRaises(ValueError):
            up.crop_array(np.ones(2), [3])

    def test_value_error(self):
        with self.assertRaises(ValueError):
            up.pad_array(np.ones(2), [1])
        with self.assertRaises(ValueError):
            up.crop_array(np.ones(2), [1, 1])

    def test_shape(self):
        N = [5]
        K = [8]
        x = np.arange(N[0])
        y = up.pad_array(x, K)
        self.assertTrue(len(y.shape) == len(x.shape))
        self.assertTrue(y.size == K[0])

    def test_center_value_after_padding(self):
        N = [5]
        K = [8]
        x = np.arange(N[0])
        # fftshift convention
        cN = N[0] // 2
        cK = K[0] // 2
        y = up.pad_array(x, K, "around")
        self.assertTrue(x[cN] == y[cK])
        # imfilter convention
        cN = (N[0] - 1) // 2
        cK = (K[0] - 1) // 2
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(x[cN] == y[cK])

    def test_pad_1d_around_odd_to_even(self):
        N = [5]
        K = [8]
        M = K[0] - N[0]
        x = np.arange(N[0])
        # fftshift convention
        yref = np.r_[np.zeros(int(np.ceil(M / 2))), x, np.zeros(M // 2)]
        y = up.pad_array(x, K, "around")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.r_[np.zeros(M // 2), x, np.zeros(int(np.ceil(M / 2)))]
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_1d_around_even_to_odd(self):
        N = [5]
        K = [8]
        M = K[0] - N[0]
        xref = np.arange(N[0])
        # fftshift convention
        y = np.r_[np.zeros(int(np.ceil(M / 2))), xref, np.zeros(M // 2)]
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        y = np.r_[np.zeros(M // 2), xref, np.zeros(int(np.ceil(M / 2)))]
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_pad_1d_around_even_to_odd(self):
        N = [6]
        K = [9]
        M = K[0] - N[0]
        x = np.arange(N[0])
        # fftshift convention
        yref = np.r_[np.zeros(int(np.ceil(M / 2))), x, np.zeros(M // 2)]
        y = up.pad_array(x, K, "around")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.r_[np.zeros(M // 2), x, np.zeros(int(np.ceil(M / 2)))]
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_1d_around_odd_to_even(self):
        N = [6]
        K = [9]
        M = K[0] - N[0]
        xref = np.arange(N[0])
        # fftshift convention
        y = np.r_[np.zeros(int(np.ceil(M / 2))), xref, np.zeros(M // 2)]
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        y = np.r_[np.zeros(M // 2), xref, np.zeros(int(np.ceil(M / 2)))]
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_pad_1d_around_odd_to_odd(self):
        N = [5]
        K = [9]
        M = K[0] - N[0]
        x = np.arange(N[0])
        # fftshift convention
        yref = np.r_[np.zeros(int(np.ceil(M / 2))), x, np.zeros(M // 2)]
        y = up.pad_array(x, K, "around")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.r_[np.zeros(M // 2), x, np.zeros(int(np.ceil(M / 2)))]
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_1d_around_odd_to_odd(self):
        N = [5]
        K = [9]
        M = K[0] - N[0]
        xref = np.arange(N[0])
        # fftshift convention
        y = np.r_[np.zeros(int(np.ceil(M / 2))), xref, np.zeros(M // 2)]
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        y = np.r_[np.zeros(M // 2), xref, np.zeros(int(np.ceil(M / 2)))]
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_pad_1d_around_even_to_even(self):
        N = [4]
        K = [8]
        M = K[0] - N[0]
        x = np.arange(N[0])
        # fftshift convention
        yref = np.r_[np.zeros(int(np.ceil(M / 2))), x, np.zeros(M // 2)]
        y = up.pad_array(x, K, "around")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.r_[np.zeros(M // 2), x, np.zeros(int(np.ceil(M / 2)))]
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_pad_1d_around_even_to_even_(self):
        N = [4]
        K = [8]
        M = K[0] - N[0]
        x = np.arange(N[0])
        # fftshift convention
        yref = np.r_[np.zeros(int(np.ceil(M / 2))), x, np.zeros(M // 2)]
        y = up.pad_array(x, K, padmode="around", mode="constant")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.r_[np.zeros(M // 2), x, np.zeros(int(np.ceil(M / 2)))]
        y = up.pad_array(x, K, "around", True, "constant")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_1d_around_even_to_even(self):
        N = [4]
        K = [8]
        M = K[0] - N[0]
        xref = np.arange(N[0])
        # fftshift convention
        y = np.r_[np.zeros(int(np.ceil(M / 2))), xref, np.zeros(M // 2)]
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        y = np.r_[np.zeros(M // 2), xref, np.zeros(int(np.ceil(M / 2)))]
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_pad_2d_around(self):
        N = [5, 4]
        K = [7, 8]
        M = np.array([K[n] - N[n] for n in range(len(K))])
        x = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        # fftshift convention
        yref = np.zeros(K)
        start_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        yref[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = x
        y = up.pad_array(x, K, "around")
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)
        # imfilter convention
        yref = np.zeros(K)
        start_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        yref[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = x
        y = up.pad_array(x, K, "around", True)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_2d_around(self):
        N = [5, 4]
        K = [7, 8]
        M = np.array([K[n] - N[n] for n in range(len(K))])
        xref = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        # fftshift convention
        y = np.zeros(K)
        start_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        y[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = xref
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        y = np.zeros(K)
        start_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        y[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = xref
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_pad_2d_after(self):
        N = [5, 4]
        K = [7, 8]
        x = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        yref = np.zeros(K)
        yref[: N[0], : N[1]] = x
        y = up.pad_array(x, K)
        self.assertTrue(np.linalg.norm(y - yref) <= 1e-13)

    def test_crop_2d_after(self):
        N = [5, 4]
        K = [7, 8]
        xref = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        y = np.zeros(K)
        y[: N[0], : N[1]] = xref
        x = up.crop_array(y, N)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    # * testing pad_array_nd / crop_array_nd
    def test_pad_array_nd_shape(self):
        N = np.array([5, 4], dtype="i")
        L = np.array([0, 2], dtype="i")
        R = np.array([2, 0], dtype="i")
        rng = np.random.default_rng(1234)
        x = (1 + 1j) * rng.standard_normal(size=N)
        y = up.pad_array_nd(x, L, R)

        self.assertTrue(np.allclose(np.array(y.shape, dtype="i"), N + L + R))
        self.assertTrue(np.isclose(np.linalg.norm(y), np.linalg.norm(x)))

    def test_pad_array_nd_error(self):
        x = np.array([5, 4], dtype="i")
        L = [0, 2]
        R = [2]  # last element in the slice

        with self.assertRaises(ValueError):
            up.pad_array_nd(x, L, R)

    def test_crop_array_nd_error(self):
        x = np.array([5, 4], dtype="i")
        L = [0, 2]
        R = [2]  # last element in the slice

        with self.assertRaises(ValueError):
            up.crop_array_nd(x, L, R)

    def test_adjoint_pad_crop_array_nd(self):
        N = [5, 4]
        Lx = [0, 2]
        Rx = [2, 0]
        Ly = [0, 2]
        Ry = [2, 0]
        rng = np.random.default_rng(1234)
        x = (1 + 1j) * rng.standard_normal(size=N)
        Cx = up.crop_array_nd(x, Lx, Rx)
        y = (1 + 1j) * rng.standard_normal(size=Cx.shape)
        Cadj_y = up.pad_array_nd(y, Ly, Ry)

        # check the 2 operators are adjoint
        sp1 = np.sum(np.conj(Cx) * y)
        sp2 = np.sum(np.conj(x) * Cadj_y)
        self.assertTrue(np.abs(sp1 - sp2) <= 1e-13)


if __name__ == "__main__":
    unittest.main()
