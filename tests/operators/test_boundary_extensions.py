"""Test the implementation of the boundary extension operators and their
adjoint. Supported extensions include the zero-padding, (half-point) symmetric
and circular boundary extensions."""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest

import dsgs.operators.padding as amp


@pytest.fixture
def rng():
    return np.random.default_rng(1234)


@pytest.fixture
def image_size():
    return np.array([10, 7], dtype="i")
    # return np.array([2, 3], dtype="i")  # should cause an issue for the
    # adjoint of the padding with "reflect" boundary condition, given the
    # current values for the left and right padding


@pytest.fixture
def lsize():
    return np.array([2, 2], dtype="i")


@pytest.fixture
def rsize():
    return np.array([2, 3], dtype="i")


@pytest.fixture
def x(image_size, rng):
    """Random image."""
    return rng.normal(loc=0.0, scale=1.0, size=image_size)


def test_error_message_pad_array_nd(x, rsize):
    """Testing errors thrown by the pad_array_nd function."""
    with pytest.raises(ValueError) as excinfo:
        amp.pad_array_nd(x, np.array([1, 2, 3]), rsize, mode="constant")
    assert "`x.shape`, `lsize` and `rsize` must have the same length." in str(
        excinfo.value
    )


def test_error_message_adjoint_padding_nd(x, lsize, rsize):
    """Testing errors thrown by the adjoint_padding_nd function."""
    with pytest.raises(ValueError) as excinfo:
        amp.adjoint_padding(x, np.array([1, 2, 3]), rsize, mode="constant")
    assert "`x.shape`, `lsize` and `rsize` must have the same length." in str(
        excinfo.value
    )

    with pytest.raises(ValueError) as excinfo:
        amp.adjoint_padding(x, lsize, rsize, mode="zpd")
    assert "Unknown extension `mode`: zpd" in str(excinfo.value)


# zero padding
def test_zero_padding(x, lsize, rsize, rng):
    """Check consistency of the implementation of the adjoint zero-padding operator."""
    Px = amp.pad_array_nd(x, lsize, rsize, mode="constant")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rsize, mode="constant")

    sp1 = np.sum(np.conj(Px) * y)  # usual scalar product
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_left_zero_padding(x, lsize, rng):
    """Check consistency of the implementation of the adjoint zero-padding
    operator when post padding size is zero along each axis."""
    rs = len(x.shape) * [0]
    Px = amp.pad_array_nd(x, lsize, rs, mode="constant")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rs, mode="constant")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_right_zero_padding(x, rsize, rng):
    """Check consistency of the implementation of the adjoint zero-padding
    operator when pre padding size is zero along each axis."""
    ls = len(x.shape) * [0]
    Px = amp.pad_array_nd(x, ls, rsize, mode="constant")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rsize, mode="constant")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_no_zero_padding(x, rng):
    """Check consistency of the implementation of the adjoint zero-padding
    operator when pre padding size is zero along each axis."""
    ls = len(x.shape) * [0]
    rs = len(x.shape) * [0]
    Px = amp.pad_array_nd(x, ls, rs, mode="constant")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rs, mode="constant")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


# circular extension
def test_circular_padding(x, lsize, rsize, rng):
    """Check consistency of the implementation of the adjoint zero-padding operator."""
    Px = amp.pad_array_nd(x, lsize, rsize, mode="wrap")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rsize, mode="wrap")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_left_circular_padding(x, lsize, rng):
    """Check consistency of the implementation of the adjoint circular-padding
    operator when post padding size is zero along each axis."""
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, lsize, rs, mode="wrap")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rs, mode="wrap")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_right_circular_padding(x, rsize, rng):
    """Check consistency of the implementation of the adjoint circular-padding
    operator when pre padding size is zero along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rsize, mode="wrap")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rsize, mode="wrap")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_circular_no_padding(x, rng):
    """Check consistency of the implementation of the adjoint circular-padding
    operator when padding size is zero in both directions along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rs, mode="wrap")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rs, mode="wrap")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


# symmetric extension
def test_symmetric_padding_size(x, rng):
    """Check consistency size condition for the adjoint of the symmetric
    padding operator."""
    ls = np.array([11, 0], dtype="i")
    rs = np.array([0, 8], dtype="i")

    with pytest.raises(ValueError) as excinfo:
        amp.adjoint_padding(x, ls, rs, mode="symmetric")
    assert (
        "Extension mode symmetric requires: `np.maximum(lsize[d], rsize[d]) > input.shape[d]` for each axis `d`"
        in str(excinfo.value)
    )


def test_symmetric_padding(x, lsize, rsize, rng):
    """Check consistency of the implementation of the adjoint symmetric-padding
    operator."""
    Px = amp.pad_array_nd(x, lsize, rsize, mode="symmetric")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rsize, mode="symmetric")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_left_symmetric_padding(x, lsize, rng):
    """Check consistency of the implementation of the adjoint symmetric-padding
    operator when post padding size is zero along each axis."""
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, lsize, rs, mode="symmetric")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rs, mode="symmetric")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_right_symmetric_padding(x, rsize, rng):
    """Check consistency of the implementation of the adjoint symmetric-padding
    operator when pre padding size is zero along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rsize, mode="symmetric")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rsize, mode="symmetric")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_symmetric_no_padding(x, rng):
    """Check consistency of the implementation of the adjoint symmetric-padding
    operator when padding size is zero in both directions along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rs, mode="symmetric")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rs, mode="symmetric")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


# reflect extension
def test_reflect_padding_size(x, rng):
    """Check consistency size condition for the adjoint of the reflect
    padding operator."""
    ls = np.array([10, 0], dtype="i")
    rs = np.array([0, 7], dtype="i")

    with pytest.raises(ValueError) as excinfo:
        amp.adjoint_padding(x, ls, rs, mode="reflect")
    assert (
        "Extension mode reflect requires: `np.maximum(lsize[d], rsize[d]) + 1 > output.shape[d]` for each axis `d`"
        in str(excinfo.value)
    )


def test_reflect_padding(x, lsize, rsize, rng):
    """Check consistency of the implementation of the adjoint reflect-padding
    operator."""
    Px = amp.pad_array_nd(x, lsize, rsize, mode="reflect")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rsize, mode="reflect")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_left_reflect_padding(x, lsize, rng):
    """Check consistency of the implementation of the adjoint reflect-padding
    operator when post padding size is zero along each axis."""
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, lsize, rs, mode="reflect")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, lsize, rs, mode="reflect")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_right_reflect_padding(x, rsize, rng):
    """Check consistency of the implementation of the adjoint reflect-padding
    operator when pre padding size is zero along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rsize, mode="reflect")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rsize, mode="reflect")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)


def test_reflect_no_padding(x, rng):
    """Check consistency of the implementation of the adjoint reflect-padding
    operator when padding size is zero in both directions along each axis."""
    ls = np.zeros(len(x.shape), dtype="i")
    rs = np.zeros(len(x.shape), dtype="i")
    Px = amp.pad_array_nd(x, ls, rs, mode="reflect")
    y = rng.normal(loc=0.0, scale=1.0, size=Px.shape)
    Padj_y = amp.adjoint_padding(y, ls, rs, mode="reflect")

    sp1 = np.sum(np.conj(Px) * y)
    sp2 = np.sum(np.conj(x) * Padj_y)
    assert np.isclose(sp1, sp2)
