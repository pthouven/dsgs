# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest

import dsgs.operators.tv as tv


@pytest.fixture
def rng():
    return np.random.default_rng(1234)


@pytest.fixture
def N():
    return [5, 3]


@pytest.fixture
def x(N, rng):
    return (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)


@pytest.fixture
def y(N, rng):
    return (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=[2, *N])


@pytest.fixture
def Dx(x):
    return tv.gradient_2d(x)


@pytest.fixture
def Dadjy(y):
    return tv.gradient_2d_adjoint(y)


def test_gradient2d_throws_exception():
    with pytest.raises(AssertionError) as excinfo:
        tv.gradient_2d(np.ones(5))
    assert "gradient_2d: Invalid input, expected a 2d numpy array" in str(excinfo.value)


def test_shape(x, Dx, y, Dadjy):
    assert y.shape == Dx.shape
    assert Dadjy.shape == x.shape


def test_adjoint(x, Dx, y, Dadjy):
    sp1 = np.sum(np.conj(Dx) * y)
    sp2 = np.sum(np.conj(x) * Dadjy)
    assert np.allclose(sp1, sp2)


def test_tv(x, Dx):
    tv_x = tv.tv(x)
    assert np.allclose(tv_x, np.sum(np.sqrt(np.sum(np.abs(Dx) ** 2, axis=0))))


def test_gradientnd(x):
    u = tv.gradient_nd(x)
    u_ref = tv.gradient_2d(x)
    err = np.array([np.linalg.norm(u[k] - u_ref[k]) for k in range(len(u))])
    assert np.allclose(err, 0)


def test_gradientndadjoint(x, Dx, y, Dadjy):
    Dx = tv.gradient_nd(x)
    Dadjy = tv.gradient_nd_adjoint(y)
    sp1 = np.sum(np.conj(Dx) * y)
    sp2 = np.sum(np.conj(x) * Dadjy)
    assert np.allclose(sp1, sp2)


def test_tv_nd(x):
    tv_nd_x = tv.tv_nd(x)
    tv_x = tv.tv(x)
    assert np.allclose(tv_x, tv_nd_x)


def test_smooth_tv(x, Dx):
    tv_x = tv.smooth_tv(x, np.finfo(float).eps)
    assert np.allclose(
        tv_x,
        np.sum(np.sqrt(np.abs(Dx[0]) ** 2 + np.abs(Dx[1]) ** 2 + np.finfo(float).eps)),
    )


def test_gradient_smooth_tv(x, Dx):
    grad_smooth_tv_x = tv.gradient_smooth_tv(x, np.finfo(float).eps)
    c = np.sqrt(np.abs(Dx[0]) ** 2 + np.abs(Dx[1]) ** 2 + np.finfo(float).eps)
    Dx = Dx / c

    assert np.allclose(grad_smooth_tv_x, tv.gradient_2d_adjoint(Dx))
