# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import numpy as np

import dsgs.operators.jtv as tv
import dsgs.utils.communications as ucomm


class TestJitTV(unittest.TestCase):
    def setUp(self):
        self.rng = np.random.default_rng(1234)
        N = [5, 3]
        self.x = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=N)
        self.yh = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=N)
        self.yv = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=N)
        self.Dx = tv.gradient_2d(self.x)
        self.Dadjy = tv.gradient_2d_adjoint(self.yh, self.yv)

    def test_shape(self):
        self.assertTrue(len(self.yh.shape) == len(self.Dx[0].shape))
        self.assertTrue(len(self.yv.shape) == len(self.Dx[1].shape))
        self.assertTrue(len(self.Dadjy.shape) == len(self.x.shape))

    def test_adjoint(self):
        sp1 = np.sum(np.conj(self.Dx[0]) * self.yh + np.conj(self.Dx[1]) * self.yv)
        sp2 = np.sum(np.conj(self.x) * self.Dadjy)
        self.assertTrue(np.abs(sp1 - sp2) <= 1e-13)

    def test_tv(self):
        tv_x = tv.tv(self.x)
        self.assertTrue(
            np.abs(
                tv_x
                - np.sum(np.sqrt(np.abs(self.Dx[0]) ** 2 + np.abs(self.Dx[1]) ** 2))
                <= 1e-13
            )
        )

    def test_smooth_tv(self):
        tv_x = tv.smooth_tv(self.x, np.finfo(float).eps)
        self.assertTrue(
            np.abs(
                tv_x
                - np.sum(
                    np.sqrt(
                        np.abs(self.Dx[0]) ** 2
                        + np.abs(self.Dx[1]) ** 2
                        + np.finfo(float).eps
                    )
                )
                <= 1e-13
            )
        )

    def test_gradient_smooth_tv(self):
        grad_smooth_tv_x = tv.gradient_smooth_tv(self.x, np.finfo(float).eps)
        c = np.sqrt(
            np.abs(self.Dx[0]) ** 2 + np.abs(self.Dx[1]) ** 2 + np.finfo(float).eps
        )
        Dh_x = self.Dx[0] / c
        Dv_x = self.Dx[1] / c

        self.assertTrue(
            np.linalg.norm(grad_smooth_tv_x - tv.gradient_2d_adjoint(Dh_x, Dv_x))
            <= 1e-13
        )

    def test_gradient2d_throws_exception(self):
        x = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=(3, *self.x.shape))
        with self.assertRaises(AssertionError) as context:
            tv.gradient_2d(x)
        self.assertTrue(
            "gradient_2d: Invalid input, expected len(x.shape)==2"
            in str(context.exception)
        )
        # self.assertRaises(AssertionError, tv.gradient_smooth_tv, x, np.finfo(float).eps)

    def test_gradient2dadjoint_throws_exception(self):
        uh = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=(3, *self.x.shape))
        uv = (1 + 1j) * self.rng.normal(loc=0.0, scale=1.0, size=(3, *self.x.shape))
        with self.assertRaises(AssertionError) as context:
            tv.gradient_2d_adjoint(uh, uv)
        self.assertTrue(
            "gradient_2d_adjoint: Invalid input, expected len(uh.shape)==len(uv.shape)==2"
            in str(context.exception)
        )

    def test_chunkgradient2d_backward(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        x = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uh0, uv0 = tv.gradient_2d(x)
        uh = np.empty(N, dtype=complex)
        uv = np.empty(N, dtype=complex)

        grid_size = np.array([3, 2], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1

            # direct operator
            rd = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=True
            )
            facet = x[rd[0, 0] : rd[0, 1] + 1, rd[1, 0] : rd[1, 1] + 1]
            uh_k, uv_k = tv.chunk_gradient_2d(facet, islast)

            start = rd[:, 0]
            stop = start + np.array(uv_k.shape, dtype="i")
            uv[start[0] : stop[0], start[1] : stop[1]] = uv_k
            stop = start + np.array(uh_k.shape, dtype="i")
            uh[start[0] : stop[0], start[1] : stop[1]] = uh_k

        self.assertTrue(np.allclose(uv, uv0))
        self.assertTrue(np.allclose(uh, uh0))

    def test_chunkgradient2d_forward(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        x = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uh0, uv0 = tv.gradient_2d(x)
        uh = np.empty(N, dtype=complex)
        uv = np.empty(N, dtype=complex)

        grid_size = np.array([3, 2], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1

            # direct operator
            r0 = ucomm.local_split_range_nd(grid_size, N, ranknd)
            rd = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=False
            )
            facet = x[rd[0, 0] : rd[0, 1] + 1, rd[1, 0] : rd[1, 1] + 1]
            uh_k, uv_k = tv.chunk_gradient_2d(facet, islast)

            start = r0[:, 0]
            stop = start + np.array(uv_k.shape, dtype="i")
            uv[start[0] : stop[0], start[1] : stop[1]] = uv_k
            stop = start + np.array(uh_k.shape, dtype="i")
            uh[start[0] : stop[0], start[1] : stop[1]] = uh_k

        self.assertTrue(np.allclose(uv, uv0))
        self.assertTrue(np.allclose(uh, uh0))

    def test_chunkgradient2dadjoint(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        uh = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uv = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        x0 = tv.gradient_2d_adjoint(uh, uv)
        x = np.zeros(N, dtype=complex)

        grid_size = np.array([3, 3], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1
            isfirst = ranknd == 0
            r0 = ucomm.local_split_range_nd(grid_size, N, ranknd)
            Nk = r0[:, 1] - r0[:, 0] + 1

            ra = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=True
            )
            facet_h = uh[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            facet_v = uv[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            x_k = np.zeros(Nk, dtype=complex)
            tv.chunk_gradient_2d_adjoint(facet_h, facet_v, x_k, isfirst, islast)

            start = r0[:, 0]
            stop = start + Nk
            x[start[0] : stop[0], start[1] : stop[1]] = x_k

        self.assertTrue(np.allclose(x, x0))

    def test_chunkgradient2dadjoint_single_facet(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        uh = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uv = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        x0 = tv.gradient_2d_adjoint(uh, uv)
        x = np.zeros(N, dtype=complex)

        grid_size = np.array([1, 1], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1
            isfirst = ranknd == 0
            r0 = ucomm.local_split_range_nd(grid_size, N, ranknd)
            Nk = r0[:, 1] - r0[:, 0] + 1

            ra = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=True
            )
            facet_h = uh[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            facet_v = uv[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            x_k = np.zeros(Nk, dtype=complex)
            tv.chunk_gradient_2d_adjoint(facet_h, facet_v, x_k, isfirst, islast)

            start = r0[:, 0]
            stop = start + Nk
            x[start[0] : stop[0], start[1] : stop[1]] = x_k

        self.assertTrue(np.allclose(x, x0))

    def test_chunkgradient2dadjoint_two_facets_y(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        uh = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uv = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        x0 = tv.gradient_2d_adjoint(uh, uv)
        x = np.zeros(N, dtype=complex)

        grid_size = np.array([1, 2], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1
            isfirst = ranknd == 0
            r0 = ucomm.local_split_range_nd(grid_size, N, ranknd)
            Nk = r0[:, 1] - r0[:, 0] + 1

            ra = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=True
            )
            facet_h = uh[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            facet_v = uv[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            x_k = np.zeros(Nk, dtype=complex)
            tv.chunk_gradient_2d_adjoint(facet_h, facet_v, x_k, isfirst, islast)

            start = r0[:, 0]
            stop = start + Nk
            x[start[0] : stop[0], start[1] : stop[1]] = x_k

        self.assertTrue(np.allclose(x, x0))

    def test_chunkgradient2dadjoint_two_facets_x(self):
        N = np.array([10, 5], "i")
        rng = np.random.default_rng(1234)
        uh = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        uv = (1 + 1j) * rng.normal(loc=0.0, scale=1.0, size=N)
        x0 = tv.gradient_2d_adjoint(uh, uv)
        x = np.zeros(N, dtype=complex)

        grid_size = np.array([2, 1], dtype="i")
        nchunks = np.prod(grid_size)

        overlap = (grid_size > 1).astype(int)

        for k in range(nchunks):
            ranknd = np.array(np.unravel_index(k, grid_size), dtype="i")
            islast = ranknd == grid_size - 1
            isfirst = ranknd == 0
            r0 = ucomm.local_split_range_nd(grid_size, N, ranknd)
            Nk = r0[:, 1] - r0[:, 0] + 1

            ra = ucomm.local_split_range_nd(
                grid_size, N, ranknd, overlap=overlap, backward=True
            )
            facet_h = uh[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            facet_v = uv[ra[0, 0] : ra[0, 1] + 1, ra[1, 0] : ra[1, 1] + 1]
            x_k = np.zeros(Nk, dtype=complex)
            tv.chunk_gradient_2d_adjoint(facet_h, facet_v, x_k, isfirst, islast)

            start = r0[:, 0]
            stop = start + Nk
            x[start[0] : stop[0], start[1] : stop[1]] = x_k

        self.assertTrue(np.allclose(x, x0))


if __name__ == "__main__":
    unittest.main()
