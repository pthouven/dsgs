# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import numpy as np

import dsgs.operators.padding as up


class TestCrop(unittest.TestCase):
    def test_dimension_error(self):
        with self.assertRaises(ValueError):
            up.crop_array(np.ones(2), [3, 3])

    def test_value_error(self):
        with self.assertRaises(ValueError):
            up.crop_array(np.ones(2), [3])

    def test_shape(self):
        N = [5]
        K = [8]
        y = np.arange(K[0])
        x = up.crop_array(y, N)
        self.assertTrue(len(y.shape) == len(x.shape))
        self.assertTrue(x.size == N[0])

    def test_center_value_after_cropping(self):
        N = [5]
        K = [8]
        y = np.arange(K[0])
        # fftshift convention
        cN = N[0] // 2
        cK = K[0] // 2
        x = up.crop_array(y, N, "around")
        self.assertTrue(x[cN] == y[cK])
        # imfilter convention
        cN = (N[0] - 1) // 2
        cK = (K[0] - 1) // 2
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(x[cN] == y[cK])

    def test_crop_2d_around(self):
        N = [5, 4]
        K = [7, 8]
        M = np.array([K[n] - N[n] for n in range(len(K))])
        xref = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        # fftshift convention
        y = np.zeros(K)
        start_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        y[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = xref
        x = up.crop_array(y, N, "around")
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)
        # imfilter convention
        yref = np.zeros(K)
        start_crop = [int(np.floor(M[n] / 2)) for n in range(len(N))]
        stop_crop = [int(np.ceil(M[n] / 2)) for n in range(len(N))]
        yref[tuple([np.s_[start_crop[n] : -stop_crop[n]] for n in range(len(N))])] = x
        x = up.crop_array(y, N, "around", True)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_crop_2d_after(self):
        N = [5, 4]
        K = [7, 8]
        xref = np.arange(N[0])[:, np.newaxis] * np.arange(N[1])[np.newaxis, :]
        y = np.zeros(K)
        y[: N[0], : N[1]] = xref
        x = up.crop_array(y, N)
        self.assertTrue(np.linalg.norm(x - xref) <= 1e-13)

    def test_crop_array_nd_shape(self):
        N = np.array([5, 4], dtype="i")
        L = [0, 2]
        R = [2, 1]  # last element in the slice
        rng = np.random.default_rng(1234)
        x = (1 + 1j) * rng.standard_normal(size=N)
        y = up.crop_array_nd(x, L, R)

        self.assertTrue(
            np.allclose(np.array(y.shape, dtype="i"), np.array([3, 1], dtype="i"))
        )

    def test_crop_array_nd_error(self):
        N = np.array([5, 4], dtype="i")
        L = [0, 2]
        R = [2]  # last element in the slice

        with self.assertRaises(ValueError):
            up.crop_array_nd(np.ones(N), L, R)


if __name__ == "__main__":
    unittest.main()
