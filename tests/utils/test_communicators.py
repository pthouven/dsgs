# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import h5py
import numpy as np
from mpi4py import MPI

import dsgs.operators.convolutions as uconv
import dsgs.utils.communications as ucomm
from dsgs.operators.data import generate_2d_gaussian_kernel
from dsgs.operators.linear_convolution import SyncLinearConvolution
from dsgs.utils.communicators import SyncCartesianCommunicator


# TODO: to be completed
# https://stackoverflow.com/questions/63004164/python-unittest-c-fortran-mpi-functions-resulting-in-mpi-init-function-getting-c
class TestCommunicators(unittest.TestCase):
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    seed = 1234

    def tearDown(self):
        self.comm.Barrier()

    def test_communication_1d(self):
        """Test communication of a chunk of a 2d array along a single axis."""
        N = np.array([20, 20], dtype="i")  # overall image size
        M = np.array([2, 2], dtype="i")  # size of convolution kernel
        overlap_size = M - 1  # overlap size
        ndims = 2
        grid_size = [self.size, 1]
        circular_boundaries = False

        cartcomm = self.comm.Create_cart(
            dims=grid_size, periods=ndims * [False], reorder=False
        )
        grid_size_ = np.array(grid_size, dtype="i")
        ranknd = cartcomm.Get_coords(self.rank)
        ranknd = np.array(ranknd, dtype=int)

        # * local portion of the array (including overlap)
        local_tile = ucomm.local_split_range_nd(grid_size_, N, ranknd)
        tile_size = local_tile[:, 1] - local_tile[:, 0] + 1
        local_indices = ucomm.local_split_range_nd(
            grid_size_, N, ranknd, overlap=overlap_size
        )
        facet_size = local_indices[:, 1] - local_indices[:, 0] + 1
        facet = np.full(facet_size, self.rank + 1, dtype=np.float64)

        # print("Worker {}: {}".format(self.rank, facet))

        # * custom communicator object
        cartcomm = self.comm.Create_cart(
            dims=grid_size,
            periods=ndims * [circular_boundaries],
            reorder=False,
        )
        communicator = SyncCartesianCommunicator(
            self.comm,
            cartcomm,
            grid_size,
            facet.itemsize,
            facet_size,
            overlap_size,
            direction=True,
        )

        # * communications
        # ! beware type of the array to avoid segfault
        communicator.update_borders(facet)

        # * checking consistency of the results
        (
            dst,
            sr,
            isvalid_dest,
            isvalid_src,
            sizes_dest,
            sizes_src,
            start_src,
        ) = ucomm.isvalid_communication(ranknd, grid_size_, overlap_size, N)

        s0 = np.zeros(ndims * (ndims - 1) + 1)

        for k in range(sizes_src.shape[0]):
            if isvalid_src[k]:
                sel = tuple(
                    [
                        np.s_[start_src[k, d] : start_src[k, d] + sizes_src[k, d]]
                        for d in range(ndims)
                    ]
                )
                s0[k] = np.sum(facet[sel])

        self.assertTrue(
            np.isclose(np.sum(facet), (self.rank + 1) * np.prod(tile_size) + np.sum(s0))
        )

        # ! beware consistency between type of input array and type assumed
        # ! in the communicator
        # print("Worker {}: {}".format(self.rank, facet))

        self.comm.Barrier()
        communicator.remove()
        self.comm.Barrier()

    def test_communication_2d(self):
        """Test communication of chunks of a 2d array axis, each along one of
        the axis.
        """
        N = np.array([20, 20], dtype="i")  # overall image size
        M = np.array([2, 2], dtype="i")  # size of convolution kernel
        overlap_size = M - 1  # overlap size
        ndims = 2
        grid_size = MPI.Compute_dims(self.size, ndims)
        circular_boundaries = False

        # * create Cartesian topology communicator
        cartcomm = self.comm.Create_cart(
            dims=grid_size, periods=ndims * [False], reorder=False
        )
        grid_size_ = np.array(grid_size, dtype="i")
        ranknd = cartcomm.Get_coords(self.rank)
        ranknd = np.array(ranknd, dtype=int)

        # * local portion of the array (including overlap)
        local_tile = ucomm.local_split_range_nd(grid_size_, N, ranknd)
        tile_size = local_tile[:, 1] - local_tile[:, 0] + 1
        local_indices = ucomm.local_split_range_nd(
            grid_size_, N, ranknd, overlap=overlap_size
        )
        facet_size = local_indices[:, 1] - local_indices[:, 0] + 1
        facet = np.full(facet_size, self.rank + 1, dtype=np.float64)

        # print("Worker {}: {}".format(self.rank, facet))

        # * custom communicator object
        cartcomm = self.comm.Create_cart(
            dims=grid_size,
            periods=ndims * [circular_boundaries],
            reorder=False,
        )
        communicator = SyncCartesianCommunicator(
            self.comm,
            cartcomm,
            grid_size,
            facet.itemsize,
            facet_size,
            overlap_size,
            direction=True,
        )

        # * communications
        # ! beware type of the array to avoid segfault
        communicator.update_borders(facet)

        # * checking consistency of the results
        (
            dst,
            sr,
            isvalid_dest,
            isvalid_src,
            sizes_dest,
            sizes_src,
            start_src,
        ) = ucomm.isvalid_communication(ranknd, grid_size_, overlap_size, N)

        s0 = np.zeros(ndims * (ndims - 1) + 1)

        for k in range(sizes_src.shape[0]):
            if isvalid_src[k]:
                sel = tuple(
                    [
                        np.s_[start_src[k, d] : start_src[k, d] + sizes_src[k, d]]
                        for d in range(ndims)
                    ]
                )
                s0[k] = np.sum(facet[sel])

        self.assertTrue(
            np.isclose(np.sum(facet), (self.rank + 1) * np.prod(tile_size) + np.sum(s0))
        )

        # print("Worker {}: {}".format(self.rank, facet))

        self.comm.Barrier()
        communicator.remove()
        self.comm.Barrier()

    def test_distributed_convolution_backward(self):
        """Testing 2d distributed convolution with backward facet overlap."""

        image_size = np.array([20, 20], dtype="i")  # overall image size
        kernel_size = np.array([2, 2], dtype="i")  # size of convolution
        overlap_size = kernel_size - 1  # overlap size kernel
        data_size = image_size + overlap_size  # full data size
        kernel = generate_2d_gaussian_kernel(kernel_size[0], 0.1)  # square kernel

        ndims = 2
        circular_boundaries = False
        backward = True
        grid_size_ = MPI.Compute_dims(self.size, ndims)

        # * Cartesian topology communicator and nD rank
        cartcomm = self.comm.Create_cart(
            dims=grid_size_, periods=ndims * [False], reorder=False
        )
        ranknd = cartcomm.Get_coords(self.rank)
        ranknd = np.array(ranknd, dtype="i")
        grid_size = np.array(grid_size_, dtype="i")

        # tile size
        tile_pixels = ucomm.local_split_range_nd(
            grid_size, image_size, ranknd, backward=backward
        )
        tile_size = tile_pixels[:, 1] - tile_pixels[:, 0] + 1

        # data size
        local_data_size = (
            tile_size + (ranknd == grid_size - 1) * overlap_size
        )  # ! backward overlap

        # facet size (convolution)
        facet_pixels = ucomm.local_split_range_nd(
            grid_size, image_size, ranknd, overlap=overlap_size, backward=backward
        )
        facet_size = facet_pixels[:, 1] - facet_pixels[:, 0] + 1
        local_conv_size = facet_size + overlap_size
        offset = facet_size - tile_size

        # * setup useful slices
        local_slice_tile = ucomm.get_local_slice(
            ranknd, grid_size, offset, backward=backward
        )  # extract tile from local conv facet

        local_slice_valid_conv = ucomm.slice_valid_coefficients(
            ranknd, grid_size, overlap_size
        )  # extract valid coefficients after local convolutions

        # indexing into global arrays
        global_slice_tile = tuple(
            [np.s_[tile_pixels[d, 0] : tile_pixels[d, 1] + 1] for d in range(ndims)]
        )
        # ! needs to be changed bepending on direction of the overlap?
        global_slice_data = tuple(
            [
                np.s_[tile_pixels[d, 0] : tile_pixels[d, 0] + local_data_size[d]]
                for d in range(ndims)
            ]
        )

        # * parallel rng
        child_seed = None
        if self.rank == 0:
            ss = np.random.SeedSequence(self.seed)
            child_seed = ss.spawn(self.size)
        local_seed = self.comm.scatter(child_seed, root=0)
        local_rng = np.random.default_rng(local_seed)

        # * local image and kernel
        local_image = np.empty(facet_size, dtype="d")
        local_image[local_slice_tile] = local_rng.standard_normal(size=tile_size)
        ft_kernel = np.fft.rfftn(kernel, local_conv_size)

        # * setup communication scheme (direct convolution)
        cartcomm = self.comm.Create_cart(
            dims=grid_size,
            periods=ndims * [circular_boundaries],
            reorder=False,
        )
        communicator = SyncCartesianCommunicator(
            self.comm,
            cartcomm,
            grid_size,
            local_image.itemsize,
            facet_size,
            overlap_size,
            direction=backward,
        )

        # * communications
        communicator.update_borders(local_image)

        # * free custom types
        communicator.remove()

        local_data = uconv.fft_conv(local_image, ft_kernel, local_conv_size)[
            local_slice_valid_conv
        ]

        # * save to an h5 test file (parallel writing)
        f = h5py.File("convolution_test.h5", "w", driver="mpio", comm=self.comm)

        dset_image = f.create_dataset("x", image_size, dtype="d")
        dset_image[global_slice_tile] = local_image[local_slice_tile]

        dset_data = f.create_dataset("y", data_size, dtype="d")
        dset_data[global_slice_data] = local_data

        dset_kernel = f.create_dataset("h", kernel_size, dtype="d")
        if self.rank == 0:
            dset_kernel[:] = kernel
        f.close()
        del f, dset_image, dset_data, dset_kernel

        # * compare to full convolution
        g = h5py.File("convolution_test.h5", "r+", driver="mpio", comm=self.comm)
        if self.rank == 0:
            y0 = g["y"][()]
            x0 = g["x"][()]
            h0 = g["h"][()]

            H0 = np.fft.rfftn(h0, data_size)
            y = uconv.fft_conv(x0, H0, data_size)
            self.assertTrue(np.allclose(y, y0))
        g.close()


if __name__ == "__main__":
    unittest.main()
    # mpiexec -n 2 python -m unittest tests/test_communicators.py
