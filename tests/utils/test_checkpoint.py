"""Test serial checkpoint functionality, extracting and restoring the state of
a random number generator.
"""

# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import numpy as np
import pytest

import dsgs.utils.checkpoint as chkpt


@pytest.fixture
def checkpointer(tmp_path):
    return chkpt.SerialCheckpoint(
        tmp_path / "test",
        cname="gzip",
        clevel=5,
        shuffle=1,
    )


@pytest.fixture
def rng():
    return np.random.default_rng(1234)


def test_checkpoint_extract_and_restore_rng(rng):
    state_array, inc_array = chkpt.extract_rng_state(rng)
    a = rng.normal()
    chkpt.restore_rng_state(rng, inc_array, state_array)
    b = rng.normal()
    assert np.allclose(a, b)


def test_checkpoint_rng(rng, checkpointer):
    checkpointer.save(1, None, rng=rng)
    a = rng.normal()
    checkpointer.load(1, [None], rng)
    b = rng.normal()
    assert np.allclose(a, b)


def test_checkpoint_variables(checkpointer):
    a = 3
    b = 4
    c = np.ones((2, 2))
    checkpointer.save(1, [None, None, None], rng=None, a=a, b=b, c=c)
    loaded_dic = checkpointer.load(1, 3 * [np.s_[:]], None, "a", "b", "c")
    assert np.allclose(a, loaded_dic["a"])
    assert np.allclose(b, loaded_dic["b"])
    assert np.allclose(c, loaded_dic["c"])


def test_checkpoint_size_one_ndarray(checkpointer):
    a = np.full((1,), 2.0)
    checkpointer.save(1, [None], rng=None, a=a)
    loaded_dic = checkpointer.load(1, [np.s_[:]], None, "a")
    assert np.allclose(a, loaded_dic["a"])


def test_checkpoint_variable_and_rng(rng, checkpointer):
    a0 = rng.standard_normal(2)
    checkpointer.save(
        1,
        [None, None],
        rng=rng,
        a=a0,
        rdcc_nbytes=1024**2 * 200,
    )  # 200 MB cache
    a = rng.standard_normal(2)
    loaded_dic = checkpointer.load(1, [np.s_[:]], rng, "a")
    b = rng.standard_normal(2)
    assert np.allclose(a, b)
    assert np.allclose(a0, loaded_dic["a"])
    assert loaded_dic["a"].dtype == a0.dtype
    # ! load data and byteorder (native is little endian on mac)
    # https://numpy.org/doc/stable/reference/generated/numpy.dtype.newbyteorder.html
    # ! check only for numpy.ndarray
    # assert loaded_dic["a"].dtype.byteorder == a0.dtype.byteorder


@pytest.mark.slow
def test_checkpoint_variables_chunking(checkpointer):
    a = np.ones((500, 500))
    b = 3
    checkpointer.save(1, [(100, 100), None], rng=None, a=a, b=b)
    loaded_dic = checkpointer.load(1, 2 * [np.s_[:]], None, "a", "b")
    assert np.allclose(a, loaded_dic["a"])
    assert np.allclose(b, loaded_dic["b"])
