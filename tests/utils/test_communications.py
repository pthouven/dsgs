# author: pthouvenin (pierre-antoine.thouvenin@centralelille.fr)
#
# reference: P.-A. Thouvenin, A. Repetti, P. Chainais - **A distributed Gibbs
# Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**,
# [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2022.

import unittest

import numpy as np
from mpi4py import MPI

import dsgs.utils.communications as ucomm


class TestCommunications(unittest.TestCase):
    def setUp(self):
        self.N = 9
        self.nchunks = 3
        self.overlap = 3

    def test_split_range_overlap_error(self):
        with self.assertRaises(ValueError):
            ucomm.split_range(self.nchunks, self.N, 4)

    def test_local_split_range_overlap_error(self):
        with self.assertRaises(ValueError):
            ucomm.local_split_range(self.nchunks, self.N, 0, 4)
        with self.assertRaises(ValueError):
            ucomm.local_split_range(self.nchunks, self.N, self.N)

    def test_split_range_no_overlap(self):
        rg = ucomm.split_range(self.nchunks, self.N)
        # check that 2 consecutive start index are distant from 1
        self.assertTrue(np.all(rg[1:, 0] - rg[:-1, 1] == 1))
        # check size of each chunk (same size for each in this case)
        self.assertTrue(np.all(np.diff(rg, n=1, axis=1) + 1 == 3))
        # test single process and global versions coincide
        rg2 = np.concatenate(
            [
                ucomm.local_split_range(self.nchunks, self.N, k)[None, :]
                for k in range(self.nchunks)
            ],
            axis=0,
        )
        self.assertTrue(np.allclose(rg, rg2))

    def test_split_range_overlap(self):
        rg = ucomm.split_range(self.nchunks, self.N, self.overlap)
        # check overlap between 2 consecutive segments (from the left)
        self.assertTrue(np.all(np.abs(rg[:-1, 1] - rg[1:, 0] + 1) == self.overlap))
        # test single process and global versions coincide
        rg2 = np.concatenate(
            [
                ucomm.local_split_range(self.nchunks, self.N, k, overlap=self.overlap)[
                    None, :
                ]
                for k in range(self.nchunks)
            ],
            axis=0,
        )
        self.assertTrue(np.allclose(rg, rg2))

    def test_split_range_overlap_forward(self):
        rg = ucomm.split_range(self.nchunks, self.N, self.overlap, False)
        # check overlap between 2 consecutive segments (from the left)
        self.assertTrue(np.all(np.abs(rg[:-1, 1] - rg[1:, 0] + 1) == self.overlap))
        # test single process and global versions coincide
        rg2 = np.concatenate(
            [
                ucomm.local_split_range(
                    self.nchunks,
                    self.N,
                    k,
                    overlap=self.overlap,
                    backward=False,
                )[None, :]
                for k in range(self.nchunks)
            ],
            axis=0,
        )
        self.assertTrue(np.allclose(rg, rg2))

    def test_local_split_range(self):
        rg = ucomm.split_range(self.nchunks, self.N, self.overlap)
        global_rg = np.concatenate(
            [
                ucomm.local_split_range(self.nchunks, self.N, k, overlap=self.overlap)[
                    None, :
                ]
                for k in range(self.nchunks)
            ],
            axis=0,
        )
        self.assertTrue(np.allclose(global_rg, rg))

    def test_local_split_range_overlap_n(self):
        rg = np.concatenate(
            (
                ucomm.local_split_range(self.nchunks, self.N, 1, self.overlap)[None, :],
                ucomm.local_split_range(self.nchunks, self.N, 0, self.overlap)[None, :],
            ),
            axis=0,
        )
        rg2 = rg = ucomm.local_split_range_nd(
            np.array(2 * [self.nchunks]),
            np.array(2 * [self.N]),
            np.array([1, 0]),
            np.array(2 * [self.overlap]),
        )
        self.assertTrue(np.allclose(rg, rg2))

    def test_split_range_interleaved_error(self):
        with self.assertRaises(ValueError):
            ucomm.split_range_interleaved(self.N + 1, self.N)

    def test_local_split_range_interleaved_error(self):
        with self.assertRaises(ValueError):
            ucomm.local_split_range_interleaved(self.N + 1, self.N, 0)
        with self.assertRaises(ValueError):
            ucomm.local_split_range_interleaved(2, self.N, self.N)

    def test_split_range_interleaved(self):
        rg = ucomm.split_range_interleaved(self.nchunks, self.N)
        self.assertTrue(np.all([rg[k].start == k for k in range(len(rg))]))
        self.assertTrue(np.all([rg[k].stop == self.N for k in range(len(rg))]))
        self.assertTrue(np.all([rg[k].step == self.nchunks for k in range(len(rg))]))
        rg2 = ucomm.local_split_range_interleaved(self.nchunks, self.N, 0)
        self.assertTrue(np.all([rg2 == rg[0]]))

    def test_get_neighbour(self):
        ranknd = np.array([0, 1], dtype="i")
        grid_size = np.array([self.nchunks, self.nchunks], dtype="i")
        disp = np.ones((2,), dtype="i")
        rank = ucomm.get_neighbour(ranknd, grid_size, disp)
        self.assertTrue(
            rank == (ranknd[0] + disp[0]) * grid_size[-1] + ranknd[-1] + disp[-1]
        )  # = 5

    def test_isvalid_communication(self):
        ranknd = np.array([1, 1, 1], dtype="i")
        grid_size = np.array([3, 3, 2], dtype="i")
        overlap_size = np.array([0, 1, 1], dtype="i")
        array_size = np.array([10, 10, 10], dtype="i")
        temp_size = array_size - overlap_size
        ndims = array_size.size

        # communication axes for 3D case: x, y, z, xy, yz, zx, xyz
        (
            dest,
            src,
            isvalid_dest,
            isvalid_src,
            sizes_dest,
            sizes_src,
            start_src,
        ) = ucomm.isvalid_communication(ranknd, grid_size, overlap_size, array_size)

        # testing valid neighbours (dest)
        # i, j, k -> i*np.prod(N[1:]) + j*N[2] + k
        ranky = ucomm.get_neighbour(ranknd, grid_size, np.array([0, 1, 0], dtype="i"))

        self.assertTrue(
            np.array_equal(
                dest,
                np.array(
                    [
                        MPI.PROC_NULL,
                        ranky,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                    ],
                    dtype="i",
                ),
            )
        )
        self.assertTrue(
            np.array_equal(
                isvalid_dest,
                np.array(
                    [False, True, False, False, False, False, False],
                    dtype="bool",
                ),
            )
        )

        # testing valid neighbours (src)
        ranky = ucomm.get_neighbour(ranknd, grid_size, np.array([0, -1, 0], dtype="i"))
        rankz = ucomm.get_neighbour(ranknd, grid_size, np.array([0, 0, -1], dtype="i"))
        rankyz = ucomm.get_neighbour(
            ranknd, grid_size, np.array([0, -1, -1], dtype="i")
        )

        self.assertTrue(
            np.array_equal(
                src,
                np.array(
                    [
                        MPI.PROC_NULL,
                        ranky,
                        rankz,
                        MPI.PROC_NULL,
                        rankyz,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                    ],
                    dtype="i",
                ),
            )
        )
        self.assertTrue(
            np.all(
                isvalid_src
                == np.array(
                    [False, True, True, False, True, False, False],
                    dtype="bool",
                )
            )
        )

        # testing size of the data to be communicated (extent along each
        # dimension for each communication)
        # ! valid destination: y
        expected_sizes_dest = np.concatenate(
            (
                np.zeros((1, ndims), dtype="i"),
                np.array([temp_size[0], overlap_size[1], *temp_size[2:]], dtype="i")[
                    None, :
                ],
                np.zeros((5, ndims)),
            ),
            axis=0,
        )

        # ! valid source: y, z, yz
        expected_sizes_src = np.concatenate(
            (
                np.zeros((1, ndims), dtype="i"),
                np.array([temp_size[0], overlap_size[1], temp_size[2]], dtype="i")[
                    None, :
                ],
                np.array([*temp_size[:2], overlap_size[2]], dtype="i")[None, :],
                np.zeros((1, ndims), dtype="i"),
                np.array([temp_size[0], *overlap_size[1:]], dtype="i")[None, :],
                np.zeros((2, ndims), dtype="i"),
            ),
            axis=0,
        )

        self.assertTrue(np.array_equal(sizes_dest, expected_sizes_dest))
        self.assertTrue(np.array_equal(sizes_src, expected_sizes_src))

        # check where the overlap should start
        # self.assertTrue(
        #     np.all(
        #         start_src
        #         == np.array(
        #             [0, , True, 0, True, 0, 0], dtype="bool"
        #         )
        #     )
        # )

    def test_isvalid_communication_forward(self):
        ranknd = np.array([1, 1, 1], dtype="i")
        grid_size = np.array([3, 3, 2], dtype="i")
        overlap_size = np.array([0, 1, 1], dtype="i")
        array_size = np.array([10, 10, 10], dtype="i")
        temp_size = array_size - overlap_size
        ndims = array_size.size

        # communication axes for 3D case: x, y, z, xy, yz, zx, xyz
        (
            dest,
            src,
            isvalid_dest,
            isvalid_src,
            sizes_dest,
            sizes_src,
            start_src,
        ) = ucomm.isvalid_communication(
            ranknd, grid_size, overlap_size, array_size, backward=False
        )

        # testing valid neighbours (dest)
        # i, j, k -> i*np.prod(N[1:]) + j*N[2] + k
        ranky = ucomm.get_neighbour(ranknd, grid_size, np.array([0, -1, 0], dtype="i"))
        rankz = ucomm.get_neighbour(ranknd, grid_size, np.array([0, 0, -1], dtype="i"))
        rankyz = ucomm.get_neighbour(
            ranknd, grid_size, np.array([0, -1, -1], dtype="i")
        )

        self.assertTrue(
            np.array_equal(
                dest,
                np.array(
                    [
                        MPI.PROC_NULL,
                        ranky,
                        rankz,
                        MPI.PROC_NULL,
                        rankyz,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                    ],
                    dtype="i",
                ),
            )
        )
        self.assertTrue(
            np.array_equal(
                isvalid_dest,
                np.array(
                    [False, True, True, False, True, False, False],
                    dtype="bool",
                ),
            )
        )

        # testing valid neighbours (src)
        ranky = ucomm.get_neighbour(ranknd, grid_size, np.array([0, 1, 0], dtype="i"))
        self.assertTrue(
            np.array_equal(
                src,
                np.array(
                    [
                        MPI.PROC_NULL,
                        ranky,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                        MPI.PROC_NULL,
                    ],
                    dtype="i",
                ),
            )
        )
        self.assertTrue(
            np.all(
                isvalid_src
                == np.array(
                    [False, True, False, False, False, False, False],
                    dtype="bool",
                )
            )
        )

        # testing size of the data to be communicated (extent along each
        # dimension for each communication)
        # ranknd = [1, 1, 1]
        # grid_size = [3, 3, 2]
        # size N[d] when the facet is on the border of the grid along dimension
        # d
        # ! valid destination: y, z, yz
        expected_sizes_dest = np.concatenate(
            (
                np.zeros((1, ndims), dtype="i"),  # x
                np.array([temp_size[0], overlap_size[1], array_size[2]], dtype="i")[
                    None, :
                ],  # y
                np.array([temp_size[0], temp_size[1], overlap_size[2]], dtype="i")[
                    None, :
                ],  # z
                np.zeros((1, ndims), dtype="i"),  # xy
                np.array([temp_size[0], overlap_size[1], overlap_size[2]], dtype="i")[
                    None, :
                ],  # yz
                np.zeros((2, ndims), dtype="i"),  # zx, xyz
            ),
            axis=0,
        )

        # ! valid source: y
        expected_sizes_src = np.concatenate(
            (
                np.zeros((1, ndims), dtype="i"),
                np.array([temp_size[0], overlap_size[1], array_size[2]], dtype="i")[
                    None, :
                ],
                np.zeros((5, ndims), dtype="i"),
            ),
            axis=0,
        )

        self.assertTrue(np.array_equal(sizes_dest, expected_sizes_dest))
        self.assertTrue(np.array_equal(sizes_src, expected_sizes_src))

    def test_get_local_slice(self):
        grid_size = np.array([self.nchunks], dtype="i")
        overlap_size = np.array([self.overlap], dtype="i")
        ranknd = np.empty(1, dtype="i")

        for k in range(self.nchunks):
            ranknd[0] = k
            local_slice = ucomm.get_local_slice(
                ranknd, grid_size, overlap_size, backward=True
            )

            self.assertTrue(local_slice[0].stop is None)
            if k == 0:
                self.assertTrue(local_slice[0].start is None)
            else:
                self.assertTrue(local_slice[0].start == overlap_size[0])

            local_slice = ucomm.get_local_slice(
                ranknd, grid_size, overlap_size, backward=False
            )

            self.assertTrue(local_slice[0].start is None)
            if k == self.nchunks - 1:
                self.assertTrue(local_slice[0].stop is None)
            else:
                self.assertTrue(local_slice[0].stop == -overlap_size[0])

    def test_get_local_slice_error(self):
        grid_size = np.array(2 * [self.nchunks], dtype="i")
        overlap_size = np.array(2 * [self.overlap], dtype="i")
        ranknd = np.ones((1), dtype="i")

        with self.assertRaises(AssertionError):
            ucomm.get_local_slice(ranknd, grid_size, overlap_size)


if __name__ == "__main__":
    unittest.main()
