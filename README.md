# A Distributed Block-Split-Gibbs Sampler (DSGS) with Hypergraph Structure for High-Dimensional Inverse Problems

[![license](https://img.shields.io/badge/license-GPL--3.0-brightgreen.svg)](LICENSE)
[![docs-page](https://img.shields.io/badge/docs-latest-blue)](#)
[![tests](docs/coverage.svg)](docs/build/coverage_html_report/index.html)
[![docstr-coverage](docs/docstr_coverage_badge.svg)](#)

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
![Python](https://img.shields.io/badge/python-3670A0?style=flat&logo=python&logoColor=ffdd54)
[![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

<details>
<summary>Table of content</summary>

## Table of content

- [A Distributed Block-Split-Gibbs Sampler (DSGS) with Hypergraph Structure for High-Dimensional Inverse Problems](#a-distributed-block-split-gibbs-sampler-dsgs-with-hypergraph-structure-for-high-dimensional-inverse-problems)
  - [Table of content](#table-of-content)
  - [Description](#description)
  - [Installation](#installation)
    - [Environment setup](#environment-setup)
  - [Experiments](#experiments)
    - [:warning: **WARNING**: Memory requirements to save all the results](#warning-warning-memory-requirements-to-save-all-the-results)
    - [Running experiments in a detached tmux session (requires tmux)](#running-experiments-in-a-detached-tmux-session-requires-tmux)
    - [Running the experiments](#running-the-experiments)
  - [Development](#development)
    - [Building the documentation](#building-the-documentation)
    - [Assessing code and docstring coverage](#assessing-code-and-docstring-coverage)
  - [License](#license)

</details>

______________________________________________________________________

## Description

Python codes associated with the method described in the following paper.

> P.-A. Thouvenin, A. Repetti, P. Chainais - **A Distributed Block-Split Gibbs Sampler with Hypergraph Structure for High-Dimensional Inverse Problems**, [arxiv preprint 2210.02341](http://arxiv.org/abs/2210.02341), October 2023, to appear in JCGS.

**Authors**: P.-A. Thouvenin, A. Repetti, P. Chainais

______________________________________________________________________

## Installation

### Environment setup

- The library requires a functional installation of [`conda`](https://docs.anaconda.com/free/anaconda/install/index.html) or [`mamba`](https://mamba.readthedocs.io/en/latest/installation.html) (a fast, drop-in replacement for `conda`). The instructions below are given in the case of `mamba` for a fast setup. Using `conda` instead only requires replacing the word `mamba` by `conda` in the command line instructions below.

- To install the library, issue the following commands in a terminal.

```bash
# Cloning the repo. or unzip the dsgs-main.zip code archive
# git clone --recurse-submodules https://gitlab.cristal.univ-lille.fr/pthouven/dsgs.git
unzip dsgs-main.zip
cd dsgs-main

# Create a conda environment using one of the lock files provided in the archive
# (use dsgs_environment_osx.lock.yml for MAC OS)
mamba env create --name dsgs --file dsgs_environment_linux.lock.yml
# mamba env create --name dsgs --file dsgs_environment.yml

# Activate the environment
mamba activate dsgs

# Install the library in editable mode
mamba develop src/

# Deleting the environment (if needed)
# mamba env remove --name dsgs

# Generating lock file from existing environment (if needed)
# mamba env export --name dsgs --file dsgs_environment_linux.lock.yml
# or
# mamba list --explicit --md5 > explicit_dsgs_env_linux-64.txt
# mamba create --name jcgs-test -c conda-forge --file explicit_dsgs_env_linux-64.txt
# pip install docstr-coverage genbadge wily sphinxcontrib-apa sphinx_copybutton

# Manual install (if absolutely needed)
# mamba create --name dsgs numpy numba mpi4py "h5py>=2.9=mpi*" scipy scikit-image matplotlib imageio tqdm jupyterlab pytest black flake8 isort coverage pre-commit sphinx sphinx_rtd_theme sphinxcontrib-bibtex sphinx-autoapi sphinxcontrib furo conda-lock conda-build
# mamba activate dsgs
# pip install sphinxcontrib-apa sphinx_copybutton docstr-coverage genbadge wily
# mamba develop src
```

- To avoid [file lock issue in h5py](https://github.com/h5py/h5py/issues/1101), you may need to add the following line to your `~/.zshrc` file (or `~/.bashrc`)

```bash
export HDF5_USE_FILE_LOCKING='FALSE'
```

- To test the installation went well, you can run the unit-tests provided in the package using the command below.

```bash
mamba activate dsgs
pytest --collect-only
export NUMBA_DISABLE_JIT=1  # need to disable jit compilation to check test coverage
coverage run -m pytest      # run all the unit-tests (see Documentation section for more details)
```

______________________________________________________________________

## Experiments

All the experiments reported in the paper can be reproduced from the `.sh` scripts provided in `./examples/jcgs`, as detailed in the following paragraphs.

### :warning: **WARNING**: Memory requirements to save all the results

Running all the experiments produces a large volume of results / checkpoint data saved to the hard-drive in HDF5 (`.h5`) format, essentially for the experiments involving serial samplers (206GB in total).

All the samples generated with serial samplers are saved to disk to better assess the sampling quality, which explains such a large requirement. Ensure maximum 20GB hard-drive memory is available for each single run of one of the serial samplers on one of the datasets considered.

Users are strongly advised to progressively run a subset of the experiments at once, carefully monitoring the remaining space available on the hard-drive where results are saved. The checkpoint files corresponding to burn-in samples can be safely discarded once the experiment is finalized.

Each run of the distributed sampler on a dataset requires 300MB memory on the hard drive, as fewer elements are saved to disk (last state to restart the chain + average over the current batch to compute the MMSE estimator).

### Running experiments in a detached tmux session (requires [tmux](https://github.com/tmux/tmux/wiki))

The experiments can be run from a detached [tmux](https://github.com/tmux/tmux/wiki) session running in the background. See the `./examples/jcgs/run_from_tmux.sh` script for further details.

A few basic instructions to interact with a `tmux` session are given below.

```bash
# check name of the tmux session, called session_name in the following
tmux list-session
tmux a -t session_name # press crtl+b to kill the session once the work is done

# to detach from a session (leave it running in the background, press ctrl+b, then ctrl+d)
# in the tmux session, press crtl+b to kill it once the work is done or, from a normal terminal
tmux kill-session -t session_name
```

### Running the experiments

- The folder `./examples/jcgs/configs` contains `.json` files summarizing the list of parameters used for the different experiments/datasets. All the experiments can be reproduced using the following commands. Details about the location of the HDF5 files / data produced are included in each script.

```bash
# from a terminal at the root of the archive

mamba activate dsgs
cd examples/jcgs

# generate all the synthetic datasets used in the experiments (to be run only once)
bash generate_data.sh

# run all the experiments based on serial samplers (MYULA and proposed sampler)
bash sampling_quality_experiment.sh

# run strong scaling experiment
bash strong_scaling_experiment.sh

# run weak scaling experiment
bash weak_scaling_experiment.sh

# deactivating the conda environment (when no longer needed)
mamba deactivate
```

- The content of an [`.h5`](https://docs.h5py.org/en/stable/mpi.html?highlight=h5dump#using-parallel-hdf5-from-h5py) file can be quickly checked from the terminal (see the [`h5py`](https://docs.h5py.org/en/stable/quick.html) documentation for further details). Some examples are provided below.

```bash
mamba activate dsgs

# replace <filename> by the name of your file in the instructions below
h5dump --header <filename>.h5 # displays the name and size of all variables contained in the file
h5dump <filename>.h5 # diplays the value of all the variables saved in the file
h5dump -d "/GroupFoo/databar[1,1;2,3;3,19;1,1]" <filename>.h5 # display part of a variable from a dataset within a given h5 file
h5dump -d dset <filename>.h5 # displays content of a dataset dset
h5dls -d <filename>.h5/dset  # displays content of a dataset dset
```

______________________________________________________________________

## Development

### Building the documentation

- Most functionalities are fully documented using the [`numpy` docstring style](https://numpydoc.readthedocs.io/en/latest/format.html).
- The documentation can be generated in `.html` format using the following commands issued from a terminal.

```bash
mamba activate dsgs
cd docs
make html
open docs/build/html/index.html
```

### Assessing code and docstring coverage

To test the code/docstring coverage, run the following commands from a terminal.

```bash
mamba activate dsgs
pytest --collect-only
export NUMBA_DISABLE_JIT=1  # need to disable jit compilation to check test coverage
coverage run -m pytest  # check all tests
coverage report  # generate a coverage report in the terminal
coverage html  # HTML-based reports which let you visually see what lines of code were not tested
coverage xml -o reports/coverage/coverage.xml  # produce xml file to generate the badge
genbadge coverage -o docs/coverage.svg
docstr-coverage .  # check docstring coverage and generate the associated coverage badge
```

To launch a single test, run a command of the form

```bash
mamba activate dsgs
python -m pytest tests/operators/test_crop.py
pytest --markers  # check full list of markers availables
pytest -m "not mpi" --ignore-glob=**/archive_unittest/* # run all tests not marked as mpi + ignore files in any directory "archive_unittest"
mpiexec -n 2 python -m mpi4py -m pytest -m mpi  # run all tests marked mpi with 2 cores
```

______________________________________________________________________

## License

The project is licensed under the [GPL-3.0 license](LICENSE).
